﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;
using EuroleagueReader;

namespace EuroleagueAnalytics
{
    class OffDefRatings
    {
        public bool RatingsCalculations(int gameId)
        {
            using (var context = new UsersContext())
            {
                
                List<Boxscore> BoxscoreList = context.Boxscores.Where(t => t.GameId == gameId).ToList();
                Game game = context.Games.FirstOrDefault(t => t.Id == gameId);
                List<PlayerPosession> pPossessions = context.PlayerPossessions.Where(t => t.GameId == gameId).ToList();
                List<Team_Possession> tPossessions = context.Team_Possessions.Where(t => t.GameId == gameId).ToList();
                var GameBoxscorePossessions = from c in BoxscoreList
                                              join t in pPossessions on c.PlayerId equals t.PlayerId
                                              select new {c, t };               
                
                foreach (var boxscore in GameBoxscorePossessions)
                {
                    double OffRtg = 0.0, DefRtg = 0.0;
                    if (boxscore.t.TeamPos != 0)
                    {
                        OffRtg = 100 * ((double)boxscore.c.PointsFw / (double)boxscore.t.TeamPos);
                        DefRtg = 100 * ((double)boxscore.c.PointsAg / (double)boxscore.t.TeamPos);
                    }                   
                    boxscore.t.OffRtg = OffRtg;
                    boxscore.t.DefRtg = DefRtg;
                    boxscore.t.NetRtg = OffRtg - DefRtg;
                }
                context.SaveChanges();           

                Team_Possession tPossessionHome = tPossessions.Where(t => t.TeamId == game.Team1).FirstOrDefault();
                Team_Possession tPossessionAway = tPossessions.Where(t => t.TeamId == game.Team2).FirstOrDefault();
                double tOffRtg = 0.0, tDefRtg = 0.0;
                tOffRtg = 100 * ((double)game.Score1 / (double)tPossessionHome.Possessions);
                tDefRtg = 100 * ((double)game.Score2 / (double)tPossessionHome.Possessions);
                tPossessionHome.OffRtg = tOffRtg;
                tPossessionHome.DefRtg = tDefRtg;
                tPossessionHome.NetRtg = tOffRtg - tDefRtg;
                tOffRtg = 100 * ((double)game.Score2 / (double)tPossessionAway.Possessions);
                tDefRtg = 100 * ((double)game.Score1 / (double)tPossessionAway.Possessions);
                tPossessionAway.OffRtg = tOffRtg;
                tPossessionAway.DefRtg = tDefRtg;
                tPossessionAway.NetRtg = tOffRtg - tDefRtg;
                context.SaveChanges();
            }
            return true;
        }
    }
}
