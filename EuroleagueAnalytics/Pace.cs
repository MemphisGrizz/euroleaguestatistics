﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;
using EuroleagueReader;

namespace EuroleagueAnalytics
{
    /*public class PlayerPossessions
    {
        public int PlayerId;
        public int GameId;
        public int TeamId;
        public int OppTeamId;
        public int Minutes; 
        public TimeSpan BoxScoreMinutes;
        public int PlayerPossesions;
        public int TeamPossesions;
    }*/

    public class PlayersOnCourt
    {
        public int PlayerId;
        public bool OnCourt;
    }

    public class Pace
    {
        public bool PlayerPossesionsImport(int gameId, List<PlayerTeam> Players)
        {
            using (var context = new UsersContext())
            {
                List<PlayersAction> ActionList = context.PlayersActions.Where(t => t.GameId == gameId).ToList();
                List<Boxscore> BoxscoreList = context.Boxscores.Where(t => t.GameId == gameId).ToList();
                List<PlayersAction> NextList = new List<PlayersAction>();
                List<PlayersAction> PrevList = new List<PlayersAction>();
                List<PlayerPosession> Possessions = new List<PlayerPosession>();
                CurrentGame curGame = new CurrentGame();
                List<PlayersOnCourt> playersOnCourt = new List<PlayersOnCourt>();
                List<Team_Possession> Team_Possessions = new List<Team_Possession>();
                int CurIndex = 0, HomeTeamPos = 0, AwayTeamPos = 0, LastMinute = 0;
                curGame.HomeId = context.Games.Where(t => t.Id == gameId).FirstOrDefault().Team1;
                curGame.VisitId = context.Games.Where(t => t.Id == gameId).FirstOrDefault().Team2;
                //заполняем кто вышел на площадку
                foreach (Boxscore boxscore in BoxscoreList)
                {                    
                    playersOnCourt.Add(new PlayersOnCourt { PlayerId = boxscore.PlayerId, OnCourt = false });
                }
                foreach (PlayerTeam player in Players)
                {
                    int playerId = context.Players.FirstOrDefault(t => t.Name == player.Name).Id;
                    Possessions.Add(new PlayerPosession
                    {
                        GameId = gameId, 
                        Minutes = context.Boxscores.Where(s => s.GameId == gameId).FirstOrDefault(t => t.PlayerId == playerId).Min,
                        BoxscoreMinutes = context.Boxscores.Where(s => s.GameId == gameId).FirstOrDefault(t => t.PlayerId == playerId).BoxMin,                      
                        PlayerId = playerId,
                        TeamId = context.Teams.FirstOrDefault(t => t.Name == player.Team).Id,
                        OppTeamId = 0,
                        PlayerPos = 0,
                        TeamPos = 0                        
                    });
                }

                //var text_action = ActionList.Where(t => t.Minute == 20);
                foreach (PlayersAction action in ActionList)
                {
                    
                    CurIndex = action.Id;
                    for (int index = CurIndex + 1; index < CurIndex + 3; index++)
                    {
                        NextList.Add(ActionList.FirstOrDefault(t => t.Id == index));
                    }
                    if ((action.ActionId != 10) && (action.ActionId != 11)) // not free throws
                    {                        
                        PlayerPossessionsEditor(action, Possessions, action.PlayerId, NextList, playersOnCourt, ref LastMinute);
                        NextList.Clear();
                    }
                    else // free throws
                    {
                        for (int index = CurIndex - 1; index > CurIndex - 13; index--)
                        {
                            PrevList.Add(ActionList.FirstOrDefault(t => t.Id == index));
                        }
                        FreeThrowsEditor(action, Possessions, action.PlayerId, NextList, PrevList, playersOnCourt, ref LastMinute);
                        NextList.Clear();
                        PrevList.Clear();
                    }
                }

                foreach (PlayerPosession possession in Possessions)
                {
                    if (possession != null)
                    {
                        if (possession.TeamId == curGame.HomeId)
                            HomeTeamPos = HomeTeamPos + possession.TeamPos;
                        if (possession.TeamId == curGame.VisitId)
                            AwayTeamPos = AwayTeamPos + possession.TeamPos;
                        context.PlayerPossessions.Add(possession);
                        context.SaveChanges();
                    }
                }

                Team_Possessions.Add(new Team_Possession { TeamId = curGame.HomeId, GameId = gameId, Possessions = HomeTeamPos/5, Minutes = LastMinute});
                Team_Possessions.Add(new Team_Possession { TeamId = curGame.VisitId, GameId = gameId, Possessions = AwayTeamPos/5, Minutes = LastMinute });

                foreach (Team_Possession tpossession in Team_Possessions)
                {
                    if (tpossession != null)
                    {
                        context.Team_Possessions.Add(tpossession);
                        context.SaveChanges();
                    }
                }
             }
            return true;
        }

        private bool PlayerPossessionsEditor(PlayersAction action, List<PlayerPosession> Possessions, int PlayerId, List<PlayersAction> NextList, List<PlayersOnCourt> playersOnCourt, ref int LastMinute)
        {
            var playerRecord = Possessions.FirstOrDefault(t => t.PlayerId == PlayerId);
            if (action != null)
            {
                switch (action.ActionId)
                {
                    case 1: // turnover
                        if (playerRecord != null) playerRecord.PlayerPos++;
                        PlayerTeamPossessionsEditor(Possessions, playersOnCourt, action.TeamId);
                        break;
                    case 6: // lay up
                        if ((NextList[0].ActionId == 8) && (NextList[1].ActionId == 9) && (NextList[1].PlayerId == PlayerId))
                        {
                            break;
                        }
                        else
                        {
                            playerRecord.PlayerPos++;
                            PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                        }
                        break;
                    case 12: // dunk
                        if ((NextList[0].ActionId == 8) && (NextList[1].ActionId == 9) && (NextList[1].PlayerId == PlayerId))
                        {
                            break;
                        }
                        else
                        {
                            playerRecord.PlayerPos++;
                            PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                        }
                        break;
                    case 15: // two pointer
                        if ((NextList[0].ActionId == 8) && (NextList[1].ActionId == 9) && (NextList[1].PlayerId == PlayerId))
                        {
                            break;
                        }
                        else
                        {
                            playerRecord.PlayerPos++;
                            PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                        }
                        break;
                    case 20: // three pointer
                        if ((NextList[0].ActionId == 8) && (NextList[1].ActionId == 9) && (NextList[1].PlayerId == PlayerId))
                        {
                            break;
                        }
                        else
                        {
                            playerRecord.PlayerPos++;
                            PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                        }
                        break;    
                    case 2: // missed lay up
                        if (NextList[0].ActionId != 26)
                        {
                            if (NextList[0].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        else
                        {
                            if ((NextList[0].Minute == 11) || (NextList[0].Minute == 21) || (NextList[0].Minute == 31) || (NextList[0].Minute == 41)
                                || (NextList[0].Minute == 46) || (NextList[0].Minute == 51) || (NextList[0].Minute == 56)) //if next action is minute and it is end of the quarter
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                                break;
                            }

                            if (NextList[1].ActionId == 5) // if not end of quarter and def rebound
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        break;
                    case 29: // missed dunk
                        if (NextList[0].ActionId != 26)
                        {
                            if (NextList[0].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        else
                        {
                            if ((NextList[0].Minute == 11) || (NextList[0].Minute == 21) || (NextList[0].Minute == 31) || (NextList[0].Minute == 41)
                                || (NextList[0].Minute == 46) || (NextList[0].Minute == 51) || (NextList[0].Minute == 56)) //if next action is minute and it is end of the quarter
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                                break;
                            }

                            if (NextList[1].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        break;
                    case 14: // missed two pointer
                        if (NextList[0].ActionId != 26)
                        {
                            if (NextList[0].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        else
                        {
                            if ((NextList[0].Minute == 11) || (NextList[0].Minute == 21) || (NextList[0].Minute == 31) || (NextList[0].Minute == 41)
                                || (NextList[0].Minute == 46) || (NextList[0].Minute == 51) || (NextList[0].Minute == 56)) //if next action is minute and it is end of the quarter
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                                break;
                            }

                            if (NextList[1].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        break;
                    case 13: // missed three pointer
                        if (NextList[0].ActionId != 26)
                        {
                            if (NextList[0].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        else
                        {
                            if (NextList[1].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }

                            if ((NextList[0].Minute == 11) || (NextList[0].Minute == 21) || (NextList[0].Minute == 31) || (NextList[0].Minute == 41)
                                || (NextList[0].Minute == 46) || (NextList[0].Minute == 51) || (NextList[0].Minute == 56)) //if next action is minute and it is end of the quarter
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                                break;
                            }
                        }
                        break;
                    case 3: // shot rejected
                        if (NextList[1].ActionId != 26)
                        {
                            if (NextList[1].ActionId == 5)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        else
                        {
                            if (NextList[2].ActionId != 16)
                            {
                                playerRecord.PlayerPos++;
                                PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                            }
                        }
                        break;                    
                    case 18: // In - changing current line-ups
                        playersOnCourt.FirstOrDefault(t => t.PlayerId == PlayerId).OnCourt = true;
                        break;
                    case 24: // Out - changing current line-ups
                        playersOnCourt.FirstOrDefault(t => t.PlayerId == PlayerId).OnCourt = false;
                        break;
                    case 23: // try to understand how many minutes were played
                        LastMinute = action.Minute - 1;
                        break;
                    default:
                        break;
                }
            }
            return true;
        }

        // calculate Team possessions while player on the court
        private bool PlayerTeamPossessionsEditor(List<PlayerPosession> Possessions, List<PlayersOnCourt> playersOnCourt, int pActionTeamId/*id of player's team*/)
        {
            var playerRecordsId = playersOnCourt.Where(t => t.OnCourt == true);
            
            if (playerRecordsId != null)
            {
                foreach (var pId in playerRecordsId)
                {
                    var playerRecord = Possessions.FirstOrDefault(t => t.PlayerId == pId.PlayerId);
                    if (playerRecord.TeamId == pActionTeamId)
                    {
                        playerRecord.TeamPos++;
                    }
                }
            }
            return true;
        }

        private bool FreeThrowsEditor(PlayersAction action, List<PlayerPosession> Possessions, int PlayerId, List<PlayersAction> NextList, List<PlayersAction> PrevList, List<PlayersOnCourt> playersOnCourt, ref int LastMinute)
        {
            var playerRecord = Possessions.FirstOrDefault(t => t.PlayerId == PlayerId);
            if (action != null)
            {
                switch (action.ActionId)
                {
                    case 11: // free throw in
                        if (NextList[0].ActionId == 11) break;
                        if (NextList[0].ActionId == 10) break;

                        if (TechnicalFoulChecker(PrevList)) break;
                        
                        playerRecord.PlayerPos++;
                        PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                        break;
                    case 10: //free throw out
                        if (NextList[0].ActionId == 11) break;
                        if (NextList[0].ActionId == 10) break;

                        if (TechnicalFoulChecker(PrevList)) break;

                        if (NextList[0].ActionId == 5)
                        {
                            playerRecord.PlayerPos++;
                            PlayerTeamPossessionsEditor(Possessions, playersOnCourt, playerRecord.TeamId);
                        }
                        break;
                }
            }
            return true;
        }

        private bool TechnicalFoulChecker(List<PlayersAction> PrevList)
        {
            for (int i = 0; i < 12; i++)
            {
                if ((PrevList[i].ActionId == 27) || (PrevList[i].ActionId == 28)) return true;
                if (PrevList[i].ActionId == 8) return false;
            }
            return false;
        }
    }
}
