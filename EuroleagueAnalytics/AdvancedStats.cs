﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace EuroleagueAnalytics
{
    class AdvancedStats
    {
        public bool UpdateAdvancedPlayerStats(int GameId)
        {
            using (var context = new UsersContext())
            {                
                List<Boxscore> gameBoxscore = new List<Boxscore>();
                gameBoxscore = context.Boxscores.Where(t => t.GameId == GameId).ToList();
                int plPoss, teamPos, offPts, defPts, games;
                double offRtg, defRtg, plActions, gameActions, plPoints, plTurnovers;
                double offReb, teamOffReb, defReb, teamDefReb, totReb, teamTotReb; 
                foreach (Boxscore gamePlayer in gameBoxscore)
                {                    
                    int playerId = gamePlayer.PlayerId;
                    int teamId = gamePlayer.TeamId;
                    long MinTotal;
                    List<Boxscore> playerBoxscore = context.Boxscores.Where(t => t.PlayerId == playerId).ToList();                    
                    List<PlayerPosession> playerPossession = context.PlayerPossessions.Where(t => t.PlayerId == playerId).ToList();
                    List<PIE> playerPIE = context.PIEs.Where(t => t.PlayerId == playerId).ToList();
                    plPoss = (from t in playerPossession select t.PlayerPos).Sum();
                    teamPos = (from t in playerPossession select t.TeamPos).Sum();
                    offPts = (from t in playerBoxscore select t.PointsFw).Sum();
                    defPts = (from t in playerBoxscore select t.PointsAg).Sum();
                    // points, scored by player
                    plPoints = (from t in playerBoxscore select t.Pts).Sum();
                    // turnovers made by player
                    plTurnovers = (from t in playerBoxscore select t.Turnovers).Sum();
                    // games played by player
                    games = playerBoxscore.Where(s => s.BoxMin.Ticks > 0).Count();
                    plActions = (from t in playerPIE select t.PlayerActions).Sum();
                    gameActions = (from t in playerPIE select t.GameActions).Sum();
                    MinTotal = (from t in playerBoxscore select t.BoxMin.Ticks).Sum();
                    // rebounds
                    offReb = (from t in playerBoxscore select t.OR).Sum();
                    defReb = (from t in playerBoxscore select t.DR).Sum();
                    totReb = (from t in playerBoxscore select t.TR).Sum();
                    teamOffReb = (from t in playerBoxscore select t.AvailableOR).Sum();
                    teamDefReb = (from t in playerBoxscore select t.AvailableDR).Sum();
                    teamTotReb = (from t in playerBoxscore select t.TotalAvailableReb).Sum();
                    if (!context.PlayerAdvAdvancedStats.Any(t => t.PlayerId == playerId))
                    {
                        PlayerAdvAverageStats pStats = new PlayerAdvAverageStats();
                        pStats.PlayerId = playerId;
                        pStats.Games = games;
                        if (plPoss != 0)
                        {
                            pStats.PtsPos = plPoints / plPoss;
                            pStats.ToPos = plTurnovers / plPoss;
                        }
                        else
                        {
                            pStats.PtsPos = 0;
                            pStats.ToPos = 0;
                        }
                        if (teamPos != 0)
                        {
                            pStats.OffRtg = offRtg = 100 * (double)offPts / (double)teamPos;
                            pStats.DefRtg = defRtg = 100 * (double)defPts / (double)teamPos;
                            pStats.NetRtg = offRtg - defRtg;
                        }
                        else
                        {
                            pStats.OffRtg = 0;
                            pStats.DefRtg = 0;
                            pStats.NetRtg = 0;
                        }
                        
                        pStats.PlayerPossessions = plPoss;
                        pStats.TeamPossessions = teamPos;
                        if (teamPos != 0)
                            pStats.Usage = (100 * (double)plPoss) / (double)teamPos;
                        else
                            pStats.Usage = 0;
                        if (MinTotal != 0)
                            pStats.Pace = ((double)teamPos * 600000000 * 40) / MinTotal;
                        else
                            pStats.Pace = 0;
                        if (gameActions != 0)
                            pStats.PIE = (100 * (double)plActions) / (double)gameActions;
                        else
                            pStats.PIE = 0;
                        if (teamOffReb != 0)
                        {
                            pStats.OffRebPerc = (100 * (double)offReb) / (double)teamOffReb;
                        }
                        else
                            pStats.OffRebPerc = 0;
                        if (teamDefReb != 0)
                        {
                            pStats.DefRebPerc = (100 * (double)defReb) / (double)teamDefReb;
                        }
                        else
                            pStats.DefRebPerc = 0;
                        if (teamTotReb != 0)
                        {
                            pStats.TotRebPerc = (100 * (double)totReb) / (double)teamTotReb;
                        }
                        else
                            pStats.TotRebPerc = 0;
                        context.PlayerAdvAdvancedStats.Add(pStats); 
                    }
                    else
                    {
                        PlayerAdvAverageStats pStats = context.PlayerAdvAdvancedStats.Where(t => t.PlayerId == playerId).FirstOrDefault();
                        pStats.PlayerId = playerId;
                        pStats.Games = games;                        ;
                        if (plPoss != 0)
                        {
                            pStats.PtsPos = plPoints / plPoss;
                            pStats.ToPos = plTurnovers / plPoss;
                        }
                        else
                        {
                            pStats.PtsPos = 0;
                            pStats.ToPos = 0;
                        }
                        if (teamPos != 0)
                        {
                            pStats.OffRtg = offRtg = 100 * (double)offPts / (double)teamPos;
                            pStats.DefRtg = defRtg = 100 * (double)defPts / (double)teamPos;
                            pStats.NetRtg = offRtg - defRtg;
                        }
                        else
                        {
                            pStats.OffRtg = 0;
                            pStats.DefRtg = 0;
                            pStats.NetRtg = 0;
                        }
                        pStats.PlayerPossessions = plPoss;
                        pStats.TeamPossessions = teamPos;
                        if (teamPos != 0)
                            pStats.Usage = (100 * (double)plPoss) / (double)teamPos;
                        else
                            pStats.Usage = 0;
                        if (MinTotal != 0)
                            pStats.Pace = ((double)teamPos * 600000000 * 40) / MinTotal;
                        else
                            pStats.Pace = 0;
                        if (gameActions != 0)
                            pStats.PIE = (100 * (double)plActions) / (double)gameActions;
                        else
                            pStats.PIE = 0;
                        if (teamOffReb != 0)
                        {
                            pStats.OffRebPerc = (100 * (double)offReb) / (double)teamOffReb;
                        }
                        else
                            pStats.OffRebPerc = 0;
                        if (teamDefReb != 0)
                        {
                            pStats.DefRebPerc = (100 * (double)defReb) / (double)teamDefReb;
                        }
                        else
                            pStats.DefRebPerc = 0;
                        if (teamTotReb != 0)
                        {
                            pStats.TotRebPerc = (100 * (double)totReb) / (double)teamTotReb;
                        }
                        else
                            pStats.TotRebPerc = 0;
                    }
                }
                context.SaveChanges();
            }
            return true;
        }

        public bool UpdateTeamAdvancedStats(int GameId)
        {
            using (var context = new UsersContext())
            {
                List<TeamBoxscore> teamBoxscore = new List<TeamBoxscore>();
                teamBoxscore = context.TeamBoxscores.Where(t => t.GameId == GameId).ToList();
                foreach (TeamBoxscore gameTeam in teamBoxscore)
                {
                    double teamPos, teamPosSum, offRtg, defRtg, pie, poss;
                    double offReb = 0, totalOffReb = 0, defReb = 0, totalDefReb = 0, Reb = 0, totalReb = 0;
                    int TeamId = gameTeam.TeamId;
                    long MinTotal;
                    List<Team_Possession> Possession = context.Team_Possessions.Where(t => t.TeamId == TeamId).ToList();
                    List<PIE_Team> PIE = context.PIE_Teams.Where(t => t.TeamId == TeamId).ToList();

                    List<TeamBoxscore> teamGames = context.TeamBoxscores.Where(t => t.TeamId == TeamId).ToList();
                    foreach (TeamBoxscore gTeam in teamGames)
                    {
                        int gameId = gTeam.GameId;
                        List<TeamBoxscore> gGame = context.TeamBoxscores.Where(t => t.GameId == gameId).ToList();
                        int tempOffReb = (from t in gGame select t.OR).Sum();
                        int tempDefReb = (from t in gGame select t.DR).Sum();
                        int tempTotReb = (from t in gGame select t.TR).Sum();
                        totalOffReb = totalOffReb + tempOffReb;
                        totalDefReb = totalDefReb + tempDefReb;
                        totalReb = totalReb + tempTotReb;
                    }
                    offReb = (from t in teamGames select t.OR).Sum();
                    defReb = (from t in teamGames select t.DR).Sum();
                    Reb = (from t in teamGames select t.TR).Sum();

                    teamPos = (from t in Possession select t.Possessions).Average();
                    offRtg = (from t in Possession select t.OffRtg).Average();
                    defRtg = (from t in Possession select t.DefRtg).Average();
                    pie = (from t in PIE select t.PIE_Value).Average();
                    poss = (from t in Possession select t.Possessions).Average();
                    teamPosSum = (from t in Possession select t.Possessions).Sum();
                    MinTotal = (from t in Possession select t.Minutes).Sum();
                    if (!context.TeamAdvAverageStats.Any(t => t.TeamId == TeamId))
                    {
                        TeamAdvAverageStats tStats = new TeamAdvAverageStats();
                        tStats.TeamId = TeamId;
                        tStats.OffRtg = offRtg;
                        tStats.DefRtg = defRtg;
                        tStats.NetRtg = offRtg - defRtg;
                        tStats.PIE = pie;
                        tStats.Possessions = poss;
                        tStats.Pace = ((double)teamPosSum * 40) / MinTotal;
                        tStats.OffRebPerc = (100 * ((double)offReb)) / (double)totalOffReb;
                        tStats.DefRebPerc = (100 * ((double)defReb)) / (double)totalDefReb;
                        tStats.TotRebPerc = (100 * ((double)Reb)) / (double)totalReb;
                        context.TeamAdvAverageStats.Add(tStats);
                    }
                    else
                    {
                        TeamAdvAverageStats tStats = context.TeamAdvAverageStats.Where(t => t.TeamId == TeamId).FirstOrDefault();
                        tStats.TeamId = TeamId;
                        tStats.OffRtg = offRtg;
                        tStats.DefRtg = defRtg;
                        tStats.NetRtg = offRtg - defRtg;
                        tStats.PIE = pie;
                        tStats.Possessions = poss;
                        tStats.Pace = ((double)teamPosSum * 40) / MinTotal;
                        tStats.OffRebPerc = (100 * ((double)offReb)) / (double)totalOffReb;
                        tStats.DefRebPerc = (100 * ((double)defReb)) / (double)totalDefReb;
                        tStats.TotRebPerc = (100 * ((double)Reb)) / (double)totalReb;
                    }
                }
                context.SaveChanges();
            }
            return true;
        }       
    }
}
