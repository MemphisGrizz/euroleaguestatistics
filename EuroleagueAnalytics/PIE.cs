﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;
using EuroleagueReader;

namespace EuroleagueAnalytics
{
    public class PIE_Editor
    {
        public bool PIE_Import(int gameId)
        {
            using (var context = new UsersContext())
            {
                List<Boxscore> BoxscoreList = context.Boxscores.Where(t => t.GameId == gameId).ToList();
                List<PIE> PIEList = new List<PIE>();                 
                List<PIE_Team> PIE_Team_List = new List<PIE_Team>();
                CurrentGame curGame = new CurrentGame();
                curGame.HomeId = context.Games.Where(t => t.Id == gameId).FirstOrDefault().Team1;
                curGame.VisitId = context.Games.Where(t => t.Id == gameId).FirstOrDefault().Team2;
                double Points = 0, FGM2 = 0, FGM3 = 0, FTM = 0, FGA2 = 0, FGA3 = 0, FTA = 0, DR = 0, OR = 0, Assists = 0, Steals = 0, BlFv = 0, FlCm = 0, Turnovers = 0; 
                if (BoxscoreList != null)
                {
                    foreach (Boxscore boxscore in BoxscoreList)
                    {
                        int playerId = context.Players.FirstOrDefault(t => t.Id == boxscore.PlayerId).Id;
                        double playerActions = boxscore.Pts + (boxscore.FGM2 + boxscore.FGM3 + boxscore.FTM - boxscore.FGA2 - boxscore.FGA3 - boxscore.FTA) +
                               boxscore.DR + boxscore.OR * 0.5 + boxscore.Assists + boxscore.Steals + boxscore.BlFv * 0.5 - boxscore.FlCm - boxscore.Turnovers;
                        Points = Points + boxscore.Pts;
                        FGM2 = FGM2 + boxscore.FGM2;
                        FGM3 = FGM3 + boxscore.FGM3;
                        FTM = FTM + boxscore.FTM;
                        FGA2 = FGA2 + boxscore.FGA2;
                        FGA3 = FGA3 + boxscore.FGA3;
                        FTA = FTA + boxscore.FTA;
                        DR = DR + boxscore.DR;
                        OR = OR + boxscore.OR;
                        Assists = Assists + boxscore.Assists;
                        Steals = Steals + boxscore.Steals;
                        BlFv = BlFv + boxscore.BlFv;
                        FlCm = FlCm + boxscore.FlCm;
                        Turnovers = Turnovers + boxscore.Turnovers;
                        PIEList.Add(new PIE { PlayerId = playerId, GameId = gameId, TeamId = boxscore.TeamId,
                                              PlayerActions = playerActions, GameActions = 0, PIE_Value = 0 });                        
                    }

                    foreach (PIE pie in PIEList)
                    {
                        pie.GameActions = Points + (FGM2 + FGM3 + FTM - FGA2 - FGA3 - FTA) + DR + 0.5 * OR + Assists + Steals + 0.5 * BlFv - FlCm - Turnovers;
                        if (pie.GameActions != 0) pie.PIE_Value = pie.PlayerActions / pie.GameActions;
                        else pie.PIE_Value = 0;
                        context.PIEs.Add(pie);
                        context.SaveChanges();
                    }

                    double Home_Team_PIE = context.PIEs.Where(t => t.GameId == gameId).Where(t => t.TeamId == curGame.HomeId).Sum(t => t.PIE_Value);
                    PIE_Team_List.Add(new PIE_Team { GameId = gameId, TeamId = curGame.HomeId, PIE_Value = Home_Team_PIE });
                    double Away_Team_PIE = context.PIEs.Where(t => t.GameId == gameId).Where(t => t.TeamId == curGame.VisitId).Sum(t => t.PIE_Value);
                    PIE_Team_List.Add(new PIE_Team { GameId = gameId, TeamId = curGame.VisitId, PIE_Value = Away_Team_PIE });

                    foreach (PIE_Team pie_team in PIE_Team_List)
                    {
                        context.PIE_Teams.Add(pie_team);
                        context.SaveChanges();
                    }
                                  
                }
            }
            return true;
        }
    }
}