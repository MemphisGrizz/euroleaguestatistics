﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;
using EuroleagueReader;

namespace EuroleagueAnalytics
{
    public class MinutesEditor
    {
        public int PlayerId;
        public string LastStatus;
        public int LastStatusMin;        
    }

    public class PlusMinusEditor
    {
        public int PlayerId;
        public string LastStatus;
        public int InDifference;
        public int PointsFw;
        public int PointsAg;
    }

    public class AvailableRebEditor
    {
        public int PlayerId;
        public int TeamDefReb;
        public int TeamOffReb;
        public int TeamTotalReb;
    }

    public class CurrentGame
    {
        public int HomeId;
        public int VisitId;
    }

    public class TestFT
    {
        public int PlayerId;
        public int FT;
    }

    public class BoxscoreEditor
    {
        public void TeamStatsImport(int gameId, List<TeamList> Teams, GameCenter game)
        {            
            int teamId, oppTeamId = 0;
            using (var context = new UsersContext())
            {                
                List<TeamBoxscore> teamBoxscore = new List<TeamBoxscore>();
                if (Teams != null)
                {
                    foreach (TeamList team in Teams)
                    {
                        teamId = context.Teams.FirstOrDefault(t => t.Name == team.Team).Id;
                        if (team.Team == game.Team1)
                            oppTeamId = context.Teams.FirstOrDefault(t => t.Name == game.Team2).Id;
                        else
                            oppTeamId = context.Teams.FirstOrDefault(t => t.Name == game.Team1).Id;
                        teamBoxscore.Add(new TeamBoxscore{
                            TeamId = teamId, Assists = team.Assists, BlAg = team.BlAg, BlFv = team.BlFv, DR = team.DR, FGA2 = team.FGA2,
                            FGA3 = team.FGA3, FGM2 = team.FGM2, FGM3 = team.FGM3, FlCm = team.FlCm, FlRv = team.FlRv, FTA = team.FTA, 
                            FTM = team.FTM, Min = team.Min, OppTeamId = oppTeamId, OR = team.OR, PIR = team.PIR, Pts = team.Pts, Steals = team.Steals,
                            TR = team.TR, Turnovers = team.Turnovers, GameId = gameId
                        });                        
                    }
                }
                if (teamBoxscore != null)
                {
                    foreach (TeamBoxscore team in teamBoxscore)
                    {
                        context.TeamBoxscores.Add(team);
                        context.SaveChanges();
                    }
                }
            }
        }

        public void BoxscoreGenerator(int gameId, List<PlayerTeam> Players, GameCenter game)
        {
            int ActionNumber = 0;
            int curTeamId = 0, oppTeamId = 0;
            var context = new UsersContext();
            List<Boxscore> GameBoxscore = new List<Boxscore>();
            List<MinutesEditor> mEditor = new List<MinutesEditor>();
            List<PlusMinusEditor> pmEditor = new List<PlusMinusEditor>();
            List<AvailableRebEditor> rebEditor = new List<AvailableRebEditor>();
            CurrentGame curGame = new CurrentGame();
            List<PlayersAction> ActionList = context.PlayersActions.Where(t => t.GameId == gameId).ToList();
            curGame.HomeId = context.Games.Where(t => t.Id == gameId).FirstOrDefault().Team1;
            curGame.VisitId = context.Games.Where(t => t.Id == gameId).FirstOrDefault().Team2;

            foreach (PlayerTeam player in Players)
            {
                int playerId = context.Players.FirstOrDefault(t => t.Name == player.Name).Id;
                curTeamId = context.Teams.FirstOrDefault(t => t.Name == player.Team).Id;
                if (player.Team == game.Team1)
                    oppTeamId = context.Teams.FirstOrDefault(t => t.Name == game.Team2).Id;
                else
                    oppTeamId = context.Teams.FirstOrDefault(t => t.Name == game.Team1).Id;
                GameBoxscore.Add(new Boxscore
                {
                    PlayerId = playerId,
                    Min = 0,
                    BoxMin = player.Min,
                    Pts = 0, FGM2 = 0, FGA2 = 0, FGM3 = 0, FGA3 = 0, FTM = 0, FTA = 0,
                    OR = 0, DR = 0, TR = 0, Assists = 0, Steals = 0, Turnovers = 0,
                    BlFv = 0, BlAg = 0, FlCm = 0, FlRv = 0, PIR = 0, PlusMinus = 0,
                    TeamId = curTeamId,
                    OppTeamId = oppTeamId,
                    GameId = gameId, PointsFw = 0, PointsAg = 0, AvailableDR = 0,
                    AvailableOR = 0, TotalAvailableReb = 0  
                });
                mEditor.Add(new MinutesEditor
                {
                    PlayerId = playerId,
                    LastStatus = "0",
                    LastStatusMin = 0                    
                });
                pmEditor.Add(new PlusMinusEditor
                {
                    PlayerId = playerId,
                    LastStatus = "0",
                    InDifference = 0,
                    PointsFw = 0,
                    PointsAg = 0
                });
                rebEditor.Add(new AvailableRebEditor
                {
                    PlayerId = playerId,
                    TeamDefReb = 0,
                    TeamOffReb = 0,
                    TeamTotalReb = 0
                });
            }

            foreach (PlayersAction action in ActionList)
            {
                if (action.PlayerId != -1)
                {
                    // если подбор в защите или нападении - вызываем редактор подборов
                    if ((action.ActionId == 5) || (action.ActionId == 16))
                    {
                        GameBoxscore = ReboundsEditor(action, GameBoxscore, mEditor, curGame);
                    }
                    GameBoxscore = BoxscorePlayerEditor(action, GameBoxscore, mEditor, action.PlayerId);
                    GameBoxscore = PlusMinusPlayerEditor(action, GameBoxscore, pmEditor, action.PlayerId, curGame);
                    // стартовые пятерки
                    if ( (ActionNumber < 10) && (action.ActionId == 18) )
                        GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).Starter = true;
                    ActionNumber++;                    
                }
            }
            //проверка на игроков которые доиграли до конца
            foreach (MinutesEditor mAction in mEditor)
            {
                if (mAction.LastStatus == "In")
                GameBoxscore.FirstOrDefault(t => t.PlayerId == mAction.PlayerId).Min = GameBoxscore.FirstOrDefault(t => t.PlayerId == mAction.PlayerId).Min + (40 - mAction.LastStatusMin);
            }

            foreach (PlusMinusEditor pmAction in pmEditor)
            {
                int FinalScoreDifference, FinalFw, FinalAg;
                if (pmAction.LastStatus == "In")
                {
                    if (GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).TeamId == curGame.HomeId)
                    {
                        FinalScoreDifference = context.Games.FirstOrDefault(t => t.Id == gameId).Score1 - context.Games.FirstOrDefault(t => t.Id == gameId).Score2;
                        FinalFw = context.Games.FirstOrDefault(t => t.Id == gameId).Score1;
                        FinalAg = context.Games.FirstOrDefault(t => t.Id == gameId).Score2;
                        GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PlusMinus = GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PlusMinus + (FinalScoreDifference - pmAction.InDifference);
                        GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsFw = GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsFw + (FinalFw - pmAction.PointsFw);
                        GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsAg = GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsAg + (FinalAg - pmAction.PointsAg);
                    }
                    else
                    {
                        FinalScoreDifference = context.Games.FirstOrDefault(t => t.Id == gameId).Score2 - context.Games.FirstOrDefault(t => t.Id == gameId).Score1;
                        FinalFw = context.Games.FirstOrDefault(t => t.Id == gameId).Score2;
                        FinalAg = context.Games.FirstOrDefault(t => t.Id == gameId).Score1;
                        GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PlusMinus = GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PlusMinus + (FinalScoreDifference - pmAction.InDifference);
                        GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsFw = GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsFw + (FinalFw - pmAction.PointsFw);
                        GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsAg = GameBoxscore.FirstOrDefault(t => t.PlayerId == pmAction.PlayerId).PointsAg + (FinalAg - pmAction.PointsAg);
                    }
                }
            }

            PlusMinusFreeThrowCorrection(ActionList, GameBoxscore, curGame);

            foreach (Boxscore playerRecord in GameBoxscore)
            {
                if (playerRecord != null)
                {
                    playerRecord.TR = playerRecord.DR + playerRecord.OR;
                    int misses = (playerRecord.FGA2 - playerRecord.FGM2) + (playerRecord.FGA3 - playerRecord.FGM3) + (playerRecord.FTA - playerRecord.FTM);
                    playerRecord.PIR = playerRecord.Pts - misses + playerRecord.TR + playerRecord.Assists + playerRecord.Steals - playerRecord.Turnovers + playerRecord.BlFv - playerRecord.BlAg - playerRecord.FlCm + playerRecord.FlRv;
                    context.Boxscores.Add(playerRecord);
                    context.SaveChanges();  
                }
            }
            context.Dispose();            
        }

        private List<Boxscore> BoxscorePlayerEditor(PlayersAction action, List<Boxscore> GameBoxscore, List<MinutesEditor> mEditor, int PlayerId)
        {
            var playerRecord = GameBoxscore.FirstOrDefault(t => t.PlayerId == PlayerId);
            int TempMinutes = 0;
            MinutesEditor mAction = new MinutesEditor();
            if ((playerRecord != null) && (action != null))
            {
                switch (action.ActionId)
                {
                    case 1:
                        playerRecord.Turnovers = playerRecord.Turnovers + 1;
                        break;
                    case 2:
                        playerRecord.FGA2 = playerRecord.FGA2 + 1;
                        break;
                    case 3:
                        playerRecord.BlAg = playerRecord.BlAg + 1;
                        break;
                    case 4:
                        playerRecord.BlFv = playerRecord.BlFv + 1;
                        break;
                    case 5:
                        playerRecord.DR = playerRecord.DR + 1;
                        break;
                    case 6:
                        playerRecord.FGM2 = playerRecord.FGM2 + 1;
                        playerRecord.FGA2 = playerRecord.FGA2 + 1;
                        playerRecord.Pts = playerRecord.Pts + 2;
                        break;
                    case 7:
                        playerRecord.Assists = playerRecord.Assists + 1;
                        break;
                    case 8:
                        playerRecord.FlCm = playerRecord.FlCm + 1;
                        break;
                    case 9:
                        playerRecord.FlRv = playerRecord.FlRv + 1;
                        break;
                    case 10:
                        playerRecord.FTA = playerRecord.FTA + 1;
                        break;
                    case 11:
                        playerRecord.FTM = playerRecord.FTM + 1;
                        playerRecord.FTA = playerRecord.FTA + 1;
                        playerRecord.Pts = playerRecord.Pts + 1;
                        break;
                    case 12:
                        playerRecord.FGM2 = playerRecord.FGM2 + 1;
                        playerRecord.FGA2 = playerRecord.FGA2 + 1;
                        playerRecord.Pts = playerRecord.Pts + 2;
                        break;
                    case 13:
                        playerRecord.FGA3 = playerRecord.FGA3 + 1;
                        break;
                    case 14:
                        playerRecord.FGA2 = playerRecord.FGA2 + 1;
                        break;
                    case 15:
                        playerRecord.FGM2 = playerRecord.FGM2 + 1;
                        playerRecord.FGA2 = playerRecord.FGA2 + 1;
                        playerRecord.Pts = playerRecord.Pts + 2;
                        break;
                    case 16:
                        playerRecord.OR = playerRecord.OR + 1;
                        break;
                    case 17:
                        playerRecord.Steals = playerRecord.Steals + 1;
                        break;
                    case 19:
                        playerRecord.FlCm = playerRecord.FlCm + 1;
                        break;
                    case 20:
                        playerRecord.FGM3 = playerRecord.FGM3 + 1;
                        playerRecord.FGA3 = playerRecord.FGA3 + 1;
                        playerRecord.Pts = playerRecord.Pts + 3;
                        break;
                    case 27:
                        playerRecord.FlCm = playerRecord.FlCm + 1;
                        break;
                    case 28:
                        playerRecord.FlCm = playerRecord.FlCm + 1;
                        break;
                    case 18:
                        mEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).LastStatus = "In";
                        mEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).LastStatusMin = action.Minute;
                        break;
                    case 24:
                        mEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).LastStatus = "Out";
                        TempMinutes = action.Minute - mEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).LastStatusMin;
                        playerRecord.Min = playerRecord.Min + TempMinutes;
                        break;
                    default:
                        break;

                }
            }
            return GameBoxscore;
        }

        private List<Boxscore> PlusMinusPlayerEditor(PlayersAction action, List<Boxscore> GameBoxscore, List<PlusMinusEditor> pmEditor, int PlayerId, CurrentGame curGame)
        {
            int OutDifference, PointsFw, PointsAg; 
            var playerRecord = GameBoxscore.FirstOrDefault(t => t.PlayerId == PlayerId);
            if (action.ActionId == 18)
            {
                pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).LastStatus = "In";
                if (action.TeamId == curGame.HomeId)
                {
                    pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).InDifference = action.Score1 - action.Score2;
                    pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).PointsFw = action.Score1;
                    pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).PointsAg = action.Score2;
                }
                else
                {
                    pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).InDifference = action.Score2 - action.Score1;
                    pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).PointsFw = action.Score2;
                    pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).PointsAg = action.Score1;
                }
            }
            if (action.ActionId == 24)
            {
                pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).LastStatus = "Out";
                if (action.TeamId == curGame.HomeId)
                {
                    OutDifference = action.Score1 - action.Score2;
                    PointsFw = action.Score1;
                    PointsAg = action.Score2;
                }
                else
                {
                    OutDifference = action.Score2 - action.Score1;
                    PointsFw = action.Score2;
                    PointsAg = action.Score1;
                }
                playerRecord.PlusMinus = playerRecord.PlusMinus + (OutDifference - pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).InDifference);
                playerRecord.PointsFw = playerRecord.PointsFw + (PointsFw - pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).PointsFw);
                playerRecord.PointsAg = playerRecord.PointsAg + (PointsAg - pmEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).PointsAg);
            }
            return GameBoxscore;
        }

        private List<Boxscore> PlusMinusFreeThrowCorrection(List<PlayersAction> ActionList, List<Boxscore> GameBoxscore, CurrentGame curGame)
        {
            List<TestFT> tFT = new List<TestFT>();
            foreach (Boxscore pl in GameBoxscore)
            {
                tFT.Add(new TestFT { FT = 0, PlayerId = pl.PlayerId });
            }
            
            foreach (PlayersAction action in ActionList)
            {
                // 18 - In, 24 - Out
                if ((action.ActionId == 18) || (action.ActionId == 24))
                {
                    int index = action.Id;
                    PlayersAction nextAction = action;
                    while ((nextAction.ActionId == 18) || (nextAction.ActionId == 24) || (nextAction.ActionId == 10) || (nextAction.ActionId == 11) || (nextAction.ActionId == 25) || (nextAction.ActionId == 26))
                    {
                        if (nextAction.ActionId == 11)
                        {
                            if ((action.ActionId == 18) && (action.TeamId == curGame.HomeId) && (nextAction.TeamId == curGame.HomeId)) // In, Team A, FT Team A
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus--;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsFw--;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT--;
                            }
                            if ((action.ActionId == 18) && (action.TeamId == curGame.HomeId) && (nextAction.TeamId == curGame.VisitId)) // In, Team A, FT Team B
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus++;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsAg--;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT++;
                            }
                            if ((action.ActionId == 18) && (action.TeamId == curGame.VisitId) && (nextAction.TeamId == curGame.HomeId)) // In, Team B, FT Team A
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus++;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsAg--;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT++;
                            }
                            if ((action.ActionId == 18) && (action.TeamId == curGame.VisitId) && (nextAction.TeamId == curGame.VisitId)) // In, Team B, FT Team B
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus--;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsFw--;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT--;
                            }
                            if ((action.ActionId == 24) && (action.TeamId == curGame.HomeId) && (nextAction.TeamId == curGame.HomeId)) // Out, Team A, FT Team A
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus++;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsFw++;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT++;
                            }
                            if ((action.ActionId == 24) && (action.TeamId == curGame.HomeId) && (nextAction.TeamId == curGame.VisitId)) // Out, Team A, FT Team B
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus--;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsAg++;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT--;
                            }
                            if ((action.ActionId == 24) && (action.TeamId == curGame.VisitId) && (nextAction.TeamId == curGame.HomeId)) // Out, Team B, FT Team A
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus--;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsAg++;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT--;
                            }
                            if ((action.ActionId == 24) && (action.TeamId == curGame.VisitId) && (nextAction.TeamId == curGame.VisitId)) // Out, Team B, FT Team B
                            {
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PlusMinus++;
                                GameBoxscore.FirstOrDefault(t => t.PlayerId == action.PlayerId).PointsFw++;
                                tFT.FirstOrDefault(t => t.PlayerId == action.PlayerId).FT++;
                            }
                        }
                        index++;
                        nextAction = ActionList.FirstOrDefault(t => t.Id == index);
                    }
                }
            }
            return GameBoxscore;
        }

        private List<Boxscore> ReboundsEditor(PlayersAction action, List<Boxscore> GameBoxscore, List<MinutesEditor> mEditor, CurrentGame curGame)
        {
            foreach (Boxscore playerRecord in GameBoxscore)
            {
                if (playerRecord != null)
                {
                    if (mEditor.FirstOrDefault(t => t.PlayerId == playerRecord.PlayerId).LastStatus == "In")
                    {
                        if (((action.ActionId == 5) && (action.TeamId == curGame.HomeId)) || ((action.ActionId == 16) && (action.TeamId == curGame.VisitId))) // def rebound of home team or off rebound away team
                        {                        
                            if (playerRecord.TeamId == curGame.HomeId)
                            {
                                playerRecord.TotalAvailableReb++;
                                playerRecord.AvailableDR++;
                            }
                            if (playerRecord.TeamId == curGame.VisitId)
                            {
                                playerRecord.TotalAvailableReb++;
                                playerRecord.AvailableOR++;
                            }
                        }
                        if (((action.ActionId == 16) && (action.TeamId == curGame.HomeId)) || ((action.ActionId == 5) && (action.TeamId == curGame.VisitId))) // off rebound of home team or def rebound away team
                        {
                            if (playerRecord.TeamId == curGame.HomeId)
                            {
                                playerRecord.TotalAvailableReb++;
                                playerRecord.AvailableOR++;
                            }
                            if (playerRecord.TeamId == curGame.VisitId)
                            {
                                playerRecord.TotalAvailableReb++;
                                playerRecord.AvailableDR++;
                            }
                        }
                    }
                }
            }
            return GameBoxscore;
        }
    }
}
