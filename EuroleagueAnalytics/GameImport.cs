﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;
using EuroleagueReader;

namespace EuroleagueAnalytics
{
    public class GameImport
    {
        public void Import(string url)
        {
            int GameId;
            List<PlayAction> actions = new List<PlayAction>();
            List<PlayAction> starters = new List<PlayAction>();
            List<PlayerTeam> players = new List<PlayerTeam>();
            List<TeamList> teams = new List<TeamList>();
            BoxscoreEditor bEditor = new BoxscoreEditor();
            GameCenter game = new GameCenter();
            Stats st = new Stats();
            LogImport dbImport = new LogImport();
            PlayersImport pImport = new PlayersImport();
            PlayersStats psImport = new PlayersStats();
            TeamStats tsStats = new TeamStats();
            Pace pace = new Pace();
            PIE_Editor pie = new PIE_Editor();
            OffDefRatings ratings = new OffDefRatings();
            AdvancedStats advStats = new AdvancedStats();
            
            //читаем игру
            teams = st.TeamStatsReader(url);
            game = st.GameReader(url + "#!boxscore");
            //читаем состав
            players = st.RosterReader(url + "#!boxscore");
            //читаем playbyplay
            actions = st.GameLogReader(url + "#!playbyplay");
            //читаем пятерку
            starters = st.StartersReader(url + "#!boxscore");
            bool r1 = pImport.Import(players);
            GameId = dbImport.ActionsImport(actions, starters, game);
            bEditor.BoxscoreGenerator(GameId, players, game);
            bEditor.TeamStatsImport(GameId, teams, game);
            bool r2 = psImport.UpdatePlayersStats(GameId);
            bool r7 = tsStats.UpdateTeamStats(GameId);
            bool r3 = pace.PlayerPossesionsImport(GameId, players);
            bool r4 = pie.PIE_Import(GameId);
            bool r5 = ratings.RatingsCalculations(GameId);
            bool r8 = advStats.UpdateAdvancedPlayerStats(GameId);
            bool r9 = advStats.UpdateTeamAdvancedStats(GameId);
        }
    }
}
