﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace EuroleagueAnalytics
{
    // общая средняя статистика
    public class PlayersStats
    {
        public bool UpdatePlayersStats(int GameId)
        {
            double TempFG2 = 0, TempFGA2 = 0, TempFG3 = 0, TempFGA3 = 0, TempFT = 0, TempFTA = 0, TempPts = 0;
            using (var context = new UsersContext())
            {
                IEnumerable<Boxscore> GameBoxscore = context.Boxscores.Where(t => t.GameId == GameId);
                if (GameBoxscore != null)
                {                    
                    foreach (Boxscore player in GameBoxscore)
                    {
                        if (!context.PlayerAverageStats.Any(t => t.PlayerId == player.PlayerId))
                        {
                            PlayerAverageStats pStats = new PlayerAverageStats();
                            IEnumerable<Boxscore> boxscore = context.Boxscores.Where(t => t.PlayerId == player.PlayerId);
                            double doubleAverageTicks = boxscore.Average(t => t.BoxMin.Ticks);
                            long longAverageTicks = Convert.ToInt64(doubleAverageTicks);
                            pStats.PlayerId = player.PlayerId;
                            pStats.Name = context.Players.Where(t => t.Id == player.PlayerId).FirstOrDefault().Name;
                            pStats.MPG = new TimeSpan(longAverageTicks);
                            pStats.PPG = (from t in boxscore select t.Pts).Average();                            
                            pStats.RPG = (from t in boxscore select t.TR).Average();
                            pStats.ORPG = (from t in boxscore select t.OR).Average();
                            pStats.DRPG = (from t in boxscore select t.DR).Average();
                            pStats.APG = (from t in boxscore select t.Assists).Average();
                            pStats.BPG = (from t in boxscore select t.BlFv).Average();
                            pStats.BAPG = (from t in boxscore select t.BlAg).Average();
                            pStats.SPG = (from t in boxscore select t.Steals).Average();
                            pStats.TPG = (from t in boxscore select t.Turnovers).Average();
                            pStats.FPG = (from t in boxscore select t.FlCm).Average();
                            pStats.FAPG = (from t in boxscore select t.FlRv).Average();
                            TempPts = boxscore.Sum(t => t.Pts);
                            // 2FG%
                            TempFG2 = boxscore.Sum(t => t.FGM2);
                            TempFGA2 = boxscore.Sum(t => t.FGA2);
                            pStats.FG2M = boxscore.Sum(t => t.FGM2);
                            pStats.FG2A = boxscore.Sum(t => t.FGA2);
                            if (TempFGA2 != 0)
                            {                                
                                pStats.FG2 = TempFG2 / TempFGA2;
                            }
                            else
                                pStats.FG2 = 0.0;
                            // 3FG%
                            TempFG3 = boxscore.Sum(t => t.FGM3);
                            TempFGA3 = boxscore.Sum(t => t.FGA3);
                            pStats.FG3M = boxscore.Sum(t => t.FGM3);
                            pStats.FG3A = boxscore.Sum(t => t.FGA3);
                            if (TempFGA3 != 0)
                            {                                
                                pStats.FG3 = TempFG3 / TempFGA3;
                            }
                            else
                                pStats.FG3 = 0.0;
                            // FT%
                            TempFT = boxscore.Sum(t => t.FTM);
                            TempFTA = boxscore.Sum(t => t.FTA);
                            pStats.FTM = boxscore.Sum(t => t.FTM);
                            pStats.FTA = boxscore.Sum(t => t.FTA);
                            if (TempFTA != 0)
                            {                                
                                pStats.FT = TempFT / TempFTA;
                            }
                            else
                                pStats.FT = 0.0;
                            // EFG
                            if ((TempFGA2 + TempFGA3) != 0)
                            {
                                pStats.EFG = (TempFG2 + 1.5 * TempFG3) / (TempFGA2 + TempFGA3);
                            }
                            else
                                pStats.EFG = 0;
                            // TS
                            if ((TempFGA2 + TempFGA3 + TempFTA) != 0)
                            {
                                pStats.TS = TempPts / (2 * (TempFGA2 + TempFGA3 + 0.44 * TempFTA));
                            }
                            else
                                pStats.TS = 0;
                            pStats.PIR = (from t in boxscore select t.PIR).Average();
                            pStats.PlusMinus = (from t in boxscore select t.PlusMinus).Average();
                            context.PlayerAverageStats.Add(pStats);                            
                        }
                        else
                        {
                            PlayerAverageStats pStats = context.PlayerAverageStats.Where(t => t.PlayerId == player.PlayerId).FirstOrDefault();
                            IEnumerable<Boxscore> boxscore = context.Boxscores.Where(t => t.PlayerId == player.PlayerId);
                            double doubleAverageTicks = boxscore.Average(t => t.BoxMin.Ticks);
                            long longAverageTicks = Convert.ToInt64(doubleAverageTicks);
                            pStats.PlayerId = player.PlayerId;
                            pStats.Name = context.Players.Where(t => t.Id == player.PlayerId).FirstOrDefault().Name;
                            pStats.MPG = new TimeSpan(longAverageTicks);
                            pStats.PPG = (from t in boxscore select t.Pts).Average();                           
                            pStats.RPG = (from t in boxscore select t.TR).Average();
                            pStats.ORPG = (from t in boxscore select t.OR).Average();
                            pStats.DRPG = (from t in boxscore select t.DR).Average();
                            pStats.APG = (from t in boxscore select t.Assists).Average();
                            pStats.BPG = (from t in boxscore select t.BlFv).Average();
                            pStats.BAPG = (from t in boxscore select t.BlAg).Average();
                            pStats.SPG = (from t in boxscore select t.Steals).Average();
                            pStats.FPG = (from t in boxscore select t.FlCm).Average();
                            pStats.FAPG = (from t in boxscore select t.FlRv).Average();
                            pStats.TPG = (from t in boxscore select t.Turnovers).Average();
                            TempPts = boxscore.Sum(t => t.Pts);
                            // 2FG%
                            TempFG2 = boxscore.Sum(t => t.FGM2);
                            TempFGA2 = boxscore.Sum(t => t.FGA2);
                            pStats.FG2M = boxscore.Sum(t => t.FGM2);
                            pStats.FG2A = boxscore.Sum(t => t.FGA2);
                            if (TempFGA2 != 0)
                            {
                                pStats.FG2 = TempFG2 / TempFGA2;
                            }
                            else
                                pStats.FG2 = 0.0;
                            // 3FG%
                            TempFG3 = boxscore.Sum(t => t.FGM3);
                            TempFGA3 = boxscore.Sum(t => t.FGA3);
                            pStats.FG3M = boxscore.Sum(t => t.FGM3);
                            pStats.FG3A = boxscore.Sum(t => t.FGA3);
                            if (TempFGA3 != 0)
                            {
                                pStats.FG3 = TempFG3 / TempFGA3;
                            }
                            else
                                pStats.FG3 = 0.0;
                            // FT%
                            TempFT = boxscore.Sum(t => t.FTM);
                            TempFTA = boxscore.Sum(t => t.FTA);
                            pStats.FTM = boxscore.Sum(t => t.FTM);
                            pStats.FTA = boxscore.Sum(t => t.FTA);
                            if (TempFTA != 0)
                            {
                                pStats.FT = TempFT / TempFTA;
                            }
                            else
                                pStats.FT = 0.0;
                            if ((TempFGA2 + TempFGA3) != 0)
                            {
                                pStats.EFG = (TempFG2 + 1.5 * TempFG3) / (TempFGA2 + TempFGA3);
                            }
                            else
                                pStats.EFG = 0;
                            if ((TempFGA2 + TempFGA3 + TempFTA) != 0)
                            {
                                pStats.TS = TempPts / (2 * (TempFGA2 + TempFGA3 + 0.44 * TempFTA));
                            }
                            else
                                pStats.TS = 0;
                            pStats.PIR = (from t in boxscore select t.PIR).Average();
                            pStats.PlusMinus = (from t in boxscore select t.PlusMinus).Average();
                        }
                    }
                }                    
                else
                {
                    return false;
                }
                context.SaveChanges();
                return true;  
            }
        }
    }
}
