﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace EuroleagueAnalytics
{
    class TeamStats
    {
        public bool UpdateTeamStats(int GameId)
        {
            double TempFG, TempFGA;
            using (var context = new UsersContext())
            {
                IEnumerable<TeamBoxscore> Teams = context.TeamBoxscores.Where(t => t.GameId == GameId);
                if (Teams != null)
                {
                    foreach (TeamBoxscore team in Teams)
                    {
                        if (!context.TeamAverageStats.Any(t => t.TeamId == team.TeamId))
                        {
                            TeamAverageStats tStats = new TeamAverageStats();
                            IEnumerable<TeamBoxscore> tboxscore = context.TeamBoxscores.Where(t => t.TeamId == team.TeamId);
                            tStats.TeamId = team.TeamId;
                            tStats.Name = context.Teams.Where(t => t.Id == team.TeamId).FirstOrDefault().Name;
                            tStats.PPG = (from t in tboxscore select t.Pts).Average();
                            tStats.RPG = (from t in tboxscore select t.TR).Average();
                            tStats.APG = (from t in tboxscore select t.Assists).Average();
                            tStats.BPG = (from t in tboxscore select t.BlFv).Average();
                            tStats.SPG = (from t in tboxscore select t.Steals).Average();
                            tStats.PIR = (from t in tboxscore select t.PIR).Average();
                            if (tboxscore.Sum(t => t.FGA2) != 0)
                            {
                                TempFG = tboxscore.Sum(t => t.FGM2);
                                TempFGA = tboxscore.Sum(t => t.FGA2);
                                tStats.FG = TempFG / TempFGA;
                            }
                            else
                                tStats.FG = 0.0;
                            if (tboxscore.Sum(t => t.FGA3) != 0)
                            {
                                TempFG = tboxscore.Sum(t => t.FGM3);
                                TempFGA = tboxscore.Sum(t => t.FGA3);
                                tStats.FG3 = TempFG / TempFGA;
                            }
                            else
                                tStats.FG3 = 0.0;
                            if (tboxscore.Sum(t => t.FTA) != 0)
                            {
                                TempFG = tboxscore.Sum(t => t.FTM);
                                TempFGA = tboxscore.Sum(t => t.FTA);
                                tStats.FT = TempFG / TempFGA;
                            }
                            else
                                tStats.FT = 0.0;
                            context.TeamAverageStats.Add(tStats);
                        }
                        else
                        {
                            TeamAverageStats tStats = context.TeamAverageStats.Where(t => t.TeamId == t.TeamId).FirstOrDefault();
                            IEnumerable<TeamBoxscore> tboxscore = context.TeamBoxscores.Where(t => t.TeamId == team.TeamId);
                            tStats.TeamId = team.TeamId;
                            tStats.Name = context.Teams.Where(t => t.Id == team.TeamId).FirstOrDefault().Name;
                            tStats.PPG = (from t in tboxscore select t.Pts).Average();
                            tStats.RPG = (from t in tboxscore select t.TR).Average();
                            tStats.APG = (from t in tboxscore select t.Assists).Average();
                            tStats.BPG = (from t in tboxscore select t.BlFv).Average();
                            tStats.SPG = (from t in tboxscore select t.Steals).Average();
                            tStats.PIR = (from t in tboxscore select t.PIR).Average();
                            if (tboxscore.Sum(t => t.FGA2) != 0)
                            {
                                TempFG = tboxscore.Sum(t => t.FGM2);
                                TempFGA = tboxscore.Sum(t => t.FGA2);
                                tStats.FG = TempFG / TempFGA;
                            }
                            else
                                tStats.FG = 0.0;
                            if (tboxscore.Sum(t => t.FGA3) != 0)
                            {
                                TempFG = tboxscore.Sum(t => t.FGM3);
                                TempFGA = tboxscore.Sum(t => t.FGA3);
                                tStats.FG3 = TempFG / TempFGA;
                            }
                            else
                                tStats.FG3 = 0.0;
                            if (tboxscore.Sum(t => t.FTA) != 0)
                            {
                                TempFG = tboxscore.Sum(t => t.FTM);
                                TempFGA = tboxscore.Sum(t => t.FTA);
                                tStats.FT = TempFG / TempFGA;
                            }
                            else
                                tStats.FT = 0.0;

                        }
                    }
                }
                else
                {
                    return false;
                }
                context.SaveChanges();
            }
            return true;
        }
    }
}
