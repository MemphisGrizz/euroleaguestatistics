﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{

    public class ResultsInfoReader
    {
        public string Title;
        public ResultsGroupReader[] Groups;
    }

    public class ResultsGroupReader
    {
        public string Title;
        public ResultReader[] Results;
    }

    public class ResultReader
    {
        public int GameId;
        public string Team1Name, Team2Name;
        public int Team1Score, Team2Score;
        public string HomeLogo, AwayLogo;
        public string Date;
    }

    public class ResultsReader
    {
        public List<ResultsGroupReader> GetTopResults()
        {
            List<ResultsGroupReader> resultGroups = new List<ResultsGroupReader>();
            List<ResultReader> resultsReader = new List<ResultReader>();
            using (var context = new UsersContext())
            {
                var pGames = (from t in context.Games
                              join c in context.Teams on t.Team1 equals c.Id
                              join u in context.Teams on t.Team2 equals u.Id
                              orderby t.date descending
                              select new
                              {
                                  GameId = c.Id,
                                  HomeTeam = c.ShortName,
                                  AwayTeam = u.ShortName,
                                  HomeScore = t.Score1,
                                  AwayScore = t.Score2,
                                  Date = t.date,
                                  Round = t.Round,
                                  Stage = t.Stage,
                                  HomeLogo = c.Logo,
                                  AwayLogo = u.Logo
                              }).Take(8);

                var Rounds = pGames.Select(t => t.Round).Distinct();
                foreach (var round in Rounds)
                {                   
                    var RoundGame = pGames.Where(t => t.Round == round);
                    resultsReader.Clear();
                    foreach (var game in RoundGame)
                    {
                        resultsReader.Add ( new ResultReader
                        {
                             GameId = game.GameId,
                             Team1Name = game.HomeTeam,
                             Team2Name = game.AwayTeam,
                             Team1Score = game.HomeScore,
                             Team2Score = game.AwayScore,
                             HomeLogo = "/Images/Logo/" + game.HomeLogo,
                             AwayLogo = "/Images/Logo/" + game.AwayLogo,
                             Date = game.Date.ToShortDateString()
                        });                        
                    }

                    resultGroups.Add(new ResultsGroupReader
                    {
                        Title = round,
                        Results = resultsReader.ToArray()
                    });
                }
            }
            return resultGroups;
        }

        public List<ResultsInfoReader> GetResults()
        {
            List<ResultsInfoReader> resultStages = new List<ResultsInfoReader>();
            List<ResultsGroupReader> resultGroups = new List<ResultsGroupReader>();
            List<ResultReader> resultsReader = new List<ResultReader>();
            using (var context = new UsersContext())
            {
                var pGames = (from t in context.Games
                              join c in context.Teams on t.Team1 equals c.Id
                              join u in context.Teams on t.Team2 equals u.Id
                              orderby t.Stage descending, t.date descending                             
                              select new
                              {
                                  GameId = t.Id,
                                  HomeTeam = c.Name,
                                  AwayTeam = u.Name,
                                  HomeScore = t.Score1,
                                  AwayScore = t.Score2,
                                  Date = t.date,
                                  Round = t.Round,
                                  Stage = t.Stage,
                                  HomeLogo = c.Logo,
                                  AwayLogo = u.Logo
                              });

                var Stages = pGames.Select(t => t.Stage).Distinct();
                foreach (var stage in Stages)
                {
                    var Rounds = pGames.Where(s => s.Stage == stage).Select(t => t.Round).Distinct();                    
                    resultGroups.Clear();
                    foreach (var round in Rounds)
                    {                   
                        var RoundGame = pGames.Where(s => s.Stage == stage).Where(t => t.Round == round);
                        resultsReader.Clear();
                        foreach (var game in RoundGame)
                        {
                            resultsReader.Add ( new ResultReader
                            {
                                GameId = game.GameId,
                                Team1Name = game.HomeTeam,
                                Team2Name = game.AwayTeam,
                                Team1Score = game.HomeScore,
                                Team2Score = game.AwayScore,
                                HomeLogo = "/Images/Logo/" + game.HomeLogo,
                                AwayLogo = "/Images/Logo/" + game.AwayLogo,
                                Date = game.Date.ToShortDateString()
                            });                        
                        }
                    
                        resultGroups.Add(new ResultsGroupReader
                        {
                            Title = round,
                            Results = resultsReader.ToArray()
                        });
                    }

                    resultStages.Add (new ResultsInfoReader
                        {
                            Title = stage,
                            Groups = resultGroups.ToArray()
                        });
                }
            }

            return resultStages;
        }
    }
}
