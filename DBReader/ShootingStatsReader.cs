﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class ShootinglPlayerStats
    {
        public ShootingStatsRow[] Items;
    }

    public class ShootingStatsRow
    {
        public string Name, Team, Image, Logo;
        public double FG2, FG2A, FG3, FG3A, FT, FTA, EFG, TS;
    }

    public class ShootingStatsReader
    {
        public ShootinglPlayerStats GetShootingStats()
        {
            List<ShootingStatsRow> items = new List<ShootingStatsRow>();
            ShootinglPlayerStats stats = new ShootinglPlayerStats();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAverageStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.PIR descending
                              select new
                              {
                                  c.Name,
                                  c.Image,
                                  c.Team,
                                  u.Logo,
                                  t.FG2,
                                  t.FG2A,
                                  t.FG3,
                                  t.FG3A,
                                  t.FT,
                                  t.FTA,
                                  t.EFG,
                                  t.TS                                  
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new ShootingStatsRow
                        {
                            Name = pStat.Name,
                            Team = pStat.Team,
                            Image = "/Images/Players/" + pStat.Image,
                            Logo = "/Images/Logo/" + pStat.Logo,
                            FG2 = 100* Math.Round(pStat.FG2, 3),
                            FG2A = pStat.FG2A,
                            FG3 = 100 * Math.Round(pStat.FG3, 3),
                            FG3A = pStat.FG3A,
                            FT = 100 * Math.Round(pStat.FT, 3),
                            FTA = pStat.FTA,
                            EFG = 100 * Math.Round(pStat.EFG, 3),
                            TS = 100 * Math.Round(pStat.TS, 3)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }            
    }
}
