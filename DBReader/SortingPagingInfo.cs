﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class SortingPagingInfo
    {
        public string SortField { get; set; }
        public string SortDirection { get; set; }
        public string Team { get; set; }
        public int PageCount { get; set; }
        public int CurrentPageIndex { get; set; }
        public int PageSize { get; set; }        

        public string[] GetTeamsList()
        {
            //List<string> TeamList = null;
            using (var context = new UsersContext())
            {
                var TeamList = context.Teams.Select(t => t.Name).Distinct();
                return TeamList.ToArray();
            }            
        }
    }    
}
