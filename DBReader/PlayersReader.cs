﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    enum PlayerPos
    {
        PG = 1, SG = 2, SF = 3, PF = 4, C = 5
    }

    public class PlayersTable
    {
        public TeamsPlayers[] Items;
    }

    public class TeamsPlayers
    {
        public string Team, Logo;
        public Player[] PlayersList;
    }

    public class Player
    {
        public string Name, Image, Position, Age, Height;
        public int Number, Id;
    }

    public class PlayerStat
    {
        public string Name, Image, Logo, Team, Pos, Age, Height;
        public double Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR, PlusMinus, EFG, TS;
        public double FG2Perc, FG3Perc, FTPerc, Usage, OffRtg, DefRtg, NetRtg, PIE, PPOS, ToPos, ORebPerc, DRebPers, TRebPerc, plPos, teamPosPl;
        public int games, playerId;
        public string BoxMin;
    }

    public class PlayerGames
    {
        public int GameId, Team1, Team2, Score1, Score2;
        public string Name, Image, Logo, Team, OppTeam, Place, Result, Date;
        public double Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR, PlusMinus;
        public string BoxMin;
    }

    public class PlayerGamesList
    {
        public PlayerGames[] pGames;
    }

    public class PlayerChartData
    {
        public string Name;
        public PlayerChartRow[] Items;
    }

    public class PlayerChartRow
    {
        public string Date;
        public int Pts;
        public int PIR;
        public int Min;
    }

    public class PlayersReader
    {
        public PlayersTable GetPlayersTeamsList()
        {
            PlayersTable pTable = new PlayersTable();
            List<TeamsPlayers> tPlayers = new List<TeamsPlayers>();
            List<Player> players = new List<Player>();
            using (var context = new UsersContext())
            {
                var pPlayers = (from t in context.Players
                                join c in context.Teams on t.Team equals c.Name
                                orderby t.Position ascending
                                select new
                                {
                                    t.Id,
                                    t.Name,
                                    t.Image,
                                    t.Position,
                                    t.Team,
                                    t.Height,
                                    t.Age,
                                    t.Number,
                                    c.Logo
                                });
                var Teams = pPlayers.Select(t => t.Team).Distinct();

                foreach (var team in Teams)
                {
                    var plTeam = pPlayers.Where(t => t.Team == team).OrderBy(t => t.Position);
                    players.Clear();
                    foreach (var pl in plTeam)
                    {
                        players.Add(new Player
                        {
                            Id = pl.Id,
                            Name = pl.Name,
                            Image = "/Images/Players/" + pl.Image,
                            Age = pl.Age,
                            Height = pl.Height,
                            Number = pl.Number,
                            Position = ((PlayerPos)pl.Position).ToString()
                        });
                    }
                    tPlayers.Add(new TeamsPlayers
                    {
                        Team = plTeam.FirstOrDefault().Team,
                        Logo = "/Images/Logo/" + plTeam.FirstOrDefault().Logo,
                        PlayersList = players.ToArray()
                    });
                }

            }
            pTable.Items = tPlayers.ToArray();
            return pTable;
        }

        public PlayerStat GetPlayerStats(int PlayerId)
        {
            PlayerStat Player = new PlayerStat();
            using (var context = new UsersContext())
            {
                var player = (from t in context.PlayerAverageStats.Where(t => t.PlayerId == PlayerId)
                              join s in context.PlayerAdvAdvancedStats.Where(t => t.PlayerId == PlayerId) on t.PlayerId equals s.PlayerId
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              select new
                              {
                                  Team = u.Name,
                                  c.Image,
                                  c.Name,
                                  c.Position,
                                  c.Age,
                                  c.Height,
                                  u.Logo,
                                  s.Games,
                                  t.MPG,
                                  t.PPG,
                                  t.FG2M,
                                  t.FG2A,
                                  t.FG2, //%
                                  t.FG3M,
                                  t.FG3A,
                                  t.FG3, //%
                                  t.FTM,
                                  t.FTA,
                                  t.FT, //%
                                  t.RPG,
                                  t.ORPG,
                                  t.DRPG,
                                  t.APG,
                                  t.SPG,
                                  t.TPG,
                                  t.BPG,
                                  t.BAPG,
                                  t.FPG,
                                  t.FAPG,
                                  t.PIR,
                                  t.PlusMinus,
                                  t.EFG,
                                  t.TS,
                                  s.OffRtg,
                                  s.DefRtg,
                                  s.NetRtg,
                                  s.OffRebPerc,
                                  s.DefRebPerc,
                                  s.TotRebPerc,
                                  s.PlayerPossessions,
                                  s.TeamPossessions,
                                  s.PtsPos,
                                  s.ToPos,
                                  s.Usage,
                                  s.PIE
                              }).FirstOrDefault();

                if (player != null)
                {
                    Player.Logo = "/Images/Logo/" + player.Logo; Player.Team = player.Team; Player.Image = "/Images/Players/" + player.Image;
                    Player.Name = player.Name; Player.Pos = ((PlayerPos)player.Position).ToString();
                    Player.games = player.Games; Player.BoxMin = player.MPG.ToString(); Player.Pts = player.PPG;
                    Player.Age = player.Age; Player.Height = player.Height;

                    if (player.Games != 0)
                    {
                        Player.FG2M = (double)player.FG2M / player.Games;
                        Player.FG3M = (double)player.FG3M / player.Games;
                        Player.FG2A = (double)player.FG2A / player.Games;
                        Player.FG3A = (double)player.FG3A / player.Games;
                        Player.FTA = (double)player.FTA / player.Games;
                        Player.FTM = (double)player.FTM / player.Games;
                        Player.plPos = (double)player.PlayerPossessions / player.Games;
                        Player.teamPosPl = (double)player.TeamPossessions / player.Games;
                        
                    }
                    else
                    {
                        Player.FG2M = 0;
                        Player.FG3M = 0;
                        Player.FG2A = 0;
                        Player.FG3A = 0;
                        Player.FTA = 0;
                        Player.FTM = 0;
                        Player.plPos = 0;
                        Player.teamPosPl = 0;
                    }                    
                    Player.FG2Perc = 100 * player.FG2; 
                    Player.FG3Perc = 100* player.FG3; 
                     Player.FTPerc = 100 * player.FT; Player.TR = player.RPG; Player.OR = player.ORPG;
                    Player.DR = player.DRPG; Player.AS = player.APG; Player.ST = player.SPG; Player.TO = player.TPG; Player.BlFv = player.BPG;
                    Player.BlAg = player.BAPG; Player.FlCm = player.FPG; Player.FlRv = player.FAPG; Player.PIR = player.PIR; Player.PlusMinus = player.PlusMinus;
                    Player.EFG = 100* player.EFG; Player.TS = 100*player.TS; Player.OffRtg = player.OffRtg; Player.DefRtg = player.DefRtg; Player.NetRtg = player.NetRtg;
                    Player.ORebPerc = player.OffRebPerc; Player.DRebPers = player.DefRebPerc; Player.TRebPerc = player.TotRebPerc;
                    Player.playerId = PlayerId; 
                    Player.PPOS = player.PtsPos; Player.ToPos = player.ToPos; Player.Usage = player.Usage; Player.PIE = player.PIE;
                }
            }
            return Player;
        }

        public PlayerGamesList GetPlayerGames(int PlayerId)
        {
            PlayerGamesList pList = new PlayerGamesList();
            List<PlayerGames> plGames = new List<PlayerGames>();
            string _Place, _Result = "", _Opp;
            using (var context = new UsersContext())
            {
                var tGames = (from t in context.Boxscores.Where(t => t.PlayerId == PlayerId)
                              select new
                              {
                                  t.Pts
                              });
                var pGames = (from t in context.Boxscores.Where(t => t.PlayerId == PlayerId)
                              join s in context.Games on t.GameId equals s.Id
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby s.date descending
                              select new
                              {
                                  Team = u.Name,
                                  c.Image,
                                  c.Name,
                                  u.Logo,
                                  GameId = s.Id,
                                  s.Team1,
                                  s.Team2,
                                  s.Score1,
                                  s.Score2,
                                  t.BoxMin,
                                  t.Pts,
                                  t.FGM2,
                                  t.FGA2,
                                  t.FGM3, 
                                  t.FGA3,
                                  t.FTM,
                                  t.FTA, 
                                  t.OR,
                                  t.DR,
                                  t.TR, 
                                  t.Assists,
                                  t.Steals,
                                  t.Turnovers,
                                  t.BlFv,
                                  t.BlAg,
                                  t.FlCm,
                                  t.FlRv,
                                  t.PIR,
                                  t.PlusMinus,
                                  s.date
                              });
                
                foreach (var game in pGames)
                {
                    int teamId = context.Teams.FirstOrDefault(t => t.Name == game.Team).Id;
                    if (teamId == game.Team1) _Place = "vs";
                    else _Place = "at";
                    if ((_Place == "vs") && (game.Score1 > game.Score2)) _Result = "W";
                    if ((_Place == "vs") && (game.Score2 > game.Score1)) _Result = "L";
                    if ((_Place == "at") && (game.Score2 > game.Score1)) _Result = "W";
                    if ((_Place == "at") && (game.Score1 > game.Score2)) _Result = "L";

                    if (_Place == "vs") 
                        _Opp = context.Teams.FirstOrDefault(t => t.Id == game.Team2).ShortName;
                    else
                        _Opp = context.Teams.FirstOrDefault(t => t.Id == game.Team1).ShortName;
                    plGames.Add(new PlayerGames
                    {
                        Team = game.Team,
                        Image = game.Image,
                        Logo = game.Logo,
                        GameId = game.GameId,
                        BoxMin = game.BoxMin.ToString(),
                        Pts = game.Pts,
                        FG2M = game.FGM2,
                        FG2A = game.FGA2,
                        FG3M = game.FGM3,
                        FG3A = game.FGA3,
                        FTM = game.FTM,
                        FTA = game.FTA,
                        OR = game.OR,
                        DR = game.DR,
                        TR = game.TR, //%
                        AS = game.Assists,
                        ST = game.Steals,
                        TO = game.Turnovers,
                        BlFv = game.BlFv,
                        BlAg = game.BlAg,
                        FlCm = game.FlCm,
                        FlRv = game.FlRv,
                        PIR = game.PIR,
                        PlusMinus = game.PlusMinus , 
                        Score1 = game.Score1,
                        Score2 = game.Score2,
                        Team1 = game.Team1,
                        Team2 = game.Team2,
                        Place = _Place,
                        Result = _Result,
                        OppTeam = _Opp,
                        Name = game.Name,
                        Date = game.date.ToShortDateString()
                    });
                }
                pList.pGames = plGames.ToArray();
            }
            return pList;
        }

        public PlayerChartData GetPlayerChart(int PlayerId)
        {
            List<PlayerChartRow> items = new List<PlayerChartRow>();
            PlayerChartData stats = new PlayerChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.Boxscores.Where(t => t.PlayerId == PlayerId)
                              join c in context.Players on t.PlayerId equals c.Id
                              join s in context.Games on t.GameId equals s.Id
                              orderby s.date descending
                              select new
                              {
                                  c.Name,
                                  t.Pts,
                                  t.PIR,
                                  t.BoxMin,
                                  s.date
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new PlayerChartRow
                        {
                            Date = pStat.date.ToString("yyyy-MM-dd"),
                            Pts = pStat.Pts,
                            PIR = pStat.PIR,
                            Min = pStat.BoxMin.Minutes
                        });
                    }
                }
                stats.Items = items.OrderBy(t => t.Date).ToArray();
            }
            return stats;
        }
    }
}