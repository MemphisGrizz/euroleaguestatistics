﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class AdvPlayerStats
    {
        public AdvStatsRow[] Items;
    }

    public class AdvStatsRow
    {
        public string Name, Team, Image, Logo;
        public double OffRtg, DefRtg, NetRtg, PIE, OffRebPerc, DefRebPerc, TotRebPerc, Games;
    }

    public class AdvStatsReader
    {

        public AdvPlayerStats GetAdvancedStats()
        {
            List<AdvStatsRow> items = new List<AdvStatsRow>();
            AdvPlayerStats stats = new AdvPlayerStats();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.NetRtg descending
                              select new
                              {
                                  c.Name,
                                  c.Image,
                                  c.Team,
                                  u.Logo,
                                  t.OffRtg,
                                  t.DefRtg,
                                  t.NetRtg,
                                  t.PIE,
                                  t.OffRebPerc,
                                  t.DefRebPerc,
                                  t.TotRebPerc,
                                  t.Games
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new AdvStatsRow
                        {
                            Name = pStat.Name,
                            Team = pStat.Team,
                            Image = "/Images/Players/" + pStat.Image,
                            Logo = "/Images/Logo/" + pStat.Logo,
                            Games = pStat.Games,
                            OffRtg = Math.Round(pStat.OffRtg, 2),
                            DefRtg = Math.Round(pStat.DefRtg, 2),
                            NetRtg = Math.Round(pStat.NetRtg, 2),
                            PIE = Math.Round(pStat.PIE, 2),
                            OffRebPerc = Math.Round(pStat.OffRebPerc, 2),
                            DefRebPerc = Math.Round(pStat.DefRebPerc, 2),
                            TotRebPerc = Math.Round(pStat.TotRebPerc, 2)                            
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }
    }
}
