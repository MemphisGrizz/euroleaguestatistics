﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class TraditionalPlayerStats
    {
        public PlayerStatsRow[] Items;
    }

    public class PlayerStatsRow
    {
        public string Name, Team, Image, Logo, MPG;
        public double PIR, PPG, RPG, APG, SPG, BPG, PLUS_MINUS;
    }

   
    public class TraditionalStatsReader
    {
        public TraditionalPlayerStats GetPlayerStats()
        {
            List<PlayerStatsRow> items = new List<PlayerStatsRow>();
            TraditionalPlayerStats stats = new TraditionalPlayerStats();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAverageStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.PIR descending
                              select new { c.Name, c.Image, c.Team, u.Logo, 
                                           t.PIR, t.PPG, t.RPG, t.APG, t.SPG, t.BPG, t.MPG, t.PlusMinus });
           

                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new PlayerStatsRow
                        {
                            Name = pStat.Name,
                            Team = pStat.Team,
                            Image = "/Images/Players/" + pStat.Image,
                            Logo = "/Images/Logo/" + pStat.Logo,
                            PIR = Math.Round(pStat.PIR, 2),
                            PPG = Math.Round(pStat.PPG, 2),
                            RPG = Math.Round(pStat.RPG, 2),
                            APG = Math.Round(pStat.APG, 2),
                            SPG = Math.Round(pStat.SPG, 2),
                            BPG = Math.Round(pStat.BPG, 2),
                            MPG = pStat.MPG.ToString(),
                            PLUS_MINUS = Math.Round(pStat.PlusMinus, 2)                          
                        });
                    }
                }
                stats.Items = items.ToArray();
             }
            return stats;
        }            
    }
}
