﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class TrTeamStats
    {
        public TrTeamStatsRow[] Items;
    }

    public class TrTeamStatsRow
    {
        public string Name, Logo;
        public double PPG, RPG, APG, SPG, BPG, FG2, FG3, FT;
    }

    public class AdvTeamStats
    {
        public AdvTeamStatsRow[] Items;
    }

    public class AdvTeamStatsRow
    {
        public string Name, Logo;
        public double PIR, OffRtg, DefRtg, NetRtg, PIE, Pace, OffRebPerc, DefRebPerc, TotRebPerc;
    }

    public class OffRtgChartStats
    {
        public OffRtgChartRow[] Items;
    }

    public class OffRtgChartRow
    {
        public string Name;
        public double OffRtg, DefRtg;
    }

    public class PaceChartStats
    {
        public PaceChartRow[] Items;
    }

    public class PaceChartRow
    {
        public string Name;
        public double Pace;
    }



    public class TeamsStatsReader
    {
        public TrTeamStats GetTeamStats()
        {
            List<TrTeamStatsRow> items = new List<TrTeamStatsRow>();
            TrTeamStats stats = new TrTeamStats();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamAverageStats
                              join c in context.Teams on t.TeamId equals c.Id                             
                              orderby t.PPG descending
                              select new
                              {
                                  c.Name,
                                  c.Logo,                                  
                                  t.PPG,
                                  t.RPG,
                                  t.APG,
                                  t.SPG,
                                  t.BPG,
                                  t.FG,
                                  t.FG3,
                                  t.FT
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TrTeamStatsRow
                        {
                            Name = pStat.Name,
                            Logo = "/Images/Logo/" + pStat.Logo,                     
                            PPG = Math.Round(pStat.PPG, 2),
                            RPG = Math.Round(pStat.RPG, 2),
                            APG = Math.Round(pStat.APG, 2),
                            SPG = Math.Round(pStat.SPG, 2),
                            BPG = Math.Round(pStat.BPG, 2),
                            FG2 = Math.Round(100 * pStat.FG, 2),
                            FG3 = Math.Round(100 * pStat.FG3, 2),
                            FT = Math.Round(100 * pStat.FT, 2)                            
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public AdvTeamStats GetTeamAdvStats()
        {
            List<AdvTeamStatsRow> items = new List<AdvTeamStatsRow>();
            AdvTeamStats stats = new AdvTeamStats();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamAdvAverageStats
                              join u in context.TeamAverageStats on t.TeamId equals u.TeamId
                              join c in context.Teams on t.TeamId equals c.Id
                              orderby u.PIR descending
                              select new
                              {
                                  c.Name,
                                  c.Logo,
                                  u.PIR,
                                  t.OffRtg,
                                  t.DefRtg,
                                  t.NetRtg,
                                  t.PIE,
                                  t.Pace,
                                  t.OffRebPerc,
                                  t.DefRebPerc,
                                  t.TotRebPerc
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new AdvTeamStatsRow
                        {
                            Name = pStat.Name,
                            Logo = "/Images/Logo/" + pStat.Logo,
                            PIR = Math.Round(pStat.PIR, 2),
                            OffRtg = Math.Round(pStat.OffRtg, 2),
                            DefRtg = Math.Round(pStat.DefRtg, 2),
                            NetRtg = Math.Round(pStat.NetRtg, 2),
                            PIE = Math.Round(100 * pStat.PIE, 2),
                            Pace = Math.Round(pStat.Pace, 2),
                            OffRebPerc = Math.Round(pStat.OffRebPerc, 2),
                            DefRebPerc = Math.Round(pStat.DefRebPerc, 2),
                            TotRebPerc = Math.Round(pStat.TotRebPerc, 2)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public OffRtgChartStats GetTeamsOffRtg()
        {
            List<OffRtgChartRow> items = new List<OffRtgChartRow>();
            OffRtgChartStats stats = new OffRtgChartStats();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamAdvAverageStats                              
                              join c in context.Teams on t.TeamId equals c.Id
                              orderby t.OffRtg descending
                              select new
                              {
                                  c.ShortName,                                  
                                  t.OffRtg, 
                                  t.DefRtg
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new OffRtgChartRow
                        {
                            Name = pStat.ShortName,
                            OffRtg = Math.Round(pStat.OffRtg, 2),
                            DefRtg = Math.Round(pStat.DefRtg, 2)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public PaceChartStats GetTeamsPace()
        {
            List<PaceChartRow> items = new List<PaceChartRow>();
            PaceChartStats stats = new PaceChartStats();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamAdvAverageStats
                              join c in context.Teams on t.TeamId equals c.Id
                              orderby t.Pace descending
                              select new
                              {
                                  c.ShortName,
                                  t.Pace                                  
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new PaceChartRow
                        {
                            Name = pStat.ShortName,
                            Pace = Math.Round(pStat.Pace, 2)                            
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }


    }
}
