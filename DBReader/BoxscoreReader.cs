﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;
using System.Globalization;

namespace DBReader
{
    public class GameReader
    {        
        public GameInfo Game;
        public BoxscoreGroupReader[] Teams;
    }

    public class GameInfo
    {
        public string HomeTeam, AwayTeam, Date, Round, Stage, HomeLogo, AwayLogo;
        public int HomeScore, AwayScore;
    }

    public class BoxscoreGroupReader
    {
        public string Title;
        public PlBoxscore[] Players;
        public TBoxscore Teams;        
    }

    public class PlBoxscore
    {
        public int GameId;
        public string BoxMin;
        public bool Starter;
        public string Name, Logo;
        public int Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR, PlusMinus;
        public double FG2Perc, FG3Perc, FTPerc, Usage, OffRtg, DefRtg, NetRtg, PIE, PPOS;
        public int plPos, teamPosPl; 
        public string Date;
    }

    public class TBoxscore
    {
        public int GameId;
        public int Min;
        public string Name, Logo;
        public int Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR;
        public double FG2Perc, FG3Perc, FTPerc, OffRtg, DefRtg, NetRtg, PIE, PPOS;
        public double Poss; 
        public string Date;
    }

    public class BoxscoreChartData
    {
        public string Name;
        public BoxscoreChartRow[] Items;
    }

    public class BoxscoreChartRow
    {
        public string Name;
        public double Data;
    }


    public class BoxscoreReader
    {
        public GameReader GetGameBoxscore(int gameId)
        {
            GameReader gmReader = new GameReader();
            GameInfo gmInfo = new GameInfo();
            List<BoxscoreGroupReader> teamBox = new List<BoxscoreGroupReader>();
            List<PlBoxscore> plBoxscore = new List<PlBoxscore>();
            TBoxscore tBoxscore = new TBoxscore();
            double tPPOS;
            
            using (var context = new UsersContext())
            {               
                var pStats = (from t in context.Boxscores.Where(p => p.GameId == gameId)
                              join c in context.PlayerPossessions.Where(p => p.GameId == gameId) on t.PlayerId equals c.PlayerId
                              join u in context.PIEs.Where(p => p.GameId == gameId) on t.PlayerId equals u.PlayerId
                              join s in context.Teams on t.TeamId equals s.Id
                              join q in context.Players on t.PlayerId equals q.Id
                              orderby t.PIR descending
                              select new
                              {
                                  Player = q.Name, s.Logo, t.Starter, Team = s.Name, s.ShortName, t.BoxMin, t.PlusMinus,
                                  t.Pts, t.FGM2, t.FGA2, t.FGM3, t.FGA3, t.FTM, t.FTA, t.OR, t.DR, t.TR, t.Assists, t.Steals, t.Turnovers, t.BlFv,
                                  t.BlAg, t.FlCm, t.FlRv, t.PIR, c.PlayerPos, c.TeamPos, c.OffRtg, c.DefRtg, c.NetRtg, u.PIE_Value
                              });

                var tStats = (from t in context.TeamBoxscores.Where(p => p.GameId == gameId)
                              join c in context.Team_Possessions.Where(p => p.GameId == gameId) on t.TeamId equals c.TeamId
                              join u in context.PIE_Teams.Where(p => p.GameId == gameId) on t.TeamId equals u.TeamId
                              join s in context.Teams on t.TeamId equals s.Id                       
                              select new
                              {
                                  Team = s.Name, s.Logo, s.ShortName, t.Min,
                                  t.Pts, t.FGM2, t.FGA2, t.FGM3, t.FGA3, t.FTM, t.FTA, t.OR, t.DR, t.TR, t.Assists, t.Steals, t.Turnovers, t.BlFv,
                                  t.BlAg, t.FlCm, t.FlRv, t.PIR, c.Possessions, c.OffRtg, c.DefRtg, c.NetRtg, u.PIE_Value
                              });
                var pGames = (from t in context.Games.Where( t => t.Id == gameId)
                              join c in context.Teams on t.Team1 equals c.Id
                              join u in context.Teams on t.Team2 equals u.Id                              
                              select new
                              {
                                  GameId = c.Id,
                                  HomeTeam = c.Name,
                                  AwayTeam = u.Name,
                                  HomeScore = t.Score1,
                                  AwayScore = t.Score2,
                                  Date = t.date,
                                  Round = t.Round,
                                  Stage = t.Stage,
                                  HomeLogo = c.Logo,
                                  AwayLogo = u.Logo
                              });

                if (pGames != null)
                {
                    gmInfo.HomeTeam = pGames.FirstOrDefault().HomeTeam;
                    gmInfo.AwayTeam = pGames.FirstOrDefault().AwayTeam;
                    gmInfo.HomeScore = pGames.FirstOrDefault().HomeScore;
                    gmInfo.AwayScore = pGames.FirstOrDefault().AwayScore;
                    gmInfo.Round = pGames.FirstOrDefault().Round;
                    gmInfo.Stage = pGames.FirstOrDefault().Stage;
                    gmInfo.HomeLogo = "/Images/Logo/" + pGames.FirstOrDefault().HomeLogo;
                    gmInfo.AwayLogo = "/Images/Logo/" + pGames.FirstOrDefault().AwayLogo;
                    gmInfo.Date = pGames.FirstOrDefault().Date.ToString("MMMM dd, yyyy", CultureInfo.InvariantCulture);
                }
                
                    foreach (var team in tStats)
                    {
                        plBoxscore.Clear();
                        var curPlayers = pStats.Where(t => t.Team == team.Team);
                        var curTeam = tStats.Where(t => t.Team == team.Team);
                        if (curPlayers != null)
                        {
                            foreach (var player in curPlayers)
                            {
                                double fg2, fg3, ft, ppos, usage;                                
                                if (player.FGA2 != 0) fg2 = (100 * (double)player.FGM2) / player.FGA2;
                                else fg2 = 0;
                                if (player.FGA3 != 0) fg3 = (100 * (double)player.FGM3) / player.FGA3;
                                else fg3 = 0;
                                if (player.FTA != 0) ft = (100 * (double)player.FTM) / player.FTA;
                                else ft = 0;
                                if (player.PlayerPos != 0) ppos = (double)player.Pts / player.PlayerPos;
                                else ppos = 0;
                                if (player.TeamPos != 0) usage = (100 * (double)player.PlayerPos) / player.TeamPos;
                                else usage = 0;
                                plBoxscore.Add(new PlBoxscore
                                {
                                    AS = player.Assists,
                                    BlAg = player.BlAg,
                                    BlFv = player.BlFv,
                                    BoxMin = String.Format("{0}:{1:D2}", player.BoxMin.Minutes, player.BoxMin.Seconds),
                                    DefRtg = player.DefRtg,  
                                    DR = player.DR,
                                    FG2M = player.FGM2,
                                    FG2A = player.FGA2,
                                    FG2Perc = fg2,
                                    FG3A = player.FGA3,
                                    FG3M = player.FGM3,
                                    FG3Perc = fg3,
                                    FTM = player.FTM,
                                    FTA = player.FTA,
                                    FTPerc = ft,
                                    FlCm = player.FlCm,
                                    FlRv = player.FlRv,
                                    Name = player.Player,
                                    NetRtg = player.NetRtg,
                                    OffRtg = player.OffRtg,
                                    OR = player.OR,
                                    PIE = player.PIE_Value,
                                    PIR = player.PIR,
                                    plPos = player.PlayerPos,
                                    PlusMinus = player.PlusMinus,
                                    PPOS = ppos,
                                    Pts = player.Pts,
                                    ST = player.Steals,
                                    teamPosPl  = player.TeamPos,
                                    TO = player.Turnovers,
                                    TR = player.TR,
                                    Usage = usage,
                                    Starter = player.Starter,
                                    Logo = "/Images/Logo/" + player.Logo
                                });
                            }
                        }

                       
                        if (team.Possessions != 0)
                            tPPOS = team.Pts / team.Possessions;
                        else
                            tPPOS = 0;
                        

                        teamBox.Add(new BoxscoreGroupReader
                        {
                            Title = team.Team,
                            Players = plBoxscore.ToArray(),
                            Teams = new TBoxscore
                            {
                                 AS = team.Assists,
                                 BlAg = team.BlAg,
                                 BlFv = team.BlFv,
                                 Min = team.Min,
                                 DefRtg = team.DefRtg,
                                 DR = team.DR,
                                 FG2A = team.FGA2,
                                 FG2M = team.FGM2,
                                 FG3A = team.FGA3,
                                 FG3M = team.FGM3,
                                 FTA = team.FTA,
                                 FTM = team.FTM,
                                 FlCm = team.FlCm,
                                 FlRv = team.FlRv,
                                 Logo = "/Images/Logo/" + team.Logo,
                                 Name = team.Team,
                                 NetRtg = team.NetRtg,
                                 OffRtg = team.OffRtg,
                                 OR = team.OR,
                                 PIE = team.PIE_Value,
                                 PIR = team.PIR,
                                 Poss = team.Possessions,
                                 Pts = team.Pts,
                                 ST = team.Steals,
                                 TO = team.Turnovers,
                                 TR = team.TR,
                                 PPOS = tPPOS
                            }
                        });
                    }
                    gmReader.Teams = teamBox.ToArray();
                    gmReader.Game = gmInfo;
            }
            return gmReader;
        }

        public BoxscoreChartData GetChart2FG(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamBoxscores.Where (t => t.GameId == gameId)
                              join c in context.Teams on t.TeamId equals c.Id                              
                              select new
                              {
                                  c.ShortName,
                                  t.FGM2,
                                  t.FGA2
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = Math.Round(100 * (double) pStat.FGM2 / (double)pStat.FGA2, 1)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public BoxscoreChartData GetChart3FG(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamBoxscores.Where(t => t.GameId == gameId)
                              join c in context.Teams on t.TeamId equals c.Id
                              select new
                              {
                                  c.ShortName,
                                  t.FGM3,
                                  t.FGA3
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = Math.Round(100 * (double)pStat.FGM3 / (double)pStat.FGA3, 1)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public BoxscoreChartData GetChartFT(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamBoxscores.Where(t => t.GameId == gameId)
                              join c in context.Teams on t.TeamId equals c.Id
                              select new
                              {
                                  c.ShortName,
                                  t.FTM,
                                  t.FTA
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = Math.Round(100 * (double)pStat.FTM / (double)pStat.FTA, 1)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public BoxscoreChartData GetChartReb(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamBoxscores.Where(t => t.GameId == gameId)
                              join c in context.Teams on t.TeamId equals c.Id
                              select new
                              {
                                  c.ShortName,
                                  t.TR                                  
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = pStat.TR
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public BoxscoreChartData GetChartPIR(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamBoxscores.Where(t => t.GameId == gameId)
                              join c in context.Teams on t.TeamId equals c.Id
                              select new
                              {
                                  c.ShortName,
                                  t.PIR
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = pStat.PIR
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public BoxscoreChartData GetChartNET(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.Team_Possessions.Where(t => t.GameId == gameId)
                              join c in context.Teams on t.TeamId equals c.Id
                              select new
                              {
                                  c.ShortName,
                                  t.NetRtg
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = Math.Round(pStat.NetRtg, 1)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public BoxscoreChartData GetChartPPOS(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamBoxscores.Where(t => t.GameId == gameId)
                              join u in context.Team_Possessions.Where(p => p.GameId == gameId) on t.TeamId equals u.TeamId
                              join c in context.Teams on t.TeamId equals c.Id
                              select new
                              {
                                  c.ShortName,
                                  t.Pts,
                                  u.Possessions
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = Math.Round((double)pStat.Pts / (double)pStat.Possessions, 2)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }

        public BoxscoreChartData GetChartPIE(int gameId)
        {
            List<BoxscoreChartRow> items = new List<BoxscoreChartRow>();
            BoxscoreChartData stats = new BoxscoreChartData();
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PIE_Teams.Where(t => t.GameId == gameId)                             
                              join c in context.Teams on t.TeamId equals c.Id
                              select new
                              {
                                  c.ShortName,
                                  t.PIE_Value                                  
                              });


                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new BoxscoreChartRow
                        {
                            Name = pStat.ShortName,
                            Data = Math.Round(pStat.PIE_Value, 4)
                        });
                    }
                }
                stats.Items = items.ToArray();
            }
            return stats;
        }
    }
}
