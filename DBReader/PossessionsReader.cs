﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    
        public class PosPlayerStats
        {
            public PosStatsRow[] Items;
        }

        public class PosStatsRow
        {
            public string Name, Team, Image, Logo;
            public double PlayerPossessions, TeamPossessions, Pace, PtsPos, ToPos, Usage, Games;
        }

        public class PossessionsReader
        {

            public PosPlayerStats GetPossessionStats()
            {
                List<PosStatsRow> items = new List<PosStatsRow>();
                PosPlayerStats stats = new PosPlayerStats();
                using (var context = new UsersContext())
                {
                    var pStats = (from t in context.PlayerAdvAdvancedStats
                                  join c in context.Players on t.PlayerId equals c.Id
                                  join u in context.Teams on c.Team equals u.Name
                                  orderby t.Usage descending
                                  select new
                                  {
                                      c.Name,
                                      c.Image,
                                      c.Team,
                                      u.Logo,
                                      t.Games,
                                      t.PlayerPossessions,
                                      t.TeamPossessions,
                                      t.Pace,
                                      t.PtsPos,
                                      t.ToPos,
                                      t.Usage                                      
                                  });


                    if (pStats != null)
                    {
                        foreach (var pStat in pStats)
                        {
                            items.Add(new PosStatsRow
                            {
                                Name = pStat.Name,
                                Team = pStat.Team,
                                Image = "/Images/Players/" + pStat.Image,
                                Logo = "/Images/Logo/" + pStat.Logo,
                                Games = pStat.Games,
                                PlayerPossessions = pStat.PlayerPossessions,
                                TeamPossessions = pStat.TeamPossessions,
                                Pace = Math.Round(pStat.Pace, 2),
                                PtsPos = Math.Round(pStat.PtsPos, 2),
                                ToPos = Math.Round(pStat.ToPos, 2),
                                Usage = Math.Round(pStat.Usage, 2)                                
                            });
                        }
                    }
                    stats.Items = items.ToArray();
                }
                return stats;
            }
        }
}
