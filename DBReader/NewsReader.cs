﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class NewsTable
    {
        public NewsItem[] Items;
    }

    public class NewsItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Image_big { get; set; }
        public string Image_small { get; set; }
    }

    public class NewsReader
    {
        public NewsTable GetNewsList()
        {
            NewsTable nTable = new NewsTable();
            List<NewsItem> items = new List<NewsItem>();
            using (var context = new UsersContext())
            {
                var pNews = (from t in context.News
                             orderby t.Order descending
                             select new
                             {
                                 t.Id,
                                 t.Image,
                                 t.Image_small,
                                 t.Name,
                                 t.Text
                             }).Take(4);

                foreach (var news in pNews)
                {
                    items.Add(new NewsItem
                    {
                        Id = news.Id,
                        Image_big = "/Images/News/" + news.Image,
                        Image_small = "/Images/News/" + news.Image_small,
                        Name = news.Name,
                        Text = news.Text
                    });
                }
            }
            nTable.Items = items.ToArray();
            return nTable;
        }

        public NewsItem GetNewById(int Id)
        {
            NewsItem item = new NewsItem();
            using (var context = new UsersContext())
            {
                var pNews = (from t in context.News
                             where t.Id == Id
                             orderby t.Order descending
                             select new
                             {
                                 t.Id,
                                 t.Image,
                                 t.Image_small,
                                 t.Name,
                                 t.Text
                             }).FirstOrDefault();

                if (pNews != null)
                {
                    item.Id = pNews.Id;
                    item.Image_big = "/Images/News/" + pNews.Image;
                    item.Image_small = "/Images/News/" + pNews.Image_small;
                    item.Name = pNews.Name;
                    item.Text = pNews.Text;
                }
            }
            
            return item;
        }
    }
}
