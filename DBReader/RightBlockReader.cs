﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class RightBlockReader
    {
        public TopBlockReader GetTop25PlusMinus()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "Plus/Minus";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAverageStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.PlusMinus descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.PlusMinus }).Take(20);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = NameConcat(pStat.Name),
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.PlusMinus.ToString(("0.00")).Replace(",", ".")
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTop25EFG()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "Effective Scorers";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAverageStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.TS descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.TS }).Take(20);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = NameConcat(pStat.Name),
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = (100 * Math.Round(pStat.TS, 3)).ToString(("0.00")).Replace(",", ".") + "%"
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTop25Pace()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.Pace descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.Pace }).Take(20);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = NameConcat(pStat.Name),
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.Pace.ToString(("0.00")).Replace(",", ".")
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTop25RebPercPl()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.TotRebPerc descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.TotRebPerc }).Take(20);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = NameConcat(pStat.Name),
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.TotRebPerc.ToString(("0.00")).Replace(",", ".") + "%"
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTop25FG2Teams()
        {
            TopBlockReader teamBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            teamBlock.Title = "";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamAverageStats
                              join c in context.Teams on t.TeamId equals c.Id                              
                              orderby t.FG descending
                              select new { c.ShortName, c.Logo, t.FG }).Take(20);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.ShortName,
                            Image = "/Images/Logo/" + pStat.Logo,                            
                            Value = (100 * pStat.FG).ToString(("0.00")).Replace(",", ".") + "%"
                        });
                    }
                }
                teamBlock.Items = items.ToArray();
            }
            return teamBlock;
        }

        public TopBlockReader GetTop25NetRtgTeams()
        {
            TopBlockReader teamBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            teamBlock.Title = "";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamAdvAverageStats
                              join c in context.Teams on t.TeamId equals c.Id
                              orderby t.NetRtg descending
                              select new { c.ShortName, c.Logo, t.NetRtg }).Take(20);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.ShortName,
                            Image = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.NetRtg.ToString(("0.00")).Replace(",", ".") + "%"
                        });
                    }
                }
                teamBlock.Items = items.ToArray();
            }
            return teamBlock;
        }

        private static string NameConcat(string Name)
        {
            int Index = Name.IndexOf(",");
            Name = Name.Substring(0, Index + 3) + ".";
            return Name;
        }
    }
}
