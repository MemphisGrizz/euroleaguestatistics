﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace DBReader
{
    public class StandingsInfoReader
    {
        public TeamsGroupReader[] TeamGroups;
    }

    public class TeamsGroupReader
    {
        public string Name;
        public TeamInfoReader[] Teams;
    }

    public class TeamInfoReader
    {
        public string Logo, Name;
        public int Wins, Loses, Perc;
    }

    public class StandingsReader
    {
        public List<TeamsGroupReader> GetStandingsInfo()
        {
            List<TeamsGroupReader> standings = new List<TeamsGroupReader>();
            List<TeamInfoReader> teams = new List<TeamInfoReader>();
            using (var context = new UsersContext())
            {
                var pStangings = (from t in context.TeamStangings
                                  join c in context.Teams on t.TeamId equals c.Id
                                  select new
                                  {
                                      Position = t.Position,
                                      Name = c.ShortName,
                                      Logo = c.Logo,
                                      Wins = t.Win,
                                      Loses = t.Lost,
                                      Percentage = t.Pct,
                                      Group = t.Group
                                  });
                var groups = pStangings.Select(t => t.Group).Distinct();
                foreach (var group in groups)
                {
                    var groupStanding = pStangings.Where(t => t.Group == group).OrderBy(t => t.Position);
                    teams.Clear();
                    foreach (var team in groupStanding)
                    {
                        teams.Add(new TeamInfoReader
                        {
                            Logo = "/Images/Logo/" + team.Logo,
                            Name = team.Name,
                            Wins = team.Wins,
                            Loses = team.Loses,
                            Perc = Convert.ToInt32(team.Percentage)
                        });
                    }

                    standings.Add(new TeamsGroupReader
                    {
                        Name = "Group " + group,
                        Teams = teams.ToArray()
                    });

                }
            }
            return standings;
        }
    }
}
