﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

public class TopBlockReader
{
    public string Title;
    public TopBlockItemReader[] Items;
}

public class TopBlockItemReader
{
    public string Name, Image, Value;
    public string TeamName, TeamImage;    
}

namespace DBReader
{
    public class TopReader
    {
        public TopBlockReader GetTopPts()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "POINTS";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAverageStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.PPG descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.PPG }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.PPG.ToString(("0.0"))                            
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopReb()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "REBOUNDS";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAverageStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.RPG descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.RPG }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.RPG.ToString(("0.0"))
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopAssists()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "ASSISTS";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAverageStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.APG descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.APG }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.APG.ToString(("0.0"))
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopOffRtg()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "OFF RATING";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.OffRtg descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.OffRtg }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.OffRtg.ToString(("0.0"))
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopDefRtg()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "DEF RATING";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              where t.DefRtg > 0
                              orderby t.DefRtg ascending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.DefRtg }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.DefRtg.ToString(("0.0"))
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopNetRtg()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "NET RATING";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.NetRtg descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.NetRtg }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.NetRtg.ToString(("0.0"))
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopPIE()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "PIE";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.PIE descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.PIE }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.PIE.ToString(("0.0"))
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopPace()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "PACE";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.TeamAdvAverageStats
                              join c in context.Teams on t.TeamId equals c.Id
                              orderby t.Pace descending
                              select new { c.Name, c.Logo, t.Pace }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.Pace.ToString(("0.0"))
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }

        public TopBlockReader GetTopUsage()
        {
            TopBlockReader playerBlock = new TopBlockReader();
            List<TopBlockItemReader> items = new List<TopBlockItemReader>();
            playerBlock.Title = "USAGE %";
            using (var context = new UsersContext())
            {
                var pStats = (from t in context.PlayerAdvAdvancedStats
                              join c in context.Players on t.PlayerId equals c.Id
                              join u in context.Teams on c.Team equals u.Name
                              orderby t.Usage descending
                              select new { c.Name, c.Image, c.Team, u.Logo, t.Usage }).Take(5);
                if (pStats != null)
                {
                    foreach (var pStat in pStats)
                    {
                        items.Add(new TopBlockItemReader
                        {
                            Name = pStat.Name,
                            Image = "/Images/Players/" + pStat.Image,
                            TeamName = pStat.Team,
                            TeamImage = "/Images/Logo/" + pStat.Logo,
                            Value = pStat.Usage.ToString(("0.0")) + "%"
                        });
                    }
                }
                playerBlock.Items = items.ToArray();
            }
            return playerBlock;
        }
    }
}
