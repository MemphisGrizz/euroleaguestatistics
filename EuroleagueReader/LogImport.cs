﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace EuroleagueReader
{
    public class LogImport
    {
        // recieving List from Euroleague site, conver to PlayersAction and Save to DB 
        public int ActionsImport(List<PlayAction> Actions, List<PlayAction> Starters, GameCenter Game)
        {
            if (Actions == null) return 0;
            Game dbGame = new Game();
            int GameId, Score1, Score2;
            string Team1, Team2;
            PlayersAction dbAction = new PlayersAction();
                        
            using (var context = new UsersContext())
            {
                try
                {
                    dbGame.date = Game.GameDate;
                    dbGame.Round = Game.Round;
                    dbGame.Score1 = Game.Score1;
                    dbGame.Score2 = Game.Score2;
                    dbGame.Season = Game.Season;
                    dbGame.Stage = Game.Stage;
                    dbGame.Team1 = context.Teams.FirstOrDefault(t => t.Name == Game.Team1).Id;
                    dbGame.Team2 = context.Teams.FirstOrDefault(t => t.Name == Game.Team2).Id;
                    Team1 = context.Teams.FirstOrDefault(t => t.Name == Game.Team1).Name;
                    Team2 = context.Teams.FirstOrDefault(t => t.Name == Game.Team2).Name;
                    context.Games.Add(dbGame);
                    context.SaveChanges();
                    GameId = context.Games.Max(t => t.Id);
                    foreach (PlayAction action in Starters)
                    {
                        dbAction.ActionId = context.Actions.FirstOrDefault(t => t.Name == action.Act).Id;
                        dbAction.PlayerId = context.Players.FirstOrDefault(t => t.Name == action.Player).Id;
                        dbAction.TeamId = context.Teams.FirstOrDefault(t => t.Name == action.Team).Id;
                        if (context.Teams.FirstOrDefault(t => t.Name == action.Team).Name == Team1)
                            dbAction.OppTeamId = context.Teams.FirstOrDefault(t => t.Name == Team2).Id;
                        else
                            dbAction.OppTeamId = context.Teams.FirstOrDefault(t => t.Name == Team1).Id;
                        dbAction.Minute = 0;
                        dbAction.Score1 = 0;
                        dbAction.Score2 = 0;
                        dbAction.GameId = GameId;
                        context.PlayersActions.Add(dbAction);
                        context.SaveChanges();
                    }
                    foreach (PlayAction action in Actions)
                    {
                        EuroleagueModels.Action actTemp = context.Actions.FirstOrDefault(t => t.Name == action.Act);
                        if (actTemp!=null) dbAction.ActionId = context.Actions.FirstOrDefault(t => t.Name == action.Act).Id;
                        if (action.Player != " ")
                            dbAction.PlayerId = context.Players.FirstOrDefault(t => t.Name == action.Player).Id;
                        else dbAction.PlayerId = -1;
                        if (action.Team != " ")
                        {
                            dbAction.TeamId = context.Teams.FirstOrDefault(t => t.Name == action.Team).Id;
                            if (context.Teams.FirstOrDefault(t => t.Name == action.Team).Name == Team1)
                                dbAction.OppTeamId = context.Teams.FirstOrDefault(t => t.Name == Team2).Id;
                            else
                                dbAction.OppTeamId = context.Teams.FirstOrDefault(t => t.Name == Team1).Id;
                        }
                        else { dbAction.TeamId = -1; dbAction.OppTeamId = -1; }
                        dbAction.Minute = Int32.Parse(action.Minute);
                        Score1 = Int32.Parse(action.Score.Substring(0, action.Score.IndexOf("-") - 1));
                        dbAction.Score1 = Score1;
                        Score2 = Int32.Parse(action.Score.Substring(action.Score.IndexOf("-") + 2, action.Score.Length - action.Score.IndexOf("-") - 2));
                        dbAction.Score2 = Score2;
                        dbAction.GameId = GameId;
                        context.PlayersActions.Add(dbAction);
                        context.SaveChanges();
                    }
                     
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
            return GameId;
        }
    }
}
