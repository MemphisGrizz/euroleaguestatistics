﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EuroleagueModels;

namespace EuroleagueReader
{
    // players import to DB from List
    public class PlayersImport
    {
        public bool Import(List<PlayerTeam> Players)
        {
            Player TempPlayer;
            if (Players == null) return false;
            Player dbPlayer = new Player();
            using (var context = new UsersContext())
            {
                try
                {
                    foreach (PlayerTeam player in Players)
                    {
                        TempPlayer = context.Players.FirstOrDefault(t => t.Name == player.Name);
                        if (TempPlayer == null)
                        {
                            dbPlayer.Name = player.Name;
                            dbPlayer.Team = player.Team;
                            context.Players.Add(dbPlayer);
                            context.SaveChanges();                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
