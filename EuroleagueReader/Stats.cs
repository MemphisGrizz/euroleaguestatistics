﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace EuroleagueReader
{
    public class PlayAction
    {
        public string Minute { get; set; }
        public string Act { get; set; }
        public string Score { get; set; }
        public string Team { get; set; }
        public string Player { get; set; }
    }

    public class PlayerTeam
    {
        public string Name { get; set; }
        public string Team { get; set; }
        public TimeSpan Min { get; set; }
    }

    public class TeamList
    {
        public int Min { get; set; } // minutes get from boxscore
        public int Pts { get; set; }
        public int FGM2 { get; set; }
        public int FGA2 { get; set; }
        public int FGM3 { get; set; }
        public int FGA3 { get; set; }
        public int FTM { get; set; }
        public int FTA { get; set; }
        public int OR { get; set; }
        public int DR { get; set; }
        public int TR { get; set; }
        public int Assists { get; set; }
        public int Steals { get; set; }
        public int Turnovers { get; set; }
        public int BlFv { get; set; }
        public int BlAg { get; set; }
        public int FlCm { get; set; }
        public int FlRv { get; set; }
        public int PIR { get; set; }
        public string Team { get; set; }
    }

    public class GameCenter
    {
        public DateTime GameDate { get; set; }
        public string Team1 { get; set; }
        public string Team2 { get; set; }
        public int Score1 { get; set; }
        public int Score2 { get; set; }
        public string Stage { get; set; }
        public string Round { get; set; }
        public string Season { get; set; }
    }

    public class Stats
    {
        public List<PlayAction> GameLogReader(string url)
        {
            HtmlNode ActionNode;
            List<PlayAction> ActionList = new List<PlayAction>();
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            HtmlNodeCollection nodeList = doc.DocumentNode.SelectNodes("//div[@class='wp-item-container']");
            if (nodeList == null) return null;
            foreach (HtmlNode node in nodeList)
            {
                string text = node.InnerText;
                if (text.StartsWith("\r\n"))
                {
                    ActionNode = node.SelectSingleNode("//td[.='Start Game']");
                    HtmlNode ParentNode = ActionNode.ParentNode.ParentNode;
                    foreach (HtmlNode childnode in ParentNode.ChildNodes)
                    {
                        if (childnode.Name == "tr")
                        {
                            ActionList.Add(new PlayAction
                            {
                                Minute = childnode.ChildNodes[1].InnerText,
                                Act = childnode.ChildNodes[3].InnerText,
                                Score = childnode.ChildNodes[5].InnerText,
                                Team = childnode.ChildNodes[7].InnerText,
                                Player = childnode.ChildNodes[9].InnerText
                            });
                        }
                    }
                }
            }
            return ActionList;
        }

        public List<PlayAction> StartersReader(string url)
        {
            List<PlayAction> ActionList = new List<PlayAction>();
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);

            //Home Team
            HtmlNodeCollection nodeList = doc.DocumentNode.SelectNodes("//div[@class='LocalClubStatsContainer']");
            if (nodeList == null) return null;            
            foreach (HtmlNode node in nodeList)
            {
                int NodesCount = 29;
                int Index = 3;
                while (Index < NodesCount)
                {
                    string player = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerText;
                    string playerHTML = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerHtml;
                    if (playerHTML.Contains("PlayerStartFive"))
                    {
                        ActionList.Add(new PlayAction
                        {
                            Minute = "0",
                            Act = "In",
                            Score = "0-0",
                            Team = node.ChildNodes[1].ChildNodes[1].InnerText,
                            Player = player
                        });
                    }
                    Index = Index + 2;
                }
            }

            //Road Team
            nodeList = doc.DocumentNode.SelectNodes("//div[@class='RoadClubStatsContainer']");
            if (nodeList == null) return null;
            foreach (HtmlNode node in nodeList)
            {
                int NodesCount = 29;
                int Index = 3;
                while (Index < NodesCount)
                {
                    string player = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerText;
                    string playerHTML = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerHtml;
                    if (playerHTML.Contains("PlayerStartFive"))
                    {
                        ActionList.Add(new PlayAction
                        {
                            Minute = "0",
                            Act = "In",
                            Score = "0-0",
                            Team = node.ChildNodes[1].ChildNodes[1].InnerText,
                            Player = player
                        });
                    }
                    Index = Index + 2;
                }
            }

            return ActionList;
        }

        public List<PlayerTeam> RosterReader(string url)
        {
            List<PlayerTeam> PlayerList = new List<PlayerTeam>();
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            TimeSpan plMin;
            string strMin = "", player = "";

            //Home Team
            HtmlNodeCollection nodeList = doc.DocumentNode.SelectNodes("//div[@class='LocalClubStatsContainer']");
            if (nodeList == null) return null;
            foreach (HtmlNode node in nodeList)
            {
                //int NodesCount = 29;
                int Index = 3;
                while (player != "Team")
                {
                    player = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerText;
                    if (player != "Team")
                    {
                        strMin = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[5].InnerText;
                        if ((strMin == "DNP")||(strMin == "&nbsp;"))
                            plMin = TimeSpan.ParseExact("00:00:00", "g", null);
                        else
                            plMin = TimeSpan.ParseExact("00:" + node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[5].InnerText, "g", null);
                        string playerHTML = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerHtml;
                        PlayerList.Add(new PlayerTeam
                        {
                            Name = player,
                            Team = node.ChildNodes[1].ChildNodes[1].InnerText,
                            Min = plMin
                        });
                    }
                    Index = Index + 2;
                }
            }

            //Road Team
            nodeList = doc.DocumentNode.SelectNodes("//div[@class='RoadClubStatsContainer']");
            if (nodeList == null) return null;
            foreach (HtmlNode node in nodeList)
            {
                //int NodesCount = 29;
                int Index = 3;
                player = "";
                while (player != "Team")
                {
                    player = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerText;
                    if (player != "Team")
                    {
                        strMin = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[5].InnerText;
                        if ((strMin == "DNP") || (strMin == "&nbsp;"))
                            plMin = TimeSpan.ParseExact("00:00:00", "g", null);
                        else
                            plMin = TimeSpan.ParseExact("00:" + node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[5].InnerText, "g", null);
                        string playerHTML = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[3].InnerHtml;
                        PlayerList.Add(new PlayerTeam
                        {
                            Name = player,
                            Team = node.ChildNodes[1].ChildNodes[1].InnerText,
                            Min = plMin
                        });
                    }
                    Index = Index + 2;
                }
            }

            return PlayerList;
        }

        public List<TeamList> TeamStatsReader(string url)
        {
            List<TeamList> _TeamList = new List<TeamList>();
            string team = "", strMin = "";
            string fg = "", fg3 = "", ft = ""; 
            int fgm2, fga2, fgm3, fga3, ftm, fta; 
            int plMin;
            int Index;
            string HomeTeam = "", AwayTeam = "";
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            HtmlNodeCollection nodeList = doc.DocumentNode.SelectNodes("//div[@class='LocalClubStatsContainer']");
            HtmlNode nodeTeam = doc.DocumentNode.SelectSingleNode("//div[@class='game-score']");
            if (nodeTeam != null)
            {
                HomeTeam = nodeTeam.ChildNodes[1].ChildNodes[1].ChildNodes[1].ChildNodes[3].InnerText;
                AwayTeam = nodeTeam.ChildNodes[1].ChildNodes[3].ChildNodes[1].ChildNodes[3].InnerText;
            }
            if (nodeList == null) return null;
            foreach (HtmlNode node in nodeList)
            {
                for (Index = 3; Index < 30; Index = Index + 2)
                {
                    if (node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes.Count > 1)
                    {
                        team = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[3].InnerText;
                        if (team == "Totals")
                        {
                            strMin = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[5].InnerText;
                            plMin = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[5].InnerText.Substring(0,3));
                            fg = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[9].InnerText;
                            fgm2 = Convert.ToInt32(fg.Substring(0, fg.IndexOf("/")));
                            fga2 = Convert.ToInt32(fg.Substring(fg.IndexOf("/") + 1, fg.Length - fg.IndexOf("/") - 1));
                            fg3 = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[11].InnerText;
                            fgm3 = Convert.ToInt32(fg3.Substring(0, fg3.IndexOf("/")));
                            fga3 = Convert.ToInt32(fg3.Substring(fg3.IndexOf("/") + 1, fg3.Length - fg3.IndexOf("/") - 1));
                            ft = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[13].InnerText;
                            ftm = Convert.ToInt32(ft.Substring(0, ft.IndexOf("/")));
                            fta = Convert.ToInt32(ft.Substring(fg.IndexOf("/") + 1, ft.Length - ft.IndexOf("/") - 1));
                            _TeamList.Add(new TeamList
                            {
                                Min = plMin,
                                Pts = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[7].InnerText),
                                FGM2 = fgm2,
                                FGA2 = fga2,
                                FGM3 = fgm3,
                                FGA3 = fga3,
                                FTM = ftm,
                                FTA = fta,
                                OR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[15].InnerText),
                                DR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[17].InnerText),
                                TR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[19].InnerText),
                                Assists = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[21].InnerText),
                                Steals = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[23].InnerText),
                                Turnovers = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[25].InnerText),
                                BlFv = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[27].InnerText),
                                BlAg = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[29].InnerText),
                                FlCm = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[31].InnerText),
                                FlRv = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[33].InnerText),
                                PIR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[35].InnerText),
                                Team = HomeTeam
                            });
                            break;
                        }
                    }
                }
            }

            nodeList = doc.DocumentNode.SelectNodes("//div[@class='RoadClubStatsContainer']");
            if (nodeList == null) return null;
            foreach (HtmlNode node in nodeList)
            {
                for (Index = 3; Index < 30; Index = Index + 2)
                {
                    if (node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes.Count > 1)
                    {
                        team = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[3].InnerText;
                        if (team == "Totals")
                        {
                            strMin = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[5].InnerText;
                            plMin = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[5].InnerText.Substring(0, 3));
                            fg = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[9].InnerText;
                            fgm2 = Convert.ToInt32(fg.Substring(0, fg.IndexOf("/")));
                            fga2 = Convert.ToInt32(fg.Substring(fg.IndexOf("/") + 1, fg.Length - fg.IndexOf("/") - 1));
                            fg3 = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[11].InnerText;
                            fgm3 = Convert.ToInt32(fg3.Substring(0, fg3.IndexOf("/")));
                            fga3 = Convert.ToInt32(fg3.Substring(fg3.IndexOf("/") + 1, fg3.Length - fg3.IndexOf("/") - 1));
                            ft = node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[13].InnerText;
                            ftm = Convert.ToInt32(ft.Substring(0, ft.IndexOf("/")));
                            fta = Convert.ToInt32(ft.Substring(ft.IndexOf("/") + 1, ft.Length - ft.IndexOf("/") - 1));
                            _TeamList.Add(new TeamList
                            {
                                Min = plMin,
                                Pts = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[7].InnerText),
                                FGM2 = fgm2,
                                FGA2 = fga2,
                                FGM3 = fgm3,
                                FGA3 = fga3,
                                FTM = ftm,
                                FTA = fta,
                                OR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[15].InnerText),
                                DR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[17].InnerText),
                                TR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[19].InnerText),
                                Assists = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[21].InnerText),
                                Steals = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[23].InnerText),
                                Turnovers = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[25].InnerText),
                                BlFv = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[27].InnerText),
                                BlAg = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[29].InnerText),
                                FlCm = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[31].InnerText),
                                FlRv = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[33].InnerText),
                                PIR = Convert.ToInt32(node.ChildNodes[3].ChildNodes[1].ChildNodes[Index].ChildNodes[1].ChildNodes[35].InnerText),
                                Team = AwayTeam
                            });
                            break;
                        }
                    }
                }
            }
            return _TeamList;
        }

        public GameCenter GameReader(string url)
        {
            string strDate;
            GameCenter Game = new GameCenter();
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            Game.Score1 = 0;
            Game.Score2 = 0;
            HtmlNode node = doc.DocumentNode.SelectSingleNode("//div[@class='round-header']");
            Game.Season = node.ChildNodes[1].ChildNodes[1].InnerText;
            Game.Stage = node.ChildNodes[1].ChildNodes[2].InnerText;
            Game.Round = node.ChildNodes[1].ChildNodes[3].InnerText;
            node = doc.DocumentNode.SelectSingleNode("//div[@class='game-score']");
            Game.Team1 = node.ChildNodes[1].ChildNodes[1].ChildNodes[1].ChildNodes[3].InnerText;
            Game.Team2 = node.ChildNodes[1].ChildNodes[3].ChildNodes[1].ChildNodes[3].InnerText;
            Game.Score1 = Int32.Parse(node.ChildNodes[1].ChildNodes[1].ChildNodes[3].InnerText);
            Game.Score2 = Int32.Parse(node.ChildNodes[1].ChildNodes[3].ChildNodes[3].InnerText);
            node = doc.DocumentNode.SelectSingleNode("//div[@class='dates']");
            strDate = node.ChildNodes[1].InnerText.Substring(0, node.ChildNodes[1].InnerText.IndexOf("CET")-1);
            Game.GameDate = DateTime.ParseExact(strDate, "MMMM d, yyyy", System.Globalization.CultureInfo.InvariantCulture);
            return Game; 
        }
    }
}
