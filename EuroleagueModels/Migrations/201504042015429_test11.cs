namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAdvAverageStats", "PlayerId", c => c.Int(nullable: false));
            AddColumn("dbo.TeamAdvAverageStats", "TeamId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeamAdvAverageStats", "TeamId");
            DropColumn("dbo.PlayerAdvAverageStats", "PlayerId");
        }
    }
}
