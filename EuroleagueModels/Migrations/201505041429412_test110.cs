namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test110 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAverageStats", "FG2M", c => c.Int(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FG2A", c => c.Int(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FG2", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FG3M", c => c.Int(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FG3A", c => c.Int(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FTM", c => c.Int(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FTA", c => c.Int(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "EFG", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "TS", c => c.Double(nullable: false));
            DropColumn("dbo.PlayerAverageStats", "FG");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlayerAverageStats", "FG", c => c.Double(nullable: false));
            DropColumn("dbo.PlayerAverageStats", "TS");
            DropColumn("dbo.PlayerAverageStats", "EFG");
            DropColumn("dbo.PlayerAverageStats", "FTA");
            DropColumn("dbo.PlayerAverageStats", "FTM");
            DropColumn("dbo.PlayerAverageStats", "FG3A");
            DropColumn("dbo.PlayerAverageStats", "FG3M");
            DropColumn("dbo.PlayerAverageStats", "FG2");
            DropColumn("dbo.PlayerAverageStats", "FG2A");
            DropColumn("dbo.PlayerAverageStats", "FG2M");
        }
    }
}
