namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test117 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Player", "Position", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Player", "Position", c => c.Short(nullable: false));
        }
    }
}
