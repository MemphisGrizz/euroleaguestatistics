namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test14 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAdvAverageStats", "Usage", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayerAdvAverageStats", "Usage");
        }
    }
}
