namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test19 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAverageStats", "PlusMinus", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayerAverageStats", "PlusMinus");
        }
    }
}
