namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test15 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Standings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Position = c.Int(nullable: false),
                        TeamId = c.Int(nullable: false),
                        Win = c.Int(nullable: false),
                        Lost = c.Int(nullable: false),
                        Diff = c.Int(nullable: false),
                        Group = c.String(),
                        Stage = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Standings");
        }
    }
}
