namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test112 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Boxscore", "AvailableOR", c => c.Int(nullable: false));
            AddColumn("dbo.Boxscore", "AvailableDR", c => c.Int(nullable: false));
            AddColumn("dbo.Boxscore", "TotalAvailableReb", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Boxscore", "TotalAvailableReb");
            DropColumn("dbo.Boxscore", "AvailableDR");
            DropColumn("dbo.Boxscore", "AvailableOR");
        }
    }
}
