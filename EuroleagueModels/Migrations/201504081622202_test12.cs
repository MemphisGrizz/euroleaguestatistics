namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAdvAverageStats", "Pace", c => c.Double(nullable: false));
            AddColumn("dbo.TeamAdvAverageStats", "Possessions", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeamAdvAverageStats", "Possessions");
            DropColumn("dbo.PlayerAdvAverageStats", "Pace");
        }
    }
}
