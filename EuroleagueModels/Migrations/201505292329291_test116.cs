namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test116 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Player", "Number", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Player", "Number");
        }
    }
}
