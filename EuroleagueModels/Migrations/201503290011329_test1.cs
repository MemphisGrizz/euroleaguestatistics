namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeamAverageStats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeamId = c.Int(nullable: false),
                        Name = c.String(),
                        PPG = c.Double(nullable: false),
                        RPG = c.Double(nullable: false),
                        APG = c.Double(nullable: false),
                        SPG = c.Double(nullable: false),
                        BPG = c.Double(nullable: false),
                        FG = c.Double(nullable: false),
                        FG3 = c.Double(nullable: false),
                        FT = c.Double(nullable: false),
                        PIR = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TeamAverageStats");
        }
    }
}
