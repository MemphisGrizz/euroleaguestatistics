// <auto-generated />
namespace DB.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.2-31219")]
    public sealed partial class test118 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(test118));
        
        string IMigrationMetadata.Id
        {
            get { return "201505302258528_test118"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
