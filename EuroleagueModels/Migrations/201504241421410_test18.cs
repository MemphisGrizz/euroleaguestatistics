namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test18 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Standings", "TeamId", c => c.Int(nullable: false));
            DropColumn("dbo.Standings", "Team");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Standings", "Team", c => c.String());
            DropColumn("dbo.Standings", "TeamId");
        }
    }
}
