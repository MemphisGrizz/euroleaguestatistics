namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test115 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Player", "Age", c => c.String());
            AddColumn("dbo.Player", "Height", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Player", "Height");
            DropColumn("dbo.Player", "Age");
        }
    }
}
