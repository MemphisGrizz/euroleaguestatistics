namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test111 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAdvAverageStats", "Games", c => c.Int(nullable: false));
            AddColumn("dbo.PlayerAdvAverageStats", "PtsPos", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAdvAverageStats", "ToPos", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayerAdvAverageStats", "ToPos");
            DropColumn("dbo.PlayerAdvAverageStats", "PtsPos");
            DropColumn("dbo.PlayerAdvAverageStats", "Games");
        }
    }
}
