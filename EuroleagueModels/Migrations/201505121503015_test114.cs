namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test114 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeamAdvAverageStats", "OffRebPerc", c => c.Double(nullable: false));
            AddColumn("dbo.TeamAdvAverageStats", "DefRebPerc", c => c.Double(nullable: false));
            AddColumn("dbo.TeamAdvAverageStats", "TotRebPerc", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeamAdvAverageStats", "TotRebPerc");
            DropColumn("dbo.TeamAdvAverageStats", "DefRebPerc");
            DropColumn("dbo.TeamAdvAverageStats", "OffRebPerc");
        }
    }
}
