namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test113 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAdvAverageStats", "OffRebPerc", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAdvAverageStats", "DefRebPerc", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAdvAverageStats", "TotRebPerc", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayerAdvAverageStats", "TotRebPerc");
            DropColumn("dbo.PlayerAdvAverageStats", "DefRebPerc");
            DropColumn("dbo.PlayerAdvAverageStats", "OffRebPerc");
        }
    }
}
