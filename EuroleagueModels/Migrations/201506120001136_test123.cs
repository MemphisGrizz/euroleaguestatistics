namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test123 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "Image_small", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.News", "Image_small");
        }
    }
}
