namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Player", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Player", "Image");
        }
    }
}
