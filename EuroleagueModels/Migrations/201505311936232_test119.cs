namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test119 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAverageStats", "DRPG", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "ORPG", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayerAverageStats", "ORPG");
            DropColumn("dbo.PlayerAverageStats", "DRPG");
        }
    }
}
