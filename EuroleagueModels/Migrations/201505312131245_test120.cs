namespace DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test120 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayerAverageStats", "BAPG", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FPG", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "FAPG", c => c.Double(nullable: false));
            AddColumn("dbo.PlayerAverageStats", "TPG", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayerAverageStats", "TPG");
            DropColumn("dbo.PlayerAverageStats", "FAPG");
            DropColumn("dbo.PlayerAverageStats", "FPG");
            DropColumn("dbo.PlayerAverageStats", "BAPG");
        }
    }
}
