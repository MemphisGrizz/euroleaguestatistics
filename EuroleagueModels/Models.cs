﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;

namespace EuroleagueModels
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }        
        public DbSet<Action> Actions { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<PlayersAction> PlayersActions { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Rotation> Rotations { get; set; }
        public DbSet<Boxscore> Boxscores { get; set; }
        public DbSet<TeamBoxscore> TeamBoxscores { get; set; }
        public DbSet<PlayerAverageStats> PlayerAverageStats { get; set; }
        public DbSet<TeamAverageStats> TeamAverageStats { get; set; }
        public DbSet<PlayerPosession> PlayerPossessions { get; set; }
        public DbSet<PIE> PIEs { get; set; }
        public DbSet<PIE_Team> PIE_Teams { get; set; }
        public DbSet<Team_Possession> Team_Possessions { get; set; }
        public DbSet<PlayerAdvAverageStats> PlayerAdvAdvancedStats { get; set; }
        public DbSet<TeamAdvAverageStats> TeamAdvAverageStats { get; set; }
        public DbSet<TeamStanding> TeamStangings { get; set; }
        public DbSet<New> News { get; set; }
    }

    [Table("Action")]
    public class Action
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }

    [Table("News")]
    public class New
    {
        [Key]
        public int Id { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public string Image_small { get; set; }
    }

    [Table("Game")]
    public class Game
    {
        [Key]
        public int Id { get; set; }
        public DateTime date { get; set; }
        public int Team1 { get; set; }
        public int Team2 { get; set; }
        public int Score1 { get; set; }
        public int Score2 { get; set; }
        public string Stage { get; set; }
        public string Round { get; set; }
        public string Season { get; set; }
    }

    [Table("Player")]
    public class Player
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public short Position { get; set; }
        public short SecondPosition { get; set; }
        public string Team { get; set; }
        public int CurTeam { get; set; }
        public string Image { get; set; }
        public string Age { get; set; }
        public string Height { get; set; }
        public short Number { get; set; }
    }

    [Table("PlayersAction")]
    public class PlayersAction
    {
        [Key]
        public int Id { get; set; }
        public int ActionId { get; set; }
        public int PlayerId { get; set; }
        public int TeamId { get; set; }
        public int OppTeamId { get; set; }        
        public int Minute { get; set; }
        public int Score1 { get; set; }
        public int Score2 { get; set; }
        public int GameId { get; set; }        
    }

    [Table("Position")]
    public class Position
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }

    [Table("Team")]
    public class Team
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Logo { get; set; }
    }

    [Table("Rotation")]
    public class Rotation
    {
        [Key]
        public int Id { get; set; }
        public int Minute { get; set; }
        public string Action { get; set; }
        public int PlayerId { get; set; }        
        public int GameId { get; set; }        
    }

    [Table("Boxscore")]
    public class Boxscore
    {
        [Key]
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public bool Starter { get; set; }
        public int Min { get; set; } // minutes calculated with play-by-play
        public TimeSpan BoxMin { get; set; } // minutes get from boxscore
        public int Pts { get; set; }
        public int FGM2 { get; set; }
        public int FGA2 { get; set; }
        public int FGM3 { get; set; }
        public int FGA3 { get; set; }
        public int FTM { get; set; }
        public int FTA { get; set; }
        public int OR { get; set; }
        public int DR { get; set; }
        public int TR { get; set; }
        public int Assists { get; set; }
        public int Steals { get; set; }
        public int Turnovers { get; set; }
        public int BlFv { get; set; }
        public int BlAg { get; set; }
        public int FlCm { get; set; }
        public int FlRv { get; set; }
        public int PIR { get; set; }
        public int PlusMinus { get; set; }
        public int TeamId { get; set; }
        public int OppTeamId { get; set; }
        public int GameId { get; set; }
        public int PointsFw { get; set; }
        public int PointsAg { get; set; }
        public int AvailableOR { get; set; }
        public int AvailableDR { get; set; }
        public int TotalAvailableReb { get; set; }
    }

    [Table("TeamBoxscore")]
    public class TeamBoxscore
    {
        [Key]
        public int Id { get; set; }
        public int Min { get; set; }
        public int Pts { get; set; }
        public int FGM2 { get; set; }
        public int FGA2 { get; set; }
        public int FGM3 { get; set; }
        public int FGA3 { get; set; }
        public int FTM { get; set; }
        public int FTA { get; set; }
        public int OR { get; set; }
        public int DR { get; set; }
        public int TR { get; set; }
        public int Assists { get; set; }
        public int Steals { get; set; }
        public int Turnovers { get; set; }
        public int BlFv { get; set; }
        public int BlAg { get; set; }
        public int FlCm { get; set; }
        public int FlRv { get; set; }
        public int PIR { get; set; }        
        public int TeamId { get; set; }
        public int OppTeamId { get; set; }
        public int GameId { get; set; }        
    }
    

    [Table("PlayerAverageStats")]
    public class PlayerAverageStats
    {
        [Key]
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public string Name { get; set; }
        public double PPG { get; set; }
        public double RPG { get; set; }
        public double DRPG { get; set; }
        public double ORPG { get; set; }
        public double APG { get; set; }
        public double SPG { get; set; }
        public double BPG { get; set; }
        public double BAPG { get; set; }
        public double FPG { get; set; }
        public double FAPG { get; set; }
        public double TPG { get; set; }
        public TimeSpan MPG { get; set; }
        public int FG2M { get; set; }
        public int FG2A { get; set; }
        public double FG2 { get; set; }
        public int FG3M { get; set; }
        public int FG3A { get; set; }
        public double FG3 { get; set; }
        public int FTM { get; set; }
        public int FTA { get; set; }
        public double EFG { get; set; }
        public double TS { get; set; }
        public double FT { get; set; }
        public double PIR { get; set; }
        public double PlusMinus { get; set; }
    }

    [Table("TeamAverageStats")]
    public class TeamAverageStats
    {
        [Key]
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public double PPG { get; set; }
        public double RPG { get; set; }
        public double APG { get; set; }
        public double SPG { get; set; }
        public double BPG { get; set; }        
        public double FG { get; set; }
        public double FG3 { get; set; }
        public double FT { get; set; }
        public double PIR { get; set; }
    }

    [Table("PlayerPossession")]
    public class PlayerPosession
    {
        [Key]
        public int Id { get; set; }
        public int Minutes { get; set; }
        public TimeSpan BoxscoreMinutes { get; set; }
        public int PlayerPos { get; set; }
        public int TeamPos { get; set; } // Team Possessions while player on the court
        public int PlayerId { get; set; }
        public int GameId { get; set; }
        public int TeamId { get; set; }
        public int OppTeamId { get; set; }
        public double OffRtg { get; set; }
        public double DefRtg { get; set; }
        public double NetRtg { get; set; }
    }

    [Table("PIE")]
    public class PIE
    {
        [Key]
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public int TeamId { get; set; }
        public int GameId { get; set; }
        public double PlayerActions { get; set; }
        public double GameActions { get; set; }
        public double PIE_Value { get; set; }
    }

    [Table("TeamPIE")]
    public class PIE_Team
    {
        [Key]
        public int Id { get; set; }
        public int TeamId { get; set; }
        public int GameId { get; set; }
        public double PIE_Value { get; set; }
    }

    [Table ("TeamPossessions")]
    public class Team_Possession
    {
        [Key]
        public int Id { get; set; }
        public int TeamId { get; set; }
        public int GameId { get; set; }
        public double Possessions { get; set; }
        public int Minutes { get; set; }
        public double OffRtg { get; set; }
        public double DefRtg { get; set; }
        public double NetRtg { get; set; }
    }

    [Table("PlayerAdvAverageStats")]
    public class PlayerAdvAverageStats
    {
        [Key]
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public int Games { get; set; }
        public double OffRtg { get; set; }
        public double DefRtg { get; set; }
        public double NetRtg { get; set; }
        public double PIE { get; set; }
        public double PlayerPossessions { get; set; }
        public double TeamPossessions { get; set; }
        public double Usage { get; set; }
        public double Pace { get; set; }
        public double PtsPos { get; set; } // points per possession
        public double ToPos { get; set; } // turnovers per possession
        public double OffRebPerc { get; set; }
        public double DefRebPerc { get; set; }
        public double TotRebPerc { get; set; }
    }

    [Table("TeamAdvAverageStats")]
    public class TeamAdvAverageStats
    {
        [Key]
        public int Id { get; set; }
        public int TeamId { get; set; }
        public double OffRtg { get; set; }
        public double DefRtg { get; set; }
        public double NetRtg { get; set; }
        public double PIE { get; set; }
        public double Possessions { get; set; }
        public double Pace { get; set; }
        public double OffRebPerc { get; set; }
        public double DefRebPerc { get; set; }
        public double TotRebPerc { get; set; }
    }

    [Table("Standings")]
    public class TeamStanding
    {
        [Key]
        public int Id { get; set; }
        public int Position { get; set; }
        public int TeamId { get; set; }
        public int Win { get; set; }
        public int Lost { get; set; }
        public double Pct { get; set; }
        public string Group { get; set; }
        public string Stage { get; set; }        
    }
}
