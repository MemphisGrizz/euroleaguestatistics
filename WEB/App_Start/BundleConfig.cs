﻿using System.Web;
using System.Web.Optimization;

namespace WEB
{
    public class BundleConfig
    {
        // 678969For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/app").IncludeDirectory(
                      "~/Scripts/App", "*.js", true));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css"));
        }
    }
}