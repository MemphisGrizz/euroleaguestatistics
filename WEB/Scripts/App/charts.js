﻿
function FillDefOffChart() {
    $.ajax({        
        url: "/Chart/GetChartOffDefData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: 
            function (data) {                                
                                RenderOffDefChart(data.Data);
                                $('tspan:contains("chart by amcharts.com")').remove();                            
                            }
    });
}

function FillPaceChart() {
    $.ajax({
        url: "/Chart/GetChartPaceData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success:
            function (data) {
                RenderPaceChart(data.Data);
                $('tspan:contains("chart by amcharts.com")').remove();
            }
    });
}

function FillPlayerChart(id) {
    $.ajax({
        url: "/Chart/GetChartPlayerData",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: "{'playerId' : " + id + "}",
        dataType: "json",
        success:
            function (data) {
                RenderPlayerChart(data.Data.Player);
                $('tspan:contains("chart by amcharts.com")').remove();
            }
    });
}

function FillBoxChart(action, div, max, min, range) {
    var href = window.location.href;
    var id = href.substr(href.lastIndexOf('/') + 1);
    var str = "/Chart/"
    var href = str.concat(action);
    $.ajax({
        url: href,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: "{'id' : " + parseInt(id) + "}",
        dataType: "json",
        success:
            function (data) {
                RenderBoxChart(data.Data, div, max, min, range);
                $('tspan:contains("chart by amcharts.com")').remove();
            }
    });
}

function FillPIEChart(action, div) {
    var href = window.location.href;
    var id = href.substr(href.lastIndexOf('/') + 1);
    var str = "/Chart/"
    var href = str.concat(action);
    $.ajax({
        url: href,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: "{'id' : " + parseInt(id) + "}",
        dataType: "json",
        success:
            function (data) {
                PIEChart(data.Data, div);
                $('tspan:contains("chart by amcharts.com")').remove();
            }
    });
}




var RenderOffDefChart = function (data) {
    //var colors = ['#FF0F00', '#FF6600', '#FF9E01', '#FCD202', '#F8FF01', '#B0DE09', '#04D215', '#0D8ECF', '#0D52D1', '#2A0CD0', '#8A0CCF', '#CD0D74', '#754DEB'];
    // for (i = 0; i < data.length; i++) { data[i].color = colors[i]; }
    var chart = AmCharts.makeChart("chartdiv", {
        "type": "serial",
        "theme": "dark",
        "marginRight": 70,
        "path": "http://www.amcharts.com/lib/3/",
        "dataProvider": data,
        "valueAxes": [{            
            "position": "left",
            "title": "OFFRTG/DEFRTG",
            "minimum": 50,
            "maximum": 150,
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<b>[[category]] Offence RTG: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",            
            "valueField": "OffRtg"
        },
        {
            "balloonText": "<b>[[category]] Defense RTG: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",            
            "valueField": "DefRtg",
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "plotAreaFillAlphas": 0.1,        
        "categoryField": "Name",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 45            
        },
        "export": {
            "enabled": true
        }

    });
    jQuery('.chart-input').off().on('input change', function () {
        var property = jQuery(this).data('property');
        var target = chart;
        chart.startDuration = 0;

        if (property == 'topRadius') {
            target = chart.graphs[0];
            if (this.value == 0) {
                this.value = undefined;
            }
        }

        target[property] = this.value;
        chart.validateNow();
    });
}


var RenderPaceChart = function (data) {
    //var colors = ['#FF0F00', '#FF6600', '#FF9E01', '#FCD202', '#F8FF01', '#B0DE09', '#04D215', '#0D8ECF', '#0D52D1', '#2A0CD0', '#8A0CCF', '#CD0D74', '#754DEB'];
    // for (i = 0; i < data.length; i++) { data[i].color = colors[i]; }
    var chart = AmCharts.makeChart("chartdiv1", {
        "type": "serial",
        "theme": "light",
        "marginRight": 70,
        "path": "http://www.amcharts.com/lib/3/",
        "dataProvider": data,
        "valueAxes": [{
            "stackType": "3d",
            "position": "left",
            "title": "PACE",
            "minimum": 50,
            "maximum": 90,
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<b>[[category]] PACE: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "Pace"
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "plotAreaFillAlphas": 0.1,        
        "categoryField": "Name",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 45
        },
        "export": {
            "enabled": true
        }

    });
    jQuery('.chart-input').off().on('input change', function () {
        var property = jQuery(this).data('property');
        var target = chart;
        chart.startDuration = 0;

        if (property == 'topRadius') {
            target = chart.graphs[0];
            if (this.value == 0) {
                this.value = undefined;
            }
        }

        target[property] = this.value;
        chart.validateNow();
    });
}


var RenderBoxChart = function (data, div, max, min, range) {
    var colors = ['#FF0F00', '#FF6600'];
    for (i = 0; i < data.Team.length; i++) { data.Team[i].color = colors[i]; }
    var chart = AmCharts.makeChart(div, {
        "type": "serial",
        "theme": "dark",
        "marginRight": 70,
        "path": "http://www.amcharts.com/lib/3/",
        "dataProvider": data.Team,
        "valueAxes": [{
            "position": "right",
            "minimum": min,
            "maximum": max,
            "labelsEnabled": true,
        }],
        "startDuration": 1,
        "allLabels": [{
            "text": data.Name,
            "bold": true,
            "x": range,
            "y": 5
        }],
        "graphs": [{
            "balloonText": "<b>[[category]] : [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "marginRight": 0,
            "type": "column",
            "labelText": "[[value]]",
            "valueField": "Data"
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "plotAreaFillAlphas": 0,
        "categoryField": "Name",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 0
        },
        "export": {
            "enabled": true
        }

    });
}


    var PIEChart = function (data) {
        var chart = AmCharts.makeChart("chartdiv9", {
	"type": "pie",
	"path": "http://www.amcharts.com/lib/3/",
	"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",	
	"titleField": "Name",
	"valueField": "Data",
	"theme": "none",
	"fontSize": 12,
	"allLabels": [{
	    "text": data.Name,
	    "bold": true,
	    "x": 96,
	    "y": 5,        
	}],
	"labelRadius": -30,
	"labelcolor": "green",
	"fontSize": 11,
	"labelText": "[[percents]]%",
	"graphs": [{
	    "bulletBorderAlpha": 1
	}],
	"autoMargins": false,
	"marginTop": 0,
	"marginBottom": 0,
	"marginLeft": 0,
	"marginRight": 0,
     "borderAlpha": 1,
	"dataProvider": data.Team
    });
    }


var RenderPlayerChart = function (data) {     
    var chart = AmCharts.makeChart("chartdiv10", {
        "type": "serial",
        "theme": "default",
        "marginRight": 80,
        "autoMarginOffset": 20,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left"
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": [{
            "id": "g1",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "Points",
            "useLineColorForBulletBorder": true,
            "valueField": "Pts",
            "balloonText": "<div style='margin:5px; font-size:16px;'><span style='font-size:13px;'>[[category]]</span><br>[[value]] Points</div>"
        },
        {
            "id": "g2",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "PIR",
            "useLineColorForBulletBorder": true,
            "valueField": "PIR",
            "balloonText": "<div style='margin:5px; font-size:16px;'><span style='font-size:13px;'>[[category]]</span><br>[[value]] PIR</div>"
        },
        {
            "id": "g3",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "Minutes",
            "useLineColorForBulletBorder": true,
            "valueField": "Min",
            "balloonText": "<div style='margin:5px; font-size:16px;'><span style='font-size:13px;'>[[category]]</span><br>[[value]] Minutes</div>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "scrollbarHeight": 80,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 0,
            "valueLineAlpha": 0.2
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true,
            "position": "top"
        },
        "export": {
            "enabled": true
        },
        "dataProvider": data
    });

    var legend = new AmCharts.AmLegend();
    chart.addLegend(legend);
    chart.colors = "#f80000 #2A0CD0 #FF9933 #0D8ECF #2A0CD0 #CD0D74 #CC0000 #00CC00 #0000CC #DDDDDD #999999 #333333 #990000".split(' ');

    chart.addListener("rendered", zoomChart);

    zoomChart();

    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
    }
}