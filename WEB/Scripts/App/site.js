﻿
$(function()
{
    var wrapper = $('.server-sortable').closest('.tour');

    wrapper.on('change', '.selectpicker', function (e) {
        var t = $(this);
        var wrapper2 = t.closest('.tour');
        var team = t.find("option:selected").val();
        var query = getSortQuery(wrapper2.find('.sorted-desc, .sorted-asc'), false);
        query.CurrentPageIndex = 0;
        query.Team = team;
        refreshTraditionalPart(query, wrapper2);
    });

    wrapper.on('click', 'a.header', function (e) {
        e.preventDefault();
        var t = $(this);
        var query = getSortQuery(t, true);
        query.Team = $('.selectpicker').find("option:selected").val();
        var speed = $("div.index-traditional-stats").data("link-url");
        refreshTraditionalPart(query, t.closest('.tour'));
    });

    wrapper.on('click', 'a.pager', function (e) {
        e.preventDefault();
        var t = $(this);
        var wrapper2 = t.closest('.tour');
        var query = getSortQuery(wrapper2.find('.sorted-desc, .sorted-asc'), false);
        query.CurrentPageIndex = t.attr('data-pageindex');        
        refreshTraditionalPart(query, wrapper2);
    });

    function getSortQuery(a, changeSortOrder)
    {
        var desc = a.hasClass('sorted-desc');
        if (changeSortOrder) desc = !desc;
        return {
            SortField: a.attr('data-sortfield'),
            SortDirection: desc ? 'desc' : 'asc'
        };
    }

    function refreshTraditionalPart(query, wrapper)
    {        
        $.get($("div.index-traditional-stats").data("link-url"), query, function (res) {
            wrapper.html(res);
        });
    }
});

