﻿using DBReader;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WEB.Models
{
    #region Main page

    //public class NewsItem
    //{
    //    public string Title, Image, Url;
    //    public string TeamName, TeamImage;
    //}

    public class IndexModel
    {
        public NewsModel[] News;
        public TopBlock OFFRTG, USAGE, SCORES, REB, DEFRTG, PIE, ASSISTS, NETRTG, PACE;
        public StandingsInfo Standings;
        public ResultsInfo Results;
    }

    public class TopBlock
    {
        public string Title, Id;
        public TopBlockGroup[] Groups;
    }

    public class TopBlockGroup
    {
        public string Title;
        public TopBlockItem[] Items;
    }

    public class TopBlockItem
    {
        public string Name, Image, Value;
        public string TeamName, TeamImage;
        public TopBlockItem()
        {
        }
        public TopBlockItem(TopBlockItemReader tReader)
        {
            Name = tReader.Name;
            Image = tReader.Image;
            Value = tReader.Value;
            TeamName = tReader.TeamName;
            TeamImage = tReader.TeamImage;
        }
    }

    public class StandingsInfo
    {
        public TeamsGroup[] TeamGroups;
    }

    public class TeamsGroup
    {
        public string Name;
        public TeamInfo[] Teams;
    }

    public class TeamInfo
    {
        public string Logo, Name;
        public int Wins, Loses, Perc;
        public TeamInfo() { }
        public TeamInfo(TeamInfoReader tReader)
        {
            Logo = tReader.Logo;
            Name = tReader.Name;
            Wins = tReader.Wins;
            Loses = tReader.Loses;
            Perc = tReader.Perc;
        }
    }

    public class ResultsInfo
    {
        public string Title;
        public ResultsGroup[] Groups;
    }

    public class ResultsGroup
    {
        public string Title;
        public Result[] Results;
    }

    public class Result
    {
        public int GameId;
        public string Team1Name, Team2Name;
        public int Team1Score, Team2Score;
        public string HomeLogo, AwayLogo;
        public string Date;
        public Result() { }
        public Result(ResultReader resReader)
        {
            GameId = resReader.GameId;
            Team1Name = resReader.Team1Name;
            Team1Score = resReader.Team1Score;
            Team2Name = resReader.Team2Name;
            Team2Score = resReader.Team2Score;
            HomeLogo = resReader.HomeLogo;
            AwayLogo = resReader.AwayLogo;
            Date = resReader.Date;
        }
    }

    #endregion


    #region Games page

    public class GamesPageModel
    {
        public ResultsInfo[] Stages;
        public TopBlock PLUSMINUS;
    }

    public class GamesPageStage
    {
        public string Title;
        public ResultsInfo[] Tours;
    }

    #endregion

    #region Traditional Stats

    public class TrPlayerStats
    {
        public string Name, Image, Team, TeamLogo, MPG;
        public double PPG, RPG, APG, SPG, BPG, PIR, PLUS_MINUS;
        public TrPlayerStats() { }
        public TrPlayerStats(PlayerStatsRow psReader)
        {
            Name = psReader.Name;
            Image = psReader.Image;
            Team = psReader.Team;
            TeamLogo = psReader.Logo;
            PPG = psReader.PPG;
            RPG = psReader.RPG;
            APG = psReader.APG;
            SPG = psReader.SPG;
            BPG = psReader.BPG;
            MPG = psReader.MPG;
            PIR = psReader.PIR;
            PLUS_MINUS = psReader.PLUS_MINUS;
        }
    }

    public class TrPlayerStatsStage
    {
        public string Title;
        public TrPlayerStats[] Stats;
        public string[] Teams;
        public SortingPagingInfo Paging;
    }

    public class TraditionalStatsModel
    {
        public TrPlayerStatsStage[] Stages;
        public TopBlock PLUSMINUS;
    }

    #endregion

    #region Shooting Stats

    public class ShPlayerStats
    {
        public string Name, Image, Team, TeamLogo;
        public double FG2, FG2A, FG3, FG3A, FT, FTA, EFG, TS;
        public ShPlayerStats() { }
        public ShPlayerStats(ShootingStatsRow shReader)
        {
            Name = shReader.Name;
            Image = shReader.Image;
            Team = shReader.Team;
            TeamLogo = shReader.Logo;
            FG2 = shReader.FG2;
            FG2A = shReader.FG2A;
            FG3 = shReader.FG3;
            FG3A = shReader.FG3A;
            FT = shReader.FT;
            FTA = shReader.FTA;
            EFG = shReader.EFG;
            TS = shReader.TS;
        }
    }

    public class ShPlayerStatsStage
    {
        public string Title;
        public ShPlayerStats[] Stats;
        public string[] Teams;
        public SortingPagingInfo Paging;
    }

    public class ShootingStatsModel
    {
        public ShPlayerStatsStage[] Stages;
        public TopBlock TS;
    }

    #endregion

    #region Possessions

    public class PosessionPlayerStats
    {
        public string Name, Team, Image, Logo;
        public double PlayerPossessions, TeamPossessions, Pace, PtsPos, ToPos, Usage, Games;
        public PosessionPlayerStats() { }
        public PosessionPlayerStats(PosStatsRow posReader)
        {
            Name = posReader.Name;
            Image = posReader.Image;
            Team = posReader.Team;
            Logo = posReader.Logo;
            Games = posReader.Games;
            PlayerPossessions = posReader.PlayerPossessions;
            TeamPossessions = posReader.TeamPossessions;
            Pace = posReader.Pace;
            PtsPos = posReader.PtsPos;
            ToPos = posReader.ToPos;
            Usage = posReader.Usage;
        }
    }

    public class PosPlayerStatsStage
    {
        public string Title;
        public PosessionPlayerStats[] Stats;
        public string[] Teams;
        public SortingPagingInfo Paging;
    }

    public class PossessionStatsModel
    {
        public PosPlayerStatsStage[] Stages;
        public TopBlock Pace;
    }

    #endregion

    #region Advanced

    public class AdvancedPlayerStats
    {
        public string Name, Team, Image, Logo;
        public double OffRtg, DefRtg, NetRtg, PIE, OffRebPerc, DefRebPerc, TotRebPerc, Games;
        public AdvancedPlayerStats() { }
        public AdvancedPlayerStats(AdvStatsRow advReader)
        {
            Name = advReader.Name;
            Image = advReader.Image;
            Team = advReader.Team;
            Logo = advReader.Logo;
            Games = advReader.Games;
            OffRtg = advReader.OffRtg;
            DefRtg = advReader.DefRtg;
            NetRtg = advReader.NetRtg;
            PIE = advReader.PIE;
            OffRebPerc = advReader.OffRebPerc;
            DefRebPerc = advReader.DefRebPerc;
            TotRebPerc = advReader.TotRebPerc;
        }
    }

    public class AdvPlayerStatsStage
    {
        public string Title;
        public AdvancedPlayerStats[] Stats;
        public string[] Teams;
        public SortingPagingInfo Paging;
    }

    public class AdvancedStatsModel
    {
        public AdvPlayerStatsStage[] Stages;
        public TopBlock Pace;
    }

    #endregion

    #region Teams

    public class TraditionalTeamsStats
    {
        public string Name, Logo;
        public double PPG, RPG, APG, SPG, BPG, FG2, FG3, FT;
        public TraditionalTeamsStats() { }
        public TraditionalTeamsStats(TrTeamStatsRow trReader)
        {
            Name = trReader.Name;
            Logo = trReader.Logo;
            PPG = trReader.PPG;
            RPG = trReader.RPG;
            APG = trReader.APG;
            SPG = trReader.SPG;
            BPG = trReader.BPG;
            FG2 = trReader.FG2;
            FG3 = trReader.FG3;
            FT = trReader.FT;
        }
    }

    public class TrTeamStatsStage
    {
        public string Title;
        public TraditionalTeamsStats[] Stats;
        public string[] Teams;
        public SortingPagingInfo Paging;
    }

    public class TrTeamStatsModel
    {
        public TrTeamStatsStage[] Stages;
        public TopBlock Pace;
    }

    public class AdvancedTeamsStats
    {
        public string Name, Logo;
        public double PIR, OffRtg, DefRtg, NetRtg, PIE, Pace, OffRebPerc, DefRebPerc, TotRebPerc;
        public AdvancedTeamsStats() { }
        public AdvancedTeamsStats(AdvTeamStatsRow advReader)
        {
            Name = advReader.Name;
            Logo = advReader.Logo;
            PIR = advReader.PIR;
            OffRtg = advReader.OffRtg;
            DefRtg = advReader.DefRtg;
            NetRtg = advReader.NetRtg;
            PIE = advReader.PIE;
            Pace = advReader.Pace;
            OffRebPerc = advReader.OffRebPerc;
            DefRebPerc = advReader.DefRebPerc;
            TotRebPerc = advReader.TotRebPerc;
        }
    }

    public class AdvTeamStatsStage
    {
        public string Title;
        public AdvancedTeamsStats[] Stats;
        public string[] Teams;
        public SortingPagingInfo Paging;
    }

    public class AdvTeamStatsModel
    {
        public AdvTeamStatsStage[] Stages;
        public TopBlock Pace;
    }

    #endregion

    # region charts

    public class OffRtgChart
    {
        public string Name;
        public double OffRtg, DefRtg;
        public OffRtgChart() { }
        public OffRtgChart(OffRtgChartRow rtgReader)
        {
            Name = rtgReader.Name;
            OffRtg = rtgReader.OffRtg;
            DefRtg = rtgReader.DefRtg;
        }
    }

    public class PaceRtgChart
    {
        public string Name;
        public double Pace;
        public PaceRtgChart() { }
        public PaceRtgChart(PaceChartRow paceReader)
        {
            Name = paceReader.Name;
            Pace = paceReader.Pace;
        }
    }

    public class FG2ChartTeam
    {
        public string Name;
        public double Data;
        public FG2ChartTeam() { }
        public FG2ChartTeam(BoxscoreChartRow paceReader)
        {
            Name = paceReader.Name;
            Data = paceReader.Data;
        }
    }

    public class FG2Chart
    {
        public string Name;
        public FG2ChartTeam[] Team;
    }

    public class PlayerChData
    {
        public string date;
        public int Pts, PIR, Min;        
        public PlayerChData() { }
        public PlayerChData(PlayerChartRow plReader)
        {
            Pts = plReader.Pts;
            PIR = plReader.PIR;
            Min = plReader.Min;
            date = plReader.Date;
        }
    }

    public class PlayerChart
    {        
        public PlayerChData[] Player;
    }

    #endregion

    # region boxscore

    public class GameModel
    {
        public TopBlock Pace;
        public GameBoxscore Game;
    }

    public class GameBoxscore
    {
        public BoxscoreGameInfo Game;
        public TeamBoxscore[] Teams;
        public GameBoxscore(GameReader model)
        {
            Teams = model.Teams.Select(t => new TeamBoxscore(t)).ToArray();
            Game = new BoxscoreGameInfo(model.Game);
        }
    }

    public class TeamBoxscore
    {
        public string Title;
        public PlayerBoxscore[] Players;
        public TotalBoxscore Team;
        public TeamBoxscore(BoxscoreGroupReader model)
        {
            Players = model.Players.Select(t => new PlayerBoxscore(t)).ToArray();            
            Team = new TotalBoxscore(model.Teams);
            Title = model.Teams.Name;
        }
    }

    public class BoxscoreGameInfo
    {
        public string HomeTeam, AwayTeam, Date, Round, Stage, HomeLogo, AwayLogo;
        public int HomeScore, AwayScore;
        public BoxscoreGameInfo() { }
        public BoxscoreGameInfo(GameInfo gmInfo)
        {
            HomeTeam = gmInfo.HomeTeam;
            AwayTeam = gmInfo.AwayTeam;
            Date = gmInfo.Date;
            Round = gmInfo.Round;
            Stage = gmInfo.Stage;
            HomeLogo = gmInfo.HomeLogo;
            AwayLogo = gmInfo.AwayLogo;
            HomeScore = gmInfo.HomeScore;
            AwayScore = gmInfo.AwayScore;
            Date = gmInfo.Date;
        }
    }

    public class PlayerBoxscore
    {
        public int GameId;
        public string BoxMin;
        public bool Starter;
        public string Name, Logo;
        public int Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR, PlusMinus;
        public double FG2Perc, FG3Perc, FTPerc, Usage, OffRtg, DefRtg, NetRtg, PIE, PPOS;
        public int plPos, teamPosPl;
        public string Date;
        public PlayerBoxscore() { }
        public PlayerBoxscore(PlBoxscore plInfo)
        {
            AS = plInfo.AS;
            BlAg = plInfo.BlAg;
            BlFv = plInfo.BlFv;
            BoxMin = plInfo.BoxMin;
            DefRtg = plInfo.DefRtg; 
            DR = plInfo.DR;
            FG2M = plInfo.FG2M;
            FG2A = plInfo.FG2A;
            FG2Perc = plInfo.FG2Perc;
            FG3A = plInfo.FG3A;
            FG3M = plInfo.FG3M;
            FG3Perc = plInfo.FG3Perc;
            FTM = plInfo.FTM;
            FTA = plInfo.FTA;
            FTPerc = plInfo.FTPerc;
            FlCm = plInfo.FlCm;
            FlRv = plInfo.FlRv;
            Name = plInfo.Name;
            NetRtg = plInfo.NetRtg;
            OffRtg = plInfo.OffRtg;
            OR = plInfo.OR;
            PIE = plInfo.PIE;
            PIR = plInfo.PIR;
            plPos = plInfo.plPos;
            PlusMinus = plInfo.PlusMinus;
            PPOS = plInfo.PPOS;
            Pts = plInfo.Pts;
            ST = plInfo.ST;
            teamPosPl  = plInfo.teamPosPl;
            TO = plInfo.TO;
            TR = plInfo.TR;
            Usage = plInfo.Usage;
            Starter = plInfo.Starter;
            Logo = plInfo.Logo;
        }
    }

    public class TotalBoxscore
    {
        public int GameId;
        public int Min;
        public string Name, Logo;
        public int Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR;
        public double FG2Perc, FG3Perc, FTPerc, OffRtg, DefRtg, NetRtg, PIE, PPOS;
        public double Poss;
        public string Date;
        public TotalBoxscore() { }
        public TotalBoxscore(TBoxscore tInfo)
        {
            AS = tInfo.AS;
            BlAg = tInfo.BlAg;
            BlFv = tInfo.BlFv;
            Min = tInfo.Min;
            DefRtg = tInfo.DefRtg;
            DR = tInfo.DR;
            FG2A = tInfo.FG2A;
            FG2M = tInfo.FG2M;
            FG3A = tInfo.FG3A;
            FG3M = tInfo.FG3M;
            FTA = tInfo.FTA;
            FTM = tInfo.FTM;
            FG2Perc = tInfo.FG2Perc;
            FG3Perc = tInfo.FG3Perc;
            FTPerc = tInfo.FTPerc;
            FlCm = tInfo.FlCm;
            FlRv = tInfo.FlRv;
            Logo = tInfo.Logo;
            Name = tInfo.Name;
            NetRtg = tInfo.NetRtg;
            OffRtg = tInfo.OffRtg;
            OR = tInfo.OR;
            PIE = tInfo.PIE;
            PIR = tInfo.PIR;
            Poss = tInfo.Poss;
            PPOS = tInfo.PPOS;
            Pts = tInfo.Pts;
            ST = tInfo.ST;
            TO = tInfo.TO;
            TR = tInfo.TR;        
        }
    }

    #endregion

    # region players

    public class PlayerListModel
    {
        public TeamPlayersModel[] tpModel;
    }

    public class TeamPlayersModel
    {
        public string Team, Logo;
        public PlayerModel[] PlayersList;
        public TeamPlayersModel(TeamsPlayers model)
        {
            PlayersList = model.PlayersList.Select(t => new PlayerModel(t)).ToArray();
            Team = model.Team;
            Logo = model.Logo;
        }
    }

    public class PlayerModel
    {
        public string Name, Image, Position, Age, Height;
        public int Number;
        public int Id;
        public PlayerModel() { }
        public PlayerModel(Player pl)
        {           
            Name = pl.Name;
            Image = pl.Image;
            Position = pl.Position;
            Age = pl.Age;
            Height = pl.Height;
            Number = pl.Number;
            Id = pl.Id;
        }
    }

    public class PlayerStatModel
    {
        public string Name, Image, Logo, Team, Position, Age, Height;
        public double Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR, PlusMinus, EFG, TS;
        public double FG2Perc, FG3Perc, FTPerc, Usage, OffRtg, DefRtg, NetRtg, PIE, PPOS, ToPos, ORebPerc, DRebPers, TRebPerc, plPos, teamPosPl;
        public int games, PlayerId;
        public string BoxMin;
        public PlayerStatModel() { }
        public PlayerStatModel(PlayerStat pStat)
        {
            Logo = pStat.Logo; Team = pStat.Team; Image = pStat.Image; Position = pStat.Pos;
            Age = pStat.Age; Height = pStat.Height; Name = pStat.Name;
            games = pStat.games; BoxMin = pStat.BoxMin; Pts = pStat.Pts; FG2M = pStat.FG2M;
            FG2A = pStat.FG2A; FG2Perc = pStat.FG2Perc; FG3M = pStat.FG3M;
            FG3A = pStat.FG3A; FG3Perc = pStat.FG3Perc; FTM = pStat.FTM;
            FTA = pStat.FTA; FTPerc = pStat.FTPerc; TR = pStat.TR; OR = pStat.OR;
            DR = pStat.DR; AS = pStat.AS; ST = pStat.ST; TO = pStat.TO; BlFv = pStat.BlFv;
            BlAg = pStat.BlAg; FlCm = pStat.FlCm; FlRv = pStat.FlRv; PIR = pStat.PIR; PlusMinus = pStat.PlusMinus;
            EFG = pStat.EFG; TS = pStat.TS; OffRtg = pStat.OffRtg; DefRtg = pStat.DefRtg; NetRtg = pStat.NetRtg;
            ORebPerc = pStat.ORebPerc; DRebPers = pStat.DRebPers; TRebPerc = pStat.TRebPerc;
            plPos = pStat.plPos; teamPosPl = pStat.teamPosPl; PlayerId = pStat.playerId;
            PPOS = pStat.PPOS; ToPos = pStat.ToPos; Usage = pStat.Usage; PIE = pStat.PIE;
        }
    }

    public class PlayerGamesModel
    {
        public int GameId, Team1, Team2, Score1, Score2;
        public string Name, Image, Logo, Team, OppTeam, Place, Result, Date;
        public double Pts, FG2M, FG2A, FG3M, FG3A, FTM, FTA, OR, DR, TR, AS, ST, TO, BlFv, BlAg, FlCm, FlRv, PIR, PlusMinus;
        public string BoxMin;
        public PlayerGamesModel() { }
        public PlayerGamesModel(PlayerGames pGames)
        {
            Team = pGames.Team; Image = pGames.Image; Logo = pGames.Logo; GameId = pGames.GameId; Score1 = pGames.Score1; Score2 = pGames.Score2;
             BoxMin = pGames.BoxMin; Pts = pGames.Pts; FG2M = pGames.FG2M; FG2A = pGames.FG2A;
             FG3M = pGames.FG3M; FG3A = pGames.FG3A; FTM = pGames.FTM; FTA = pGames.FTA; OR = pGames.OR;
             DR = pGames.DR; TR = pGames.TR; AS = pGames.AS; ST = pGames.ST; TO = pGames.TO;
             BlFv = pGames.BlFv; BlAg = pGames.BlAg; FlCm = pGames.FlCm; FlRv = pGames.FlRv; PIR = pGames.PIR;
             PlusMinus = pGames.PlusMinus; Place = pGames.Place; Result = pGames.Result; OppTeam = pGames.OppTeam; Name = pGames.Name;
             Date = pGames.Date;
        }
    }

    public class PlayerProfileModel
    {
        public PlayerStatModel statModel;
        public PlayerGamesModel[] gamesModel;
    }

    #endregion

    # region news

    public class NewsModel
    {
        public int Id;
        public string Name, Text, Image_big, Image_small;        
        public NewsModel() { }
        public NewsModel(NewsItem item)
        {
            Id = item.Id;
            Image_big = item.Image_big;
            Image_small = item.Image_small;
            Name = item.Name;
            Text = item.Text;
        }
    }

    #endregion
}