﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using EuroleagueReader;
using EuroleagueAnalytics;
using WEB.Models;
using DBReader;
using Newtonsoft.Json;

namespace WEB.Controllers
{
    public class HomeController : Controller
    {
        #region const

        const int PageSize = 20;

        static Result _result = new Result { Team1Name = "Team 1", Team2Name = "Team 2", Team1Score = 80, Team2Score = 81 };
		static ResultsGroup[] _resultGroups = new[] 
		{
			new ResultsGroup { Title = "14 Dec", Results = new[] { _result, _result, _result, _result } },
			new ResultsGroup { Title = "15 Dec", Results = new[] { _result, _result, _result, _result } },
		};

        static TopBlockItem[] _blockItems = new[] 
 		{
            new TopBlockItem { Name = "Item 1", Value = "5.1", Image = "/Images/Players/c49.jpg", TeamName = "Alba Berlin", TeamImage = "/Images/Teams/5fa.png" },
			new TopBlockItem { Name = "Item 2", Value = "4.1", Image = "/Images/Players/203076.png", TeamName = "Alba Berlin" },
			new TopBlockItem { Name = "Item 3", Value = "3.1", Image = "/Images/Players/203076.png", TeamName = "Alba Berlin" },
			new TopBlockItem { Name = "Item 4", Value = "2.1", Image = "/Images/Players/203076.png", TeamName = "Alba Berlin" },
    		new TopBlockItem { Name = "Item 5", Value = "1.1", Image = "/Images/Players/203076.png", TeamName = "Alba Berlin" },
        };

        static TopBlockGroup[] _blockGroups = new[] 
		{
			new TopBlockGroup { Items = _blockItems, Title = "Group1" },
			new TopBlockGroup { Items = _blockItems, Title = "Group2" },
		};

        #endregion

        public ActionResult Index()
        {
            //GameImport gmImport = new GameImport();
            //gmImport.Import("http://www.euroleague.net/main/results/showgame?gamecode=189&seasoncode=E2014");

            TopReader tReader = new TopReader();
            ResultsReader resReader = new ResultsReader();
            StandingsReader stReader = new StandingsReader();
            NewsReader nsReader = new NewsReader();
            List<ResultsGroup> resultGroups = new List<ResultsGroup>();
            List<TeamsGroup> teamGroups = new List<TeamsGroup>();                

            var Scores = tReader.GetTopPts().Items.Select(p => new TopBlockItem(p)).ToArray();
            var Reb = tReader.GetTopReb().Items.Select(p => new TopBlockItem(p)).ToArray();
            var Ast = tReader.GetTopAssists().Items.Select(p => new TopBlockItem(p)).ToArray();
            var OffRtg = tReader.GetTopOffRtg().Items.Select(p => new TopBlockItem(p)).ToArray();
            var DefRtg = tReader.GetTopDefRtg().Items.Select(p => new TopBlockItem(p)).ToArray();
            var NetRtg = tReader.GetTopNetRtg().Items.Select(p => new TopBlockItem(p)).ToArray();
            var Usage = tReader.GetTopUsage().Items.Select(p => new TopBlockItem(p)).ToArray();
            var PIE = tReader.GetTopPIE().Items.Select(p => new TopBlockItem(p)).ToArray();
            var Pace = tReader.GetTopPace().Items.Select(p => new TopBlockItem(p)).ToArray();

            var results = resReader.GetTopResults();
            var standings = stReader.GetStandingsInfo();

            if (results != null)
            {
                foreach (var res in results)
                {
                    resultGroups.Add(new ResultsGroup
                    {
                        Title = res.Title,
                        Results = res.Results.Select(t => new Result(t)).ToArray()
                    });
                }
            }

            if (standings != null)
            {
                foreach (var standing in standings)
                {
                    teamGroups.Add(new TeamsGroup
                    {
                        Name = standing.Name,
                        Teams = standing.Teams.Select(t => new TeamInfo(t)).ToArray()
                    });
                }
            }

            var ScoreGroups = new[] 
			{
				new TopBlockGroup { Items = Scores, Title = "Top 16" },
				new TopBlockGroup { Items = Scores, Title = "Overall" },
    		};

            var UsageGroups = new[] 
			{
				new TopBlockGroup { Items = Usage, Title = "Top 16" },
				new TopBlockGroup { Items = Usage, Title = "Overall" },
    		};

            var OffRtgGroups = new[] 
			{
				new TopBlockGroup { Items = OffRtg, Title = "Top 16" },
				new TopBlockGroup { Items = OffRtg, Title = "Overall" },
    		};

            var DefRtgGroups = new[] 
			{
				new TopBlockGroup { Items = DefRtg, Title = "Top 16" },
				new TopBlockGroup { Items = DefRtg, Title = "Overall" },
    		};

            var NetRtgGroups = new[] 
			{
				new TopBlockGroup { Items = NetRtg, Title = "Top 16" },
				new TopBlockGroup { Items = NetRtg, Title = "Overall" },
    		};

            var ReboundGroups = new[] 
			{
				new TopBlockGroup { Items = Reb, Title = "Top 16" },
				new TopBlockGroup { Items = Reb, Title = "Overall" },
    		};

            var PIEGroups = new[] 
			{
				new TopBlockGroup { Items = PIE, Title = "Top 16" },
				new TopBlockGroup { Items = PIE, Title = "Overall" },
    		};

            var AssistGroups = new[] 
			{
				new TopBlockGroup { Items = Ast, Title = "Top 16" },
				new TopBlockGroup { Items = Ast, Title = "Overall" },
    		};

            var PaceGroups = new[] 
			{
				new TopBlockGroup { Items = Pace, Title = "Top 16" },
				new TopBlockGroup { Items = Pace, Title = "Overall" },
    		};


            var model = new IndexModel
            {
                News = nsReader.GetNewsList().Items.Select(p => new NewsModel(p)).ToArray(),
                Standings = new StandingsInfo { TeamGroups = teamGroups.ToArray() },
                Results = new ResultsInfo { Groups = resultGroups.ToArray() },
                OFFRTG = new TopBlock { Title = "OFF RTG", Id = "top-offrtg", Groups = OffRtgGroups },
                USAGE = new TopBlock { Title = "USAGE", Id = "top-usage", Groups = UsageGroups },
                SCORES = new TopBlock { Title = "SCORES", Id = "top-scores", Groups = ScoreGroups },
                REB = new TopBlock { Title = "REB", Id = "top-reb", Groups = ReboundGroups },
                DEFRTG = new TopBlock { Title = "DEF RTG", Id = "top-defrtg", Groups = DefRtgGroups },
                PIE = new TopBlock { Title = "PIE", Id = "top-pie", Groups = PIEGroups },
                ASSISTS = new TopBlock { Title = "ASSISTS", Id = "top-assists", Groups = AssistGroups },
                NETRTG = new TopBlock { Title = "NET RTG", Id = "top-netrtg", Groups = NetRtgGroups },
                PACE = new TopBlock { Title = "PACE", Id = "top-pace", Groups = PaceGroups },
            };

            return View(model);
        }

        public ActionResult Games()
 		{
			ViewBag.Menu = "Games"; 
            RightBlockReader pmReader = new RightBlockReader();
            ResultsReader resReader = new ResultsReader();
            List<ResultsGroup> _Tours = new List<ResultsGroup>();
            List<ResultsInfo> _Stages = new List<ResultsInfo>();
            var PlusMinus = pmReader.GetTop25PlusMinus().Items.Select(p => new TopBlockItem(p)).ToArray();
            var stages = resReader.GetResults();

            foreach (var stage in stages)
            {
                var results = stage.Groups;
                _Tours.Clear();
                if (results != null)
                {
                    foreach (var res in results)
                    {
                        _Tours.Add(new ResultsGroup
                        {
                            Title = res.Title,
                            Results = res.Results.Select(t => new Result(t)).ToArray()
                        });                        
                    }
                    var SortedTours = _Tours.OrderByDescending(t => t.Title).ToList();
                    _Stages.Add(new ResultsInfo
                    {
                        Title = stage.Title,
                        Groups = SortedTours.ToArray()
                    });
                }
            }
            var SortedStages = _Stages.OrderByDescending(t => t.Title).ToList();

            var PlusMinusGroups = new[] 
			{
				new TopBlockGroup { Items = PlusMinus, Title = "Top 16" },
				new TopBlockGroup { Items = PlusMinus, Title = "Overall" },
    		};

			var tours = new[] 
			{
				new ResultsInfo { Title = "Tour 1", Groups = _resultGroups },
				new ResultsInfo { Title = "Tour 2", Groups = _resultGroups },
			};
			var model = new GamesPageModel 
			{
                PLUSMINUS = new TopBlock { Title = "Plus/Minus", Id = "top-offrtg", Groups = PlusMinusGroups },
                Stages = SortedStages.ToArray()	
			};
			return View(model);
 		}

        public ActionResult Traditional()
        {
            RightBlockReader pmReader = new RightBlockReader();
            var PlusMinus = pmReader.GetTop25PlusMinus().Items.Select(p => new TopBlockItem(p)).ToArray();

            SortingPagingInfo info = new SortingPagingInfo();
            info.SortField = "PIR";
            info.SortDirection = "desc";
            info.CurrentPageIndex = 0;
            info.PageSize = PageSize;

            var PlusMinusGroups = new[] 
			{
				new TopBlockGroup { Items = PlusMinus, Title = "Top 16" },
				new TopBlockGroup { Items = PlusMinus, Title = "Overall" },
    		};

            var tables = new[] 
			{
				GetTrPlayerStatsStage(info, "Top 16"),
				GetTrPlayerStatsStage(info, "Overall"),
			};

            var model = new TraditionalStatsModel
            {
                PLUSMINUS = new TopBlock { Title = "Plus/Minus", Id = "top-offrtg", Groups = PlusMinusGroups },
                Stages = tables
            };
            return View(model);
        }

        public ActionResult TraditionalPart(SortingPagingInfo info)
        {
            var model = GetTrPlayerStatsStage(info, null);

            return PartialView("_IndexStats", model);
        }

        public ActionResult Shooting()
        {
            RightBlockReader pmReader = new RightBlockReader();
            var TS = pmReader.GetTop25EFG().Items.Select(p => new TopBlockItem(p)).ToArray();

            SortingPagingInfo info = new SortingPagingInfo();
            info.SortField = "FG2";
            info.SortDirection = "desc";
            info.CurrentPageIndex = 0;
            info.PageSize = PageSize;            

            var TS_Group = new[] 
			{
				new TopBlockGroup { Items = TS, Title = "Top 16" },
				new TopBlockGroup { Items = TS, Title = "Overall" },
    		};

            var tables = new[] 
			{
				GetShPlayerStatsStage(info, "Top 16"),
				GetShPlayerStatsStage(info, "Overall"),
			};

            var model = new ShootingStatsModel
            {
                TS = new TopBlock { Title = "Effective Scorers (True Shooting%)", Id = "top-offrtg", Groups = TS_Group },
                Stages = tables
            };

            return View(model);
        }

        public ActionResult ShootingPart(SortingPagingInfo info)
        {
            var model = GetShPlayerStatsStage(info, null);

            return PartialView("_IndexShooting", model);
        }


        public ActionResult Possessions()
        {
            RightBlockReader pmReader = new RightBlockReader();
            var Pace = pmReader.GetTop25Pace().Items.Select(p => new TopBlockItem(p)).ToArray();

            SortingPagingInfo info = new SortingPagingInfo();
            info.SortField = "Usage";
            info.SortDirection = "desc";
            info.CurrentPageIndex = 0;
            info.PageSize = PageSize;

            var Pace_Group = new[] 
			{
				new TopBlockGroup { Items = Pace, Title = "Top 16" },
				new TopBlockGroup { Items = Pace, Title = "Overall" },
    		};

            var tables = new[] 
			{
				GetPosPlayerStatsStage(info, "Top 16"),
				GetPosPlayerStatsStage(info, "Overall"),
			};

            var model = new PossessionStatsModel
            {
                Pace = new TopBlock { Title = "Pace", Id = "top-offrtg", Groups = Pace_Group },
                Stages = tables                
            };

            return View(model);
        }

        public ActionResult PossessionsPart(SortingPagingInfo info)
        {
            var model = GetPosPlayerStatsStage(info, null);

            return PartialView("_IndexPossessions", model);
        }

        public ActionResult Advanced()
        {
            RightBlockReader pmReader = new RightBlockReader();
            var RebPerc = pmReader.GetTop25RebPercPl().Items.Select(p => new TopBlockItem(p)).ToArray();

            SortingPagingInfo info = new SortingPagingInfo();
            info.SortField = "NetRtg";
            info.SortDirection = "desc";
            info.CurrentPageIndex = 0;
            info.PageSize = PageSize;

            var Pace_Group = new[] 
			{
				new TopBlockGroup { Items = RebPerc, Title = "Top 16" },
				new TopBlockGroup { Items = RebPerc, Title = "Overall" },
    		};

            var tables = new[] 
			{
				GetAdvPlayerStatsStage(info, "Top 16"),
				GetAdvPlayerStatsStage(info, "Overall"),
			};

            var model = new AdvancedStatsModel
            {
                Pace = new TopBlock { Title = "Pace", Id = "top-offrtg", Groups = Pace_Group },
                Stages = tables
            };

            return View(model);
        }

        public ActionResult AdvancedPart(SortingPagingInfo info)
        {
            var model = GetAdvPlayerStatsStage(info, null);

            return PartialView("_IndexAdvanced", model);
        }

        public ActionResult Teams()
        {
            RightBlockReader pmReader = new RightBlockReader();
            var FG2 = pmReader.GetTop25FG2Teams().Items.Select(p => new TopBlockItem(p)).ToArray();

            SortingPagingInfo info = new SortingPagingInfo();
            info.SortField = "PPG";
            info.SortDirection = "desc";
            info.CurrentPageIndex = 0;
            info.PageSize = PageSize;

            var Pace_Group = new[] 
			{
				new TopBlockGroup { Items = FG2, Title = "Top 16" },
				new TopBlockGroup { Items = FG2, Title = "Overall" },
    		};

            var tables = new[] 
			{
				GetTrTeamsStatsStage(info, "Top 16"),
				GetTrTeamsStatsStage(info, "Overall"),
			};

            var model = new TrTeamStatsModel
            {
                Pace = new TopBlock { Title = "FG2%", Id = "top-offrtg", Groups = Pace_Group },
                Stages = tables
            };

            return View(model);
        }

        public ActionResult TeamsPart(SortingPagingInfo info)
        {
            var model = GetTrTeamsStatsStage(info, null);

            return PartialView("_IndexTeams", model);
        }


        public ActionResult TeamsAdvanced()
        {
            RightBlockReader pmReader = new RightBlockReader();
            var NetRtg = pmReader.GetTop25NetRtgTeams().Items.Select(p => new TopBlockItem(p)).ToArray();

            SortingPagingInfo info = new SortingPagingInfo();
            info.SortField = "PIR";
            info.SortDirection = "desc";
            info.CurrentPageIndex = 0;
            info.PageSize = PageSize;

            var Pace_Group = new[] 
			{
				new TopBlockGroup { Items = NetRtg, Title = "Top 16" },
				new TopBlockGroup { Items = NetRtg, Title = "Overall" },
    		};

            var tables = new[] 
			{
				GetAdvTeamsStatsStage(info, "Top 16"),
				GetAdvTeamsStatsStage(info, "Overall"),
			};

            var model = new AdvTeamStatsModel
            {
                Pace = new TopBlock { Title = "NET Rating", Id = "top-offrtg", Groups = Pace_Group },
                Stages = tables
            };

            return View(model);
        }


        public ActionResult TeamsAdvancedPart(SortingPagingInfo info)
        {
            var model = GetAdvTeamsStatsStage(info, null);

            return PartialView("_IndexTeamsAdvanced", model);
        }


        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Boxscore(int id)
        {
            RightBlockReader pmReader = new RightBlockReader();
            var Pace = pmReader.GetTop25Pace().Items.Select(p => new TopBlockItem(p)).ToArray();

            var Pace_Group = new[] 
			{
				new TopBlockGroup { Items = Pace, Title = "Top 16" },
				new TopBlockGroup { Items = Pace, Title = "Overall" },
    		};

            BoxscoreReader boxscoreReader = new BoxscoreReader();
            GameReader gmReader = boxscoreReader.GetGameBoxscore(id);           
            var model = new GameModel
            {
                Pace = new TopBlock { Title = "Pace", Id = "top-offrtg", Groups = Pace_Group },
                Game = new GameBoxscore(gmReader)
            };
            return View(model);
        }

        public ActionResult Players()
        {
            PlayersReader pReader = new PlayersReader();
            var model = new PlayerListModel
            {
                tpModel = pReader.GetPlayersTeamsList().Items.Select(p => new TeamPlayersModel(p)).ToArray()
            };
            return View(model);
        }

        public ActionResult Player(int id)
        {
            PlayersReader pReader = new PlayersReader();
            var plStats = pReader.GetPlayerStats(id);            
            var model = new PlayerProfileModel
            {
                gamesModel = pReader.GetPlayerGames(id).pGames.Select(p => new PlayerGamesModel(p)).ToArray(),
                statModel = new PlayerStatModel(plStats)
            };
            return View(model);
        }

        public ActionResult News(int id)
        {
            NewsReader nsReader = new NewsReader();
            NewsItem item = nsReader.GetNewById(id);
            var model = new NewsModel(item);           
            return View(model);
        }

        #region private

        private TrPlayerStatsStage GetTrPlayerStatsStage(SortingPagingInfo info, string title)
        {
            TraditionalStatsReader trReader = new TraditionalStatsReader();
            var TrStats = trReader.GetPlayerStats().Items.Select(p => new TrPlayerStats(p)).ToArray();

            info.PageCount = Convert.ToInt32(Math.Ceiling((double)(TrStats.Count() / PageSize))) + 1;
            info.PageSize = PageSize;            

            IEnumerable<TrPlayerStats> SortedStats = null;
           
                switch (info.SortField)
                {
                    case "Name":
                        SortedStats = (info.SortDirection == "asc" ?
                            TrStats.OrderByDescending(c => c.Name)
                            : TrStats.OrderBy(c => c.Name));
                        break;
                    case "PPG":
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.PPG) :
                         TrStats.OrderByDescending(c => c.PPG));
                        break;
                    case "RPG":
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.RPG) :
                         TrStats.OrderByDescending(c => c.RPG));
                        break;
                    case "APG":
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.APG) :
                         TrStats.OrderByDescending(c => c.APG));
                        break;
                    case "SPG":
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.SPG) :
                         TrStats.OrderByDescending(c => c.SPG));
                        break;
                    case "BPG":
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.BPG) :
                         TrStats.OrderByDescending(c => c.BPG));
                        break;
                    case "PLUS_MINUS":
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.PLUS_MINUS) :
                         TrStats.OrderByDescending(c => c.PLUS_MINUS));
                        break;
                    case "MPG":
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.MPG) :
                         TrStats.OrderByDescending(c => c.MPG));
                        break;
                    default:
                        SortedStats = (info.SortDirection == "asc" ?
                         TrStats.OrderBy(c => c.PIR) :
                         TrStats.OrderByDescending(c => c.PIR));
                        break;
                }          

            if ((info.Team != "*") && (info.Team != null))
            {
                SortedStats = SortedStats.Where(t => t.Team == info.Team);
                info.PageCount = 1;
            }
            else
                SortedStats = SortedStats.Skip(info.CurrentPageIndex * PageSize).Take(PageSize);

            var statsArray = SortedStats.ToArray();

            return new TrPlayerStatsStage 
            {
                Title = title,
                Stats = statsArray,
                Teams = info.GetTeamsList(),
                Paging = info 
            };
        }


        private ShPlayerStatsStage GetShPlayerStatsStage(SortingPagingInfo info, string title)
        {
            ShootingStatsReader trReader = new ShootingStatsReader();
            var ShStats = trReader.GetShootingStats().Items.Select(p => new ShPlayerStats(p)).ToArray();

            info.PageCount = Convert.ToInt32(Math.Ceiling((double)(ShStats.Count() / PageSize))) + 1;
            info.PageSize = PageSize;

            IEnumerable<ShPlayerStats> SortedStats = null;

            switch (info.SortField)
            {
                case "Name":
                    SortedStats = (info.SortDirection == "asc" ?
                        ShStats.OrderByDescending(c => c.Name)
                        : ShStats.OrderBy(c => c.Name));
                    break;                
                case "FG2A":
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.FG2A) :
                     ShStats.OrderByDescending(c => c.FG2A));
                    break;
                case "FG3":
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.FG3) :
                     ShStats.OrderByDescending(c => c.FG3));
                    break;
                case "FG3A":
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.FG3A) :
                     ShStats.OrderByDescending(c => c.FG3A));
                    break;
                case "FT":
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.FT) :
                     ShStats.OrderByDescending(c => c.FT));
                    break;
                case "FTA":
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.FTA) :
                     ShStats.OrderByDescending(c => c.FTA));
                    break;
                case "EFG":
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.EFG) :
                     ShStats.OrderByDescending(c => c.EFG));
                    break;
                case "TS":
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.TS) :
                     ShStats.OrderByDescending(c => c.TS));
                    break;
                default:
                    SortedStats = (info.SortDirection == "asc" ?
                     ShStats.OrderBy(c => c.FG2) :
                     ShStats.OrderByDescending(c => c.FG2));
                    break;
            }

            if ((info.Team != "*") && (info.Team != null))
            {
                SortedStats = SortedStats.Where(t => t.Team == info.Team);
                info.PageCount = 1;
            }
            else
                SortedStats = SortedStats.Skip(info.CurrentPageIndex * PageSize).Take(PageSize);

            var statsArray = SortedStats.ToArray();

            return new ShPlayerStatsStage
            {
                Title = title,
                Stats = statsArray,
                Teams = info.GetTeamsList(),
                Paging = info
            };
        }


        private PosPlayerStatsStage GetPosPlayerStatsStage(SortingPagingInfo info, string title)
        {
            PossessionsReader trReader = new PossessionsReader();
            var PosStats = trReader.GetPossessionStats().Items.Select(p => new PosessionPlayerStats(p)).ToArray();

            info.PageCount = Convert.ToInt32(Math.Ceiling((double)(PosStats.Count() / PageSize))) + 1;
            info.PageSize = PageSize;

            IEnumerable<PosessionPlayerStats> SortedStats = null;

            switch (info.SortField)
            {
                case "Name":
                    SortedStats = (info.SortDirection == "asc" ?
                        PosStats.OrderByDescending(c => c.Name)
                        : PosStats.OrderBy(c => c.Name));
                    break;
                case "Games":
                    SortedStats = (info.SortDirection == "asc" ?
                     PosStats.OrderBy(c => c.Games) :
                     PosStats.OrderByDescending(c => c.Games));
                    break;
                case "PlayerPossessions":
                    SortedStats = (info.SortDirection == "asc" ?
                     PosStats.OrderBy(c => c.PlayerPossessions) :
                     PosStats.OrderByDescending(c => c.PlayerPossessions));
                    break;
                case "TeamPossessions":
                    SortedStats = (info.SortDirection == "asc" ?
                     PosStats.OrderBy(c => c.TeamPossessions) :
                     PosStats.OrderByDescending(c => c.TeamPossessions));
                    break;
                case "Pace":
                    SortedStats = (info.SortDirection == "asc" ?
                     PosStats.OrderBy(c => c.Pace) :
                     PosStats.OrderByDescending(c => c.Pace));
                    break;
                case "PtsPos":
                    SortedStats = (info.SortDirection == "asc" ?
                     PosStats.OrderBy(c => c.PtsPos) :
                     PosStats.OrderByDescending(c => c.PtsPos));
                    break;
                case "ToPos":
                    SortedStats = (info.SortDirection == "asc" ?
                     PosStats.OrderBy(c => c.ToPos) :
                     PosStats.OrderByDescending(c => c.ToPos));
                    break;                
                default:
                    SortedStats = (info.SortDirection == "asc" ?
                     PosStats.OrderBy(c => c.Usage) :
                     PosStats.OrderByDescending(c => c.Usage));
                    break;
            }

            
            if ((info.Team != "*") && (info.Team != null))
            {
                SortedStats = SortedStats.Where(t => t.Team == info.Team);
                info.PageCount = 1;
            }
            else
                SortedStats = SortedStats.Skip(info.CurrentPageIndex * PageSize).Take(PageSize);

            var statsArray = SortedStats.ToArray();

            return new PosPlayerStatsStage
            {
                Title = title,
                Stats = statsArray,
                Teams = info.GetTeamsList(),
                Paging = info
            };
        }



        private AdvPlayerStatsStage GetAdvPlayerStatsStage(SortingPagingInfo info, string title)
        {
            AdvStatsReader trReader = new AdvStatsReader();
            var AdvStats = trReader.GetAdvancedStats().Items.Select(p => new AdvancedPlayerStats(p)).ToArray();

            info.PageCount = Convert.ToInt32(Math.Ceiling((double)(AdvStats.Count() / PageSize))) + 1;
            info.PageSize = PageSize;

            IEnumerable<AdvancedPlayerStats> SortedStats = null;

            switch (info.SortField)
            {
                case "Name":
                    SortedStats = (info.SortDirection == "asc" ?
                        AdvStats.OrderByDescending(c => c.Name)
                        : AdvStats.OrderBy(c => c.Name));
                    break;
                case "Games":
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.Games) :
                     AdvStats.OrderByDescending(c => c.Games));
                    break;
                case "OffRtg":
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.OffRtg) :
                     AdvStats.OrderByDescending(c => c.OffRtg));
                    break;
                case "DefRtg":
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.DefRtg) :
                     AdvStats.OrderByDescending(c => c.DefRtg));
                    break;
                case "NetRtg":
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.NetRtg) :
                     AdvStats.OrderByDescending(c => c.NetRtg));
                    break;
                case "PIE":
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.PIE) :
                     AdvStats.OrderByDescending(c => c.PIE));
                    break;
                case "OffRebPerc":
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.OffRebPerc) :
                     AdvStats.OrderByDescending(c => c.OffRebPerc));
                    break;
                case "DefRebPerc":
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.DefRebPerc) :
                     AdvStats.OrderByDescending(c => c.DefRebPerc));
                    break;
                default:
                    SortedStats = (info.SortDirection == "asc" ?
                     AdvStats.OrderBy(c => c.TotRebPerc) :
                     AdvStats.OrderByDescending(c => c.TotRebPerc));
                    break;
            }


            if ((info.Team != "*") && (info.Team != null))
            {
                SortedStats = SortedStats.Where(t => t.Team == info.Team);
                info.PageCount = 1;
            }
            else
                SortedStats = SortedStats.Skip(info.CurrentPageIndex * PageSize).Take(PageSize);

            var statsArray = SortedStats.ToArray();

            return new AdvPlayerStatsStage
            {
                Title = title,
                Stats = statsArray,
                Teams = info.GetTeamsList(),
                Paging = info
            };
        }


        private TrTeamStatsStage GetTrTeamsStatsStage(SortingPagingInfo info, string title)
        {
            TeamsStatsReader trReader = new TeamsStatsReader();
            var TrStats = trReader.GetTeamStats().Items.Select(p => new TraditionalTeamsStats(p)).ToArray();

            info.PageCount = Convert.ToInt32(Math.Ceiling((double)(TrStats.Count() / PageSize))) + 1;
            info.PageSize = PageSize;

            IEnumerable<TraditionalTeamsStats> SortedStats = null;

            switch (info.SortField)
            {
                case "Name":
                    SortedStats = (info.SortDirection == "asc" ?
                        TrStats.OrderByDescending(c => c.Name)
                        : TrStats.OrderBy(c => c.Name));
                    break;
                case "PPG":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.PPG) :
                     TrStats.OrderByDescending(c => c.PPG));
                    break;
                case "RPG":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.RPG) :
                     TrStats.OrderByDescending(c => c.RPG));
                    break;
                case "APG":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.APG) :
                     TrStats.OrderByDescending(c => c.APG));
                    break;
                case "SPG":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.SPG) :
                     TrStats.OrderByDescending(c => c.SPG));
                    break;
                case "BPG":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.BPG) :
                     TrStats.OrderByDescending(c => c.BPG));
                    break;
                case "FG2":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.FG2) :
                     TrStats.OrderByDescending(c => c.FG2));
                    break;
                case "FG3":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.FG3) :
                     TrStats.OrderByDescending(c => c.FG3));
                    break;
                default:
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.FT) :
                     TrStats.OrderByDescending(c => c.FT));
                    break;
            }

            var statsArray = SortedStats.ToArray();

            return new TrTeamStatsStage
            {
                Title = title,
                Stats = statsArray,
                Teams = info.GetTeamsList(),
                Paging = info
            };
        }


        private AdvTeamStatsStage GetAdvTeamsStatsStage(SortingPagingInfo info, string title)
        {
            TeamsStatsReader trReader = new TeamsStatsReader();
            var TrStats = trReader.GetTeamAdvStats().Items.Select(p => new AdvancedTeamsStats(p)).ToArray();

            info.PageCount = Convert.ToInt32(Math.Ceiling((double)(TrStats.Count() / PageSize))) + 1;
            info.PageSize = PageSize;

            IEnumerable<AdvancedTeamsStats> SortedStats = null;

            switch (info.SortField)
            {
                case "Name":
                    SortedStats = (info.SortDirection == "asc" ?
                        TrStats.OrderByDescending(c => c.Name)
                        : TrStats.OrderBy(c => c.Name));
                    break;
                case "PIR":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.PIR) :
                     TrStats.OrderByDescending(c => c.PIR));
                    break;
                case "OffRtg":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.OffRtg) :
                     TrStats.OrderByDescending(c => c.OffRtg));
                    break;
                case "DefRtg":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.DefRtg) :
                     TrStats.OrderByDescending(c => c.DefRtg));
                    break;
                case "NetRtg":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.NetRtg) :
                     TrStats.OrderByDescending(c => c.NetRtg));
                    break;
                case "PIE":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.PIE) :
                     TrStats.OrderByDescending(c => c.PIE));
                    break;
                case "Pace":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.Pace) :
                     TrStats.OrderByDescending(c => c.Pace));
                    break;
                case "OffRebPerc":
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.OffRebPerc) :
                     TrStats.OrderByDescending(c => c.OffRebPerc));
                    break;
                default:
                    SortedStats = (info.SortDirection == "asc" ?
                     TrStats.OrderBy(c => c.DefRebPerc) :
                     TrStats.OrderByDescending(c => c.DefRebPerc));
                    break;
            }


            var statsArray = SortedStats.ToArray();

            return new AdvTeamStatsStage
            {
                Title = title,
                Stats = statsArray,
                Teams = info.GetTeamsList(),
                Paging = info
            };
        }

        #endregion
    }
}
