﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBReader;
using Newtonsoft.Json;
using EuroleagueReader;
using EuroleagueAnalytics;
using WEB.Models;

namespace WEB.Controllers
{
    public class ChartController : Controller
    {
        //
        // GET: /Chart/
        public ActionResult GetChartOffDefData()
        {
            TeamsStatsReader trReader = new TeamsStatsReader();
            var Pace = trReader.GetTeamsOffRtg().Items.Select(p => new OffRtgChart(p)).ToArray();
            return Json(new { Data = Pace }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartPaceData()
        {
            TeamsStatsReader trReader = new TeamsStatsReader();
            var Pace = trReader.GetTeamsPace().Items.Select(p => new PaceRtgChart(p)).ToArray();
            return Json(new { Data = Pace }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartFg2Data(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var FG2 = boxReader.GetChart2FG(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "FG2%", Team = FG2 };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartFg3Data(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var FG3 = boxReader.GetChart3FG(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "FG3%", Team = FG3 };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartFTData(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var FT = boxReader.GetChartFT(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "FT%", Team = FT };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartRebData(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var Reb = boxReader.GetChartReb(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "Rebounds", Team = Reb };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartPIRData(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var PIR = boxReader.GetChartPIR(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "PIR", Team = PIR };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartNETData(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var NET = boxReader.GetChartNET(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "NET Rating", Team = NET };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartPPOSData(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var PPOS = boxReader.GetChartPPOS(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "Points per possession", Team = PPOS };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartPIEData(int id)
        {
            BoxscoreReader boxReader = new BoxscoreReader();
            var PIE = boxReader.GetChartPIE(id).Items.Select(p => new FG2ChartTeam(p)).ToArray();
            FG2Chart data = new FG2Chart { Name = "PIE", Team = PIE };
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartPlayerData(int playerId)
        {
            PlayersReader plReader = new PlayersReader();
            var PlayerStats = plReader.GetPlayerChart(playerId).Items.Select(p => new PlayerChData(p)).ToArray();
            var PlayerName = plReader.GetPlayerChart(playerId).Name;
            PlayerChart data = new PlayerChart { Player = PlayerStats};
            return Json(new { Data = data }, JsonRequestBehavior.AllowGet);
        }

    }
}
